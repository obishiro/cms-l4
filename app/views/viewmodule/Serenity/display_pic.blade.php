 <?php

$news = Content::select('tb_content.id','tb_content.content_name','tb_content.content_detail','tb_content.content_url','tb_content.content_picture','tb_content.created_at','tb_content.content_view')
->where(array(
  'tb_content.content_categories'=>$id,
  'tb_content.content_show'   =>'1'
  ))
//->join('tb_files','tb_files.token','=','tb_content.content_file')
->orderBy('tb_content.id','desc')->get();



?>
   @if($show !='0')
   
   <div class="solidline">
       <h4 class="text-topic"> <i class="{{$icon}}"></i> {{ $title }}</h4>
   </div>  
   @endif

    <div class="row">
      <ul class="portfolio-area da-thumbs">
        @foreach($news as $new2=>$n2)
        @if($n2->content_picture !="")
        <?php $picnews2 = URL::to("uploadfiles/news/".$n2->content_picture); ?>
      @else
      <?php $picnews2 = URL::to("uploadfiles/nopic.png"); ?>
      @endif
        <li class="portfolio-item2" data-id="id-0" data-type="web">
          <div class="span3">
            <div class="thumbnail">
              <div class="image-wrapp">
                
                <img src="{{$picnews2}}" alt="" style="height: 150px;width:270px">
                <article class="da-animate da-slideFromRight" style="display: block;">
                  <h4 ><a href="{{ URL::to('news',array($n2->content_url))}}" style="color: #FFF">{{ $n2->content_name}}</a></h4>
                   
                </article>
              </div>
            </div>
          </div>
        </li>
        @endforeach
        
        
        
        
        
        
      </ul>
    </div>