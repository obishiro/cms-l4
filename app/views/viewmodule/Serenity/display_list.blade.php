<?php

$news2 = Content::select('tb_depart.depart_name','tb_content.id','tb_content.content_name'
,'tb_content.content_detail','tb_content.content_url','tb_content.content_fast'
,'tb_content.content_picture','tb_content.created_at'
,'tb_content.content_view')
->join('tb_depart','tb_depart.id','=','tb_content.content_author')	
->where(array(
  'tb_content.content_categories'=>$id,
  'tb_content.content_show'   =>'1'
  ))
//->join('tb_files','tb_files.token','=','tb_content.content_file')
->orderBy('tb_content.id','desc')->take(10)->skip(0)->get();
 


?>
 
  @if($show !='0')
 
      <h3 class="h3_title"><a  href="{{ URL::to('allnews',array($url))}}" ><i class="{{$icon}}"></i>{{ $title }}</a></h3>
   
  @endif

    <div class="row-fluid">
 
		<div class="span12">
        
    
            <table class="table table-striped mb-0 table-hover">
                <thead>
                 
                     
                  </thead>
                  <tbody>
                  @foreach($news2 as $GetHomeNews=>$HN) 
                    <tr><td>
                    <?php 
                    switch($HN->content_fast)
                    {
                      case '1':
                        echo "<span class=\"label label-important\">ด่วนมาก!</span>";
                      break;
                      case '2':
                        echo "<span class=\"label label-important\">ด่วนที่สุด!</span>";
                    break;
                     
                    }
                    ?>
                    @if(Helpers::DateDiff($HN->created_at)<=3)
                    <span class="label label-warning">ล่าสุด</span>
                    @endif
                    <?php //echo mb_strimwidth($HN->content_name,0,100,"...");?>
                      
                      <a href="{{ URL::to('news',array($HN->content_url))}}">{{$HN->content_name}}</a>
                        <span class="badge badge-success">{{ Helpers::DateFormat($HN->created_at) }}</span>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="span12" style="text-align:right;padding-right:20px;">
                  <a style="color:#fff" href="{{ URL::to('allnews',array($url))}}" class="btn btn-info"><i class="icon-link"></i> อ่านทั้งหมด</a>
                 </div>
 
 
 
          
    </div>
    
  
    </div>
    <hr>