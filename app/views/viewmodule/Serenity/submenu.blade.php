@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$data->submenu_name)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
 
          <section id="content">
              <div class="container">
                <div class="row ">
                  <div class="span9" >
                    <article>
                        <div class="row card-2">
                          <div class="span9">
                            <div class="post-video">
                              <div class="post-heading">
                                  <ul class="breadcrumb">
                                      <li><a href="{{URL::to('/')}}"><span class="font-icon-home"></span> หน้าหลัก</a> <span class="divider">/</span></li>
                                      <li><a href="#" >{{ $data->mainmenu_name }}</a> <span class="divider">/</span></li>
                                      <li class="active">{{ $data->submenu_name}}</li>
                                    </ul>
                               
                              </div>
                              
                            </div>
                            <div class="clearfix">
                              </div>
                            <h6> {{ $data->submenu_name}}</h6>
                            <div class="addthis_sharing_toolbox"></div>
                            <p>
                                {{ $data->submenu_detail}}
                               
                            </p>
                            <div class="bottom-article">
                              <ul class="post-meta">
                                <li><i class="icon-calendar"></i> {{ Helpers::DateFormat($data->created_at) }}</li>
                                <li><i class="icon-user"></i> {{$user->firstName}} {{$user->lastName}}</li>
                                <li><i class="icon-folder-open"></i> <a href="#" >{{ $data->mainmenu_name }}</a></li>
                                <!-- <li><i class="icon-comments"></i>{{ number_format($data->submenu_view)}}</li> -->
                              </ul>
                               
                            </div>
                          </div>
                        </div>
                     
                      </article>
                  
                     
                  </div>
         
 
@stop

@section('script')
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e2bd5c123e4313e"></script>
@stop
