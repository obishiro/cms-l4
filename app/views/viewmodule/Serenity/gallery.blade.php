@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$Title)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
 

          <section id="content">
              <div class="container">
                <div class="row ">
                  <div class="span9" >
                    <article>
                        <div class="row card-2">
                          <div class="span9">
                            <div class="post-video">
                              <div class="post-heading">
                                  <ul class="breadcrumb">
                                      <li><a href="{{URL::to('/')}}"><span class="font-icon-home"></span> หน้าหลัก</a> <span class="divider">/</span></li>
 
                                      <li class="active">{{ $Title}}</li>
                                    </ul>
                               
                              </div>
                              
                            </div>
                            <div class="clearfix">
                              </div>
                            <h6> {{ $Title}}</h6>
                            <div class="addthis_sharing_toolbox"></div>
                            <ul id="thumbs" class="portfolio">
                                <!-- Item Project and Filter Name -->
                                @foreach($Gallery_file as $Gallery =>$g)
                                <li class="item-thumbs span3 design" g-id="id-0" data-type="web">
                                 
                                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="" href="{{ URL::to('uploadfiles/gallery',array($g->files_newname))}}">
                                      <span class="overlay-img"></span>
                                      <span class="overlay-img-thumb font-icon-plus"></span>
                                      </a>
                                  <!-- Thumb Image and Description -->
                                  <img src="{{ URL::to('uploadfiles/gallery/thumb',array($g->files_newname))}}" alt="" >
                                </li>
                                @endforeach
                                
                              </ul>
                            <div class="bottom-article">
                              <ul class="post-meta">
                                <li><i class="icon-calendar"></i> {{ Helpers::DateFormat($g->created_at) }}</li>
                                <li><i class="icon-user"></i> Admin</li>
                                 
                                <li><i class="icon-comments"></i>{{ number_format($g->gallery_view)}}</li>
                              </ul>
                               
                            </div>
                          </div>
                        </div>
                     
                      </article>
                  
                     
                  </div>
         
 
@stop

@section('right')
 
@stop
@section('script')
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e2bd5c123e4313e"></script>
@stop
