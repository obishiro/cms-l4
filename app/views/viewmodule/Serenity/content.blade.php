@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$data->content_name)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
 
 
 
 
     
     
    <section id="content">
      <div class="container">
        <div class="row ">
          <div class="span9" >
            <article>
                <div class="row card-2">
                  <div class="span9">
                    <div class="post-video">
                      <div class="post-heading">
                          <ul class="breadcrumb">
                              <li><a href="{{URL::to('/')}}"><span class="font-icon-home"></span> หน้าหลัก</a> <span class="divider">/</span></li>
                              <li><a href="{{URL::to('allnews',array($data->categories_url))}}" >{{ $data->categories_name }}</a> <span class="divider">/</span></li>
                              <li class="active">{{ $data->content_name}}</li>
                            </ul>
                       
                      </div>
                      
                      
                    </div>
                    <div class="clearfix">
                      </div>
                    <h4> {{ $data->content_name}}</h4>
                    <div class="addthis_sharing_toolbox"></div>
                    <p>
                        {{ $data->content_detail}}
                        {{ $data->content_alldetail}}
                    </p>
                   @if($Numfiles!=0)
                    <div id="pdfobject-container" style="height: 650px"></div>
                    @endif
                    
                    <div class="bottom-article">
                      <ul class="post-meta">
                        <li><i class="icon-calendar"></i> {{ Helpers::DateFormat($data->created_at) }}</li>
                        <li><i class="icon-user"></i> {{$user->firstName}} {{$user->lastName}}</li>
                        <li><i class="icon-folder-open"></i> <a href="{{URL::to('allnews',array($data->categories_url))}}" >{{ $data->categories_name }}</a></li>
                        <li><i class="icon-eye-open"></i>{{ number_format($data->content_view)}}</li>
                      </ul>
                       
                    </div>
                  </div>
                </div>
             
              </article>
          
             
          </div>
          @if($Numfiles!=0)
          <script src="{{ URL::to('js/pdfobject.min.js') }}"></script>
          <script>PDFObject.embed("{{URL::to('uploadfiles/',$Files->files_newname)}}", "#pdfobject-container");</script>
          @endif
@stop
@section('right')
<div class="widget">
    <h4 class="widgetheading">บทความที่เกี่ยวข้อง</h4>
    <ul class="recent">
            @foreach($Datarandom as $dataran =>$dr)
            @if($dr->content_picture !="")
                      <?php $picnews = URL::to("uploadfiles/news/".$dr->content_picture); ?>
                    @else
                    <?php $picnews = URL::to("uploadfiles/nopic.png"); ?>
                    @endif
         <li>
            <a  href="{{ URL::to('news',array($dr->content_url))}}">
                <img src="{{ $picnews }}" class="pull-left" class="zoom" style="width:65px;height:65px" />
            </a>
            <h8><a  href="{{ URL::to('news',array($dr->content_url))}}"><?php echo mb_strimwidth($dr->content_name,0,100,"..."); ?></a></h8>
         
            </li>
      @endforeach
    </ul>
  </div>

@stop
@section('script')

  <!-- Go to www.addthis.com/dashboard to customize your tools -->
 
  
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e2bd5c123e4313e"></script>
@stop
