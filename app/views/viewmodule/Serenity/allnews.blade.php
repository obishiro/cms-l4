@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$Title)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')

@section('content')
 
 
 
 
     
     
    <section id="content">
      <div class="container">
        <div class="row ">
          <div class="span9" >
            <article>
                <div class="row card-2">
                  <div class="span9">
                    <div class="post-video">
                      <div class="post-heading">
                          <ul class="breadcrumb">
                              <li><a href="{{URL::to('/')}}"><span class="font-icon-home"></span> หน้าหลัก</a> <span class="divider">/</span></li>
                              <li>{{ $Title}}</li>
                           
                            </ul>
                       
                      </div>
                      
                    </div>
                    <div class="clearfix">
                      </div>
                    
                    <div class="addthis_sharing_toolbox"></div>
                    <div class="accordion" id="accordion2">
                        <div class="accordion-group">
                          <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                {{ $Title}}
                            </a>
                          </div>
                          <div id="collapseOne" class="accordion-body collapse in">
                            <div class="accordion-inner">
                                <table class="table table-striped mb-0 table-hover">
                                    <thead>
                                        <tr><th>หัวข้อประกาศ</th><th width="15%">วันที่ประกาศ</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($data as $new => $HN)
                                        <tr><td><span class="label label-warning">ล่าสุด!</span>
                                          <a  href="{{ URL::to('news',array($HN->content_url))}}"><?php echo mb_strimwidth($HN->content_name,0,100,"...");?></a>
                                          </td>
                                            <td>
                                              {{ Helpers::DateFormat($HN->created_at) }}
                                          </td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                    
                            </div>
                           
                          </div>
                          <?php echo $data->links(); ?>
                        </div>
                  
                      </div>
                    
                  </div>
                </div>
             
              </article>
          
             
          </div>
 
@stop
 
@section('script')
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e2bd5c123e4313e"></script>
@stop
