<script src="{{ asset('frontend/js/jquery-1.11.1.min.js') }} "></script>
<script src="{{ asset('frontend/js/jquery-migrate-1.2.1.min.js') }} "></script>	
<script src="{{ asset('frontend/js/bootstrap.min.js') }} "></script>
<script src="{{ asset('frontend/js/bootstrap-hover-dropdown.min.js') }} "></script>
 
<script src="{{ asset('frontend/js/owl.carousel.min.js') }} "></script>
<script src="{{ asset('frontend/js/custom.js') }} "></script>
 <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
<script src="{{ asset('frontend/js/ddsmoothmenu.js') }} "></script>
<script src="{{ asset('frontend/js/jquery.slicknav.js') }} "></script>
<script src="{{ asset('js/masonry.pkgd.min.js') }} "></script>

 

<script language="javascript">
	ddsmoothmenu.init({
	mainmenuid: "smoothmenu2", //Menu DIV id
	orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
	method: 'hover', // set to 'hover' (default) or 'toggle'
	arrowswap: true, // enable rollover effect on menu arrow images?
	//customtheme: ["#804000", "#482400"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>
<script type="text/javascript">
$('#btsearch').click(function(){
 $('#keyword').fadeIn( "slow" );
});
$('.grid').masonry({
  // options
  itemSelector: '.grid-item',
 // columnWidth: 150
});
</script>
<script>
	$('#menu2').slicknav({
	label: '',
	duration: 1000,
	easingOpen: "easeOutBounce", //available with jQuery UI
	prependTo:'#demo2'
});
</script>
@yield('script')
</body>
</html>