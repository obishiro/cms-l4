      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            
            
          </div>
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            @if(Auth::user()->user_type==1)
            <li class="treeview">
              <a href="#">
                <i class="fa fa-gears"></i> <span>{{ Lang::get('msg.dashboard',array(),'th') }}</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 
                <li>
                  <a href="{{ URL::to('backend/config/enviroment') }}">
                    <i class="fa fa-globe" aria-hidden="true"></i>
                     {{ Lang::get('msg.config-env',array(),'th') }}
                  </a>
                </li>
             
                 <li>
                  <a href="{{ URL::to('backend/config/tag') }}">
                    <i class="fa fa-tags" aria-hidden="true"></i>
                     {{ Lang::get('msg.config-tag',array(),'th') }}
                  </a>
                </li>
                  <li>
              <a href="{{ URL::to('backend/banner') }}">
                <i class="fa fa-television"></i> <span>{{ Lang::get('msg.config-slide',array(),'th') }}</span>
              </a>
            </li>
                  <li>
                  <a href="{{ URL::to('backend/config/password') }}">
                    <i class="fa fa-lock"></i> {{ Lang::get('msg.config-password',array(),'th') }}
                  </a>
                </li>
               </ul>
            </li>
            
        @endif
           
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>{{ Lang::get('msg.mainmenu',array(),'th') }}</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 
                <li>
                  <a href="{{ URL::to('backend/menu/mainmenu') }}">
                    <i class="fa fa-caret-right"></i> {{ Lang::get('msg.mainmenu',array(),'th') }}
                  </a>
                </li>
                 
                 <li>
                  <a href="{{ URL::to('backend/menu/submenu') }}">
                    <i class="fa fa-caret-right"></i> {{ Lang::get('msg.submenu',array(),'th') }}
                  </a>
                </li>


              </ul>
            </li>
            <li class="treeview">
              <a href="{{ URL::to('backend/content') }}">
                <i class="fa fa-files-o"></i>
                <span>{{ Lang::get('msg.content',array(), 'th') }}</span><i class="fa fa-angle-left pull-right"></i>
             </a>
               <ul class="treeview-menu">
                   <li>
                  <a href="{{ URL::to('backend/config/categories') }}">
                    <i class="fa fa-caret-right"></i> {{ Lang::get('msg.config-categories',array(),'th') }}
                  </a>
                </li>
                  <li>
                  <a href="{{ URL::to('backend/content') }}">
                    <i class="fa fa-caret-right"></i> {{ Lang::get('msg.content',array(), 'th') }}
                  </a>
                </li>
               </ul>
            </li>
            <li>
              <a href="{{ URL::to('backend/gallery') }}">
                <i class="fa fa-file-image-o" aria-hidden="true"></i> <span>{{ Lang::get('msg.gallery',array(),'th') }}</span>
              </a>
             
            </li>

                 
             @if(Auth::user()->user_type==1)
            {{--  <li class="treeview">
               <a href="{{ URL::to('backend/config/theme') }}">
                    <i class="fa fa-dashboard" aria-hidden="true"></i>
                     {{ Lang::get('msg.config-theme',array(),'th') }}</span><i class="fa fa-angle-left pull-right"></i>
             </a>
               <ul class="treeview-menu">
                   <li>
                  <a href="{{ URL::to('backend/config/css') }}">
                    <i class="fa fa-caret-right"></i> {{ Lang::get('msg.config-css',array(),'th') }}
                  </a>
                </li>
                 
               </ul>
            </li>  --}}
            @if(Auth::user()->user_state==1)
            <li class="treeview">
              
              <a href="#">
             
                <i class="fa fa-user"></i> 
                 
                <span>{{ Lang::get('msg.msg_user',array(), 'th') }}</span><i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                 
                <li>
                  <a href="{{ URL::to('backend/user')}}">
                    <i class="fa fa-caret-right"></i> ผู้ใช้งานเว็บไซต์ กสด.นรด.
                  </a>

                </li>
                <li>
                  <a href="{{ URL::to('backend/user-aro')}}">
                    <i class="fa fa-caret-right"></i> ผู้ใช้งานระบบ ARO
                  </a>

                </li>
              </ul>
            </li>
            @endif
            @endif  
            
            <li><a href="{{ URL::to('backend/logout') }}"><i class="fa fa-power-off"></i> <span>{{ Lang::get('msg.msg_logout',array(),'th') }}</span></a></li>
            
          </ul>

        </section>
        <!-- /.sidebar -->
      </aside>