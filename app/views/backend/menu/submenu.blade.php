@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
			 
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ Lang::get('msg.list-item',array(), 'th') }}</h3>
               
               
              
              @if(Session::get('permission')=="1" || Session::get('permission')=="2" || Session::get('permission')=="3" || Session::get('permission')=="5")
              <div class="box-tools pull-right">
               <a href="{{ URL::to('backend/menu/add/submenu') }}"   class="btn btn-success"><i class="fa fa-plus-square"></i> {{ Lang::get('msg.msg_add',array(),'th') }}</a> </div>
              @endif
            </div>
            
            <div class="row" >
              <div class="col-md-5 col-sm-6 col-xs-12" 
              @if(Session::has('status'))
              id ="null"
              @endif
              @if(Session::has('save-success'))
               id="status_save" 
              @endif
              @if(Session::has('edit-success'))
               id="status_save" 
              @endif
              @if(Session::has('del-success'))
               id="status_save" 
              @endif
                style="margin-top:10px;margin-left:30%;  display:none" >
                 @if(Session::has('save-success'))
                  <div class="info-box bg-green">
                 @endif
                 @if(Session::has('edit-success'))
                  <div class="info-box bg-teal">
                 @endif
                 @if(Session::has('del-success'))
                  <div class="info-box bg-red-active">
                 @endif
                <span class="info-box-icon">
                  @if(Session::has('save-success'))
                  <i class="fa fa-save"></i>
                  @endif
                  @if(Session::has('del-success'))
                  <i class="fa fa-trash"></i>
                  @endif
                  @if(Session::has('edit-success'))
                  <i class="fa fa-pencil">
                  @endif
                  </i>
                </span>
                <div class="info-box-content">
                  <span class="info-box-text">{{ Lang::get('msg.msg_result', array(), 'th') }}</span>
                  <span class="info-box-number">
                    @if(Session::has('save-success'))
                    {{ Lang::get('msg.msg_save_success', array(), 'th') }}
                    @endif
                    @if(Session::has('del-success'))
                    {{ Lang::get('msg.msg_del_success', array(), 'th') }}
                    @endif
                    @if(Session::has('edit-success'))
                    {{ Lang::get('msg.msg_edit_success', array(), 'th') }}
                    @endif
                  </span>
                   </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              @if(Session::has('save-success') || Session::has('del-success') || Session::has('edit-success'))
               </div>
              @endif

              
            </div>
            <div class="box-body">
              <table id="Submenu_data" class="table table-bordered table-striped">
              	<thead>
             
                   
                    <th width="5%">{{ Lang::get('msg.msg_no', array(), 'th') }}</th> 
              		  <th width="30%">{{ Lang::get('msg.submenu', array(), 'th') }}</th> 
                      <th  width="20%">{{ Lang::get('msg.mainmenu_name', array(), 'th') }}</th>
                      <th width="10%">{{ Lang::get('msg.msg_type', array(), 'th') }}</th>
                      <th>{{ Lang::get('msg.msg_sort', array(), 'th') }}</th>
              		    <th>{{ Lang::get('msg.msg_show', array(), 'th') }}</th>
              		 
               
              		<th>{{ Lang::get('msg.msg_tools', array(), 'th') }}</th>
              	</thead>
              </table>
            </div><!-- /.box-body -->
             
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
{{ Session::get('status') }}
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_tag',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')
         <script type="text/javascript">
         var msg = $('#lang').val();
         var status =$('#status').val();
 
      var my_table= $('#Submenu_data').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 50,
            "targets": 0,
            "sAjaxSource": "{{ $api }}",
            columns: [
            {data:'no',name:'no'},
            {data: 'submenu_name', name: 'submenu_name'},
            {data: 'mainmenu_name', name: 'mainmenu_name'},
            {data: 'submenu_type', name : 'submenu_type'},
            {data: 'submenu_sorting', name : 'submenu_sorting'},
            {data: 'submenu_show', name : 'submenu_show'},
            
          
            {data: 'tools', name: 'tools'}
        ],
        "fnDrawCallback":function(){
         table_rows = my_table.fnGetNodes(); 
          $.each(table_rows, function(index){
          $("td:first", this).html(index+1);
          });
         }

            });
            
              $('#status_save').show(0).delay(2000).slideUp();
               
             
     
        </script>
@stop