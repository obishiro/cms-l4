@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
			 
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ Lang::get('msg.msg_edit',array(), 'th') }}</h3>
               
               
              

             
            </div>
            
            <div class="row" >
              <div class="col-md-5 col-sm-6 col-xs-12" 
              @if(Session::has('status'))
              id ="null"
              @endif
              @if(Session::has('save-success'))
               id="status_save" 
              @endif
              @if(Session::has('edit-success'))
               id="status_save" 
              @endif
              @if(Session::has('del-success'))
               id="status_save" 
              @endif
                style="margin-top:10px;margin-left:30%;  display:none" >
                 @if(Session::has('save-success'))
                  <div class="info-box bg-green">
                 @endif
                 @if(Session::has('edit-success'))
                  <div class="info-box bg-teal">
                 @endif
                 @if(Session::has('del-success'))
                  <div class="info-box bg-red-active">
                 @endif
                <span class="info-box-icon">
                  @if(Session::has('save-success'))
                  <i class="fa fa-save"></i>
                  @endif
                  @if(Session::has('del-success'))
                  <i class="fa fa-trash"></i>
                  @endif
                  @if(Session::has('edit-success'))
                  <i class="fa fa-pencil">
                  @endif
                  </i>
                </span>
                <div class="info-box-content">
                  <span class="info-box-text">{{ Lang::get('msg.msg_result', array(), 'th') }}</span>
                  <span class="info-box-number">
                    @if(Session::has('save-success'))
                    {{ Lang::get('msg.msg_save_success', array(), 'th') }}
                    @endif
                    @if(Session::has('del-success'))
                    {{ Lang::get('msg.msg_del_success', array(), 'th') }}
                    @endif
                    @if(Session::has('edit-success'))
                    {{ Lang::get('msg.msg_edit_success', array(), 'th') }}
                    @endif
                  </span>
                   </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              @if(Session::has('save-success') || Session::has('del-success') || Session::has('edit-success'))
               </div>
              @endif

              
            </div>
            <div class="box-body">
               <div class="box box-primary">
                 
              
                  {{ Form::open(array(
                    'id'=>'form-content'
                    ,'role'=>'form'
                   ,'enctype'=>'multipart/form-data'
                    ,'url'=>'backend/content/editcontent'
                    ), $rules)}}
                  <div class="box-body">
                    <div class="form-group required">
                      <label for="">{{ Lang::get('msg.content_categories', array(), 'th') }}</label>
                      <select name="txt_type" id="bt-url" class="form-control">
                        <option value=""></option>
                        @foreach($sql as $cat => $ca)
                        <option value="{{ $ca->id }}" @if($c->content_categories==$ca->id) selected=true @endif>{{ $ca->categories_name }}</option>
                        @endforeach
                      </select>
                       
                       </div>
                    <div class="form-group">
                      <label for="">{{ Lang::get('msg.content_name', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_name', $c->content_name, 
                         array(
                               'class'=>'form-control'

                              ))}}
                    </div>
                    <div class="form-group required">
                      <label for="">แผนกเจ้าของเรื่อง</label>
                      <select name="txt_depart"  class="form-control" data-validetta='required'>
                          <option value="">-เลือกแผนก-</option>
                         @foreach($depart as $catd =>$cd)
                         <option value="{{ $cd->id }}" @if($c->content_author==$cd->id) selected=true @endif>{{ $cd->depart_name }}</option>
                        @endforeach
						 
						 
                      </select>
                       
                       </div>
                    
                    <div class="form-group">
                      <label for="">{{ Lang::get('msg.content_detail',array(),'th') }}</label>
                         <textarea id="editor1" name="txt_detail" rows="10" cols="80">{{ $c->content_detail }}</textarea>
                      </div> 
                      <div class="form-group">
                      <label for="">{{ Lang::get('msg.content_alldetail',array(),'th') }}</label>
                         <textarea id="editor2" name="txt_alldetail" rows="10" cols="80" data-validetta='required'>{{ $c->content_alldetail }}</textarea>
                      </div> 
                      <img class="img-responsive" style="height:150px;" src="{{ URL::to('uploadfiles/news/thumb',array($c->content_picture))}}" alt="">
                        <div class="form-group">

                      <label for="">{{ Lang::get('msg.content_picture',array(),'th') }}</label>
                         <input type="file" name="picture">
                      </div>
                          <div class="form-group">
                      <label for="">{{ Lang::get('msg.web_keyword', array(), 'th') }}</label>
                      <textarea name="web_keyword" id="" cols="30" rows="1" class="form-control"></textarea>

                    </div>
                       <div class="form-group">
                      <label for="">{{ Lang::get('msg.tag_name', array(), 'th') }}</label>
                       <br>
                              <select name="txt_tag[]"  multiple class="chosen-select form-control">
                                <option value=""></option>
                                @foreach($tag as $tags => $t)
                                <option value="{{ $t->id }}" 
                                  @foreach($tagcontent as $tagcon =>$tc)
                                  @if($t->id==$tc->tag_id) selected=true @endif
                                  @endforeach
                                  >{{ $t->tag_name }}</option>
                                @endforeach
                              </select>
                    </div>
                      <label for="">{{ Lang::get('msg.content_file',array(), 'th')}}</label>
                      <div id="form_dropzone_edit" class="dropzone" ></div>
                  </div><!-- /.box-body -->
                  <br>
                    <div class="row">
                      <div class="form-group col-md-4">
                        <label for="">ความเร่งด่วน</label>
                         <br>
                          <input type="radio" name="txt_fast" value="0" @if($c->content_fast=="0") checked="true"  @endif> ปกติ &nbsp;&nbsp;
                          <input type="radio" name="txt_fast" value="1" @if($c->content_fast=="1") checked="true"  @endif> ด่วนมาก &nbsp;&nbsp;          
                          <input type="radio" name="txt_fast" value="2" @if($c->content_fast=="2") checked="true"  @endif> ด่วนที่สุด           
                                
                      </div>
                       <div class="form-group col-md-4">
                      <label for="">{{ Lang::get('msg.msg_showhome', array(), 'th') }}</label>
                      <br>
                        <input type="radio" name="txt_showhome" value="1" @if($c->content_showhome=="1") checked="true"  @endif> แสดงหน้าแรก
                        <input type="radio" name="txt_showhome" value="0"  @if($c->content_showhome=="0") checked="true"  @endif> ไม่แสดงหน้าแรก
                      
                              
                    </div>
                     <div class="form-group col-md-4">
                      <label for="">{{ Lang::get('msg.msg_show', array(), 'th') }}</label>
                       <br>
                        <input type="radio" name="txt_show" value="1"  @if($c->content_show=="1") checked="true" @endif> เผยแพร่
                        <input type="radio" name="txt_show" value="0"  @if($c->content_show=="0") checked="true"  @endif> ยังไม่เผยแพร่             
                              
                    </div>
                    </div>
                  <div class="box-footer" >
                    
              </div><!-- /.box -->
              <input type="hidden" name="key" value="{{ $c->content_file }}" >
              <input type="hidden" name="old_picture" value="{{ $c->content_picture }}" >
            </div><!-- /.box-body -->
              <button type="button" id="bt-reset" class="btn btn-danger  pull-right">
              <i class="fa fa-close"></i> {{ Lang::get('msg.msg_cancle',array(), 'th')}}</button>
              <button type="submit" class="btn btn-primary">
              <i class="fa fa-check-circle"></i> {{ Lang::get('msg.msg_submit',array(), 'th')}}
               </button>
                {{ Form::hidden('id',$c->id)}}
                {{ Form::close()}}
                  </div>
                
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
 
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_content',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')
 
         <script type="text/javascript">
                        
      
         var msg = $('#lang').val();
         var status =$('#status').val();
  
            
              $('#status_save').show(0).delay(2000).slideUp();
               
              $('#bt-url').change(function(){
                type = $(this).val();
                if(type==2) {
                $('#content').hide();
                $('#url').show();
              }else if(type==1){
                $('#url').hide();
                $('#content').show();
              }else if(type==3){
                $('#url').hide();
                $('#content').hide();
              }
              });
              $('#bt-reset').click(function(){
                $(this).closest('#form-mainmenu').find("input[type=text], textarea").val("");
                window.location.href='{{ URL::to("backend/content")}}';
              });



        </script>
          <script src="{{ asset('js/dropzone.js') }}"></script>
                <script src="{{ asset('js/libs.js') }}"></script>
          <script src="{{ asset('js/chosen.jquery.js') }}"></script>
           
@stop