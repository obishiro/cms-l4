@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
			 
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ Lang::get('msg.list-item',array(), 'th') }}</h3>
               
               
              

              <div class="box-tools pull-right">
                @if(Session::get('permission')=="1" || Session::get('permission')=="2" || Session::get('permission')=="3" || Session::get('permission')=="5")
               <button href="#" type="button" data-toggle="modal" data-target="#Modal-add" class="btn btn-success"><i class="fa fa-plus-square"></i> {{ Lang::get('msg.msg_add',array(),'th') }}</button>
               @endif
               <div class="modal fade" id="Modal-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" style="width:75%">
                                            <div class="modal-content"  >
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                         <i class="fa fa-plus-square"></i> {{ Lang::get('msg.msg_add',array(),'th') }}                                         
                                                     
                                                </h4>
                                              </div>
                                              <div class="modal-body" style="border-bottom:0px">
                                              	 <div id="alert"></div>
                                              	{{ Form::open(array(
                                              		'method'=>'POST','name'=>'form'
                                              		,'id'=>'form-add','class'=>'form-group'
                                         //         'url'=>'backend/edit/categories'
                                              		), $rules)}}
                                              	<div class="form-group">
                                              	<label for="">{{ Lang::get('msg.categories_name', array(), 'th')}}</label>
                                              	{{Form::input('text', 'txt_name', '', 
                                              	array(
                                              		'class'=>'form-control',
                                                  'data-validetta'=>'required'

                                              		))}}
                                              	</div>
                                                   <div class="form-group">
                                                <label for="">{{ Lang::get('msg.msg_icon', array(), 'th')}}</label>
                                                {{Form::input('text', 'txt_icon', '', 
                                                array(
                                                  'class'=>'form-control'

                                                  ))}}
                                                </div>
                                              
                                                  <br>
                                                   <div class="row">
                       <div class="form-group col-md-12">
                      <label for="">{{ Lang::get('msg.msg_displaytype', array(), 'th') }}</label>
                      <br>
                      <select name="txt_displaytype" id="bt-displaytype" class="form-control">
                        <option></option>
                        <option value="1">{{ Lang::get('msg.msg_picture_type', array(), 'th') }}</option>
                        <option value="2">{{ Lang::get('msg.msg_list_type', array(), 'th') }}</option>
                        <option value="3">{{ Lang::get('msg.msg_embed', array(), 'th') }}</option>
                      </select>
                       
                      
                              
                    </div>
                    <div class="form-group col-md-12">
                     <div class="form-group" style="display:none" id="showdetail">
                      <label for="">{{ Lang::get('msg.content_detail',array(),'th') }}</label>
                         <textarea id="editor1" name="txt_detail" rows="10" cols="80" ></textarea>
                      </div> 
                     </div>
                    </div>
                                                  <br>
                      <div class="row">
                       <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_showhome', array(), 'th') }}</label>
                      <br>
                        <input type="radio" name="txt_showhome" value="1" checked> แสดงหน้าแรก
                        <input type="radio" name="txt_showhome" value="0" > ไม่แสดงหน้าแรก
                      
                              
                    </div>
                   
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_show', array(), 'th') }}</label>
                       <br>
                        <input type="radio" name="txt_show" value="1" checked> เผยแพร่
                        <input type="radio" name="txt_show" value="0" > ยังไม่เผยแพร่             
                              
                    </div>
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_show', array(), 'th') }} Title</label>
                       <br>
                        <input type="radio" name="txt_show_title" value="1" checked> เผยแพร่
                        <input type="radio" name="txt_show_title" value="0" > ยังไม่เผยแพร่             
                              
                    </div>
                    </div>
                                              	
                                              </div>
                                              <div class="modal-footer"  style="border:0px">
                                                
                                               <button type="button" class="btn btn-danger" data-dismiss="modal">
                                               	<i class="fa fa-close"></i>
                                                {{ Lang::get('msg.msg_cancle',array(), 'th')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                            	<i class="fa fa-check-circle"></i>
                                                {{ Lang::get('msg.msg_submit',array(), 'th')}}
                                            </button>
                                            {{ Form::close()}}
                                              </div>
                                                
                                            </div>
                                          </div>
                                        </div>
           </div>
            </div>
            
            <div class="row" >
              <div class="col-md-5 col-sm-6 col-xs-12" 
              @if(Session::has('status'))
              id ="null"
              @endif
              @if(Session::has('save-success'))
               id="status_save" 
              @endif
              @if(Session::has('edit-success'))
               id="status_save" 
              @endif
              @if(Session::has('del-success'))
               id="status_save" 
              @endif
                style="margin-top:10px;margin-left:30%;  display:none" >
                 @if(Session::has('save-success'))
                  <div class="info-box bg-green">
                 @endif
                 @if(Session::has('edit-success'))
                  <div class="info-box bg-teal">
                 @endif
                 @if(Session::has('del-success'))
                  <div class="info-box bg-red-active">
                 @endif
                <span class="info-box-icon">
                  @if(Session::has('save-success'))
                  <i class="fa fa-save"></i>
                  @endif
                  @if(Session::has('del-success'))
                  <i class="fa fa-trash"></i>
                  @endif
                  @if(Session::has('edit-success'))
                  <i class="fa fa-pencil">
                  @endif
                  </i>
                </span>
                <div class="info-box-content">
                
                  <span class="info-box-number">
                    @if(Session::has('save-success'))
                    {{ Lang::get('msg.msg_save_success', array(), 'th') }}
                    @endif
                    @if(Session::has('del-success'))
                    {{ Lang::get('msg.msg_del_success', array(), 'th') }}
                    @endif
                    @if(Session::has('edit-success'))
                    {{ Lang::get('msg.msg_edit_success', array(), 'th') }}
                    @endif
                  </span>
                   </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              @if(Session::has('save-success') || Session::has('del-success') || Session::has('edit-success'))
               </div>
              @endif

              
            </div>
            <div class="box-body">
              
              <table id="Categories_data" class="table table-bordered table-striped">
              	<thead>
             
                   
              		<th width="5%">{{ Lang::get('msg.msg_no', array(), 'th') }}</th> 
              		<th width="50%">{{ Lang::get('msg.categories_name', array(), 'th') }}</th>
                  <th>{{ Lang::get('msg.msg_sort',array(), 'th')}}</th>
                    <th>{{ Lang::get('msg.msg_show',array(), 'th')}}</th>
                    
                    <th>{{ Lang::get('msg.msg_showhome',array(), 'th')}}</th>
              	 
              		<th>{{ Lang::get('msg.msg_tools', array(), 'th') }}</th>
              	</thead>
              </table>
            </div><!-- /.box-body -->
             
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
{{ Session::get('status') }}
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_categories',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')

         <script type="text/javascript">
         $(document).ready(function(){
          $("#form-add").validetta({ 
            display : 'inline',
           errorTemplateClass : 'validetta-inline'});
         });
         var msg = $('#lang').val();
         var status =$('#status').val();


      var my_table= $('#Categories_data').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 50,
            "targets": 0,
            "sAjaxSource": "{{ $api }}",
            columns: [
            {data:'no',name:'no'},
            {data: 'categories_name', name: 'categories_name'},
             {data: 'sort', name: 'sort'},
            {data: 'categories_show', name: 'categories_show'},
           
            {data: 'categories_showhome', name: 'categories_showhome'},
 
            {data: 'tools', name: 'tools'}
        ],
        "fnDrawCallback":function(){
         table_rows = my_table.fnGetNodes(); 
          $.each(table_rows, function(index){
          $("td:first", this).html(index+1);
          });
         }

            });
            
              $('#status_save').show(0).delay(2000).slideUp();
               
 
          $('.topsorting').click(function(){
            alert('top sorting');
          });
                  $('#bt-displaytype').change(function(){
                type = $(this).val();
               
                if(type==2) {
                $('#showdetail').hide();
                 
              }else if(type==1){
                $('#showdetail').hide();
              }else if(type==3){
                $('#showdetail').show();
              }
              });
                  
        </script>
         <script src="{{ asset('js/libs.js') }}"></script>
    
@stop