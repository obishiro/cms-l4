@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">

                                                {{ Form::open(array(
                                                  'method'=>'POST','name'=>'form'
                                                  ,'id'=>'form-add','class'=>'form-group'
                                                  ,'url'=>'backend/edit/categories'
                                                  ), $rules)}}
                                                <div class="form-group">
                                                <label for="">{{ Lang::get('msg.categories_name', array(), 'th')}}</label>
                                                {{Form::input('text', 'txt_name',$name, 
                                                array(
                                                  'class'=>'form-control'

                                                  ))}}
                                                </div>
                                                   <div class="form-group">
                                                <label for="">{{ Lang::get('msg.msg_icon', array(), 'th')}}</label>
                                                {{Form::input('text', 'txt_icon', $icon, 
                                                array(
                                                  'class'=>'form-control',
                                                  'data-validetta'=>'required'

                                                  ))}}
                                                </div>
                                                  <br>
                                         
                                              <div class="form-group">
                                                <label for="">{{ Lang::get('msg.msg_displaytype', array(), 'th') }}</label>
                      <br>
                      <select name="txt_displaytype" id="bt-displaytype" class="form-control">
                        <option></option>
                        <option value="1">{{ Lang::get('msg.msg_picture_type', array(), 'th') }}</option>
                        <option value="2">{{ Lang::get('msg.msg_list_type', array(), 'th') }}</option>
                        <option value="3">{{ Lang::get('msg.msg_embed', array(), 'th') }}</option>
                      </select>
                       </div>
                       <div class="form-group col-md-12">
                     <div class="form-group" style="display:none" id="showdetail">
                      <label for="">{{ Lang::get('msg.content_detail',array(),'th') }}</label>
                         <textarea id="editor1" name="txt_detail" rows="10" cols="80" >{{ $display_embed}}</textarea>
                      </div> 
                     </div>
                                           
                                           
                       <div class="row">
                       <div class="form-group col-md-4">
                      <label for="">{{ Lang::get('msg.msg_showhome', array(), 'th') }}</label>
                      <br>
                        <input type="radio" name="txt_showhome" value="1" @if($categories_showhome=="1") checked="true"  @endif> แสดงหน้าแรก
                        <input type="radio" name="txt_showhome" value="0"  @if($categories_showhome=="0") checked="true"  @endif> ไม่แสดงหน้าแรก
                      
                              
                    </div>
                     <div class="form-group col-md-4">
                      <label for="">{{ Lang::get('msg.msg_show', array(), 'th') }}</label>
                       <br>
                        <input type="radio" name="txt_show" value="1"  @if($categories_show=="1") checked="true" @endif> เผยแพร่
                        <input type="radio" name="txt_show" value="0"  @if($categories_show=="0") checked="true"  @endif> ยังไม่เผยแพร่             
                              
                    </div>
                     <div class="form-group col-md-4">
                      <label for="">{{ Lang::get('msg.msg_show', array(), 'th') }} Title</label>
                       <br>
                        <input type="radio" name="txt_show_title" value="1"  @if($show_title=="1") checked="true" @endif> เผยแพร่
                        <input type="radio" name="txt_show_title" value="0"  @if($show_title=="0") checked="true"  @endif> ยังไม่เผยแพร่             
                              
                    </div>
                    </div>
                                                <div class="form-group">
                                                   <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <i class="fa fa-close"></i>
                                                {{ Lang::get('msg.msg_cancle',array(), 'th')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-check-circle"></i>
                                                {{ Lang::get('msg.msg_submit',array(), 'th')}}
                                            </button>
                                            {{ Form::hidden("id",$id)}}
                                                                                        {{ Form::close()}}
                                                </div>
       </section>
      </div>
@stop

@section('script')

  <script type="text/javascript">
    $(document).ready(function(){
          $('#bt-displaytype').change(function(){
                type = $(this).val();
               
                if(type==2) {
                $('#showdetail').hide();
                 
              }else if(type==1){
                $('#showdetail').hide();
              }else if(type==3){
                $('#showdetail').show();
              }
              });
        });
                    
  </script>

      
               <script src="{{ asset('js/libs.js') }}"></script>
@stop