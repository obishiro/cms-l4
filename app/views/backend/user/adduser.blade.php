@extends('masterbackend')
@section('content')
	     <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             {{ $title}}
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
       <section class="content">
			 
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ Lang::get('msg.msg_add',array(), 'th') }}</h3>
               
               
              

             
            </div>
            
            <div class="row" >
              <div class="col-md-5 col-sm-6 col-xs-12" 
              @if(Session::has('status'))
              id ="null"
              @endif
              @if(Session::has('save-success'))
               id="status_save" 
              @endif
              @if(Session::has('edit-success'))
               id="status_save" 
              @endif
              @if(Session::has('del-success'))
               id="status_save" 
              @endif
                style="margin-top:10px;margin-left:30%;  display:none" >
                 @if(Session::has('save-success'))
                  <div class="info-box bg-green">
                 @endif
                 @if(Session::has('edit-success'))
                  <div class="info-box bg-teal">
                 @endif
                 @if(Session::has('del-success'))
                  <div class="info-box bg-red-active">
                 @endif
                <span class="info-box-icon">
                  @if(Session::has('save-success'))
                  <i class="fa fa-save"></i>
                  @endif
                  @if(Session::has('del-success'))
                  <i class="fa fa-trash"></i>
                  @endif
                  @if(Session::has('edit-success'))
                  <i class="fa fa-pencil">
                  @endif
                  </i>
                </span>
                <div class="info-box-content">
                  <span class="info-box-text">{{ Lang::get('msg.msg_result', array(), 'th') }}</span>
                  <span class="info-box-number">
                    @if(Session::has('save-success'))
                    {{ Lang::get('msg.msg_save_success', array(), 'th') }}
                    @endif
                    @if(Session::has('del-success'))
                    {{ Lang::get('msg.msg_del_success', array(), 'th') }}
                    @endif
                    @if(Session::has('edit-success'))
                    {{ Lang::get('msg.msg_edit_success', array(), 'th') }}
                    @endif
                  </span>
                   </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              @if(Session::has('save-success') || Session::has('del-success') || Session::has('edit-success'))
               </div>
              @endif

              
            </div>
            <div class="box-body">
               <div class="box box-primary">
                 
              
                  {{ Form::open(array(
                    'id'=>'form-content'
                    ,'role'=>'form'
                   ,'enctype'=>'multipart/form-data'
                    ,'url'=>'backend/user/adduser'
                    ), $rules)}}
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                    
                      <div class="row">
                         
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_firstname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_firstname', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_lastname', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_lastname', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                    <div class="row">
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.web_email', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_email', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'email',
                               

                              ))}}
                    </div>
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.web_tel', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_tel', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                    </div>
                     <div class="row">
                       <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.username', array(), 'th') }}</label>
                      {{Form::input('text', 'txt_username', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required'

                              ))}}
                    </div>
                     <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.password', array(), 'th') }}</label>
                      {{Form::input('password', 'txt_password', '', 
                         array(
                               'class'=>'form-control',
                               'data-validetta'=>'required,minLength[6]'

                              ))}}
                    </div>
                    
                    </div>
                    <div class="row">
                      <div class="form-group col-md-6">
                     <label for="">{{ Lang::get('msg.maindepart', array(), 'th') }}</label>
                      <select name="txt_maindepart" id="maindepart" class="form-control">
                        <option value="0"></option>
                        @foreach($maindepart as $maindep=>$mp)
                        <option value="{{$mp->id}}">{{$mp->maindepart_name}}</option>
                        @endforeach
                      </select>
                   </div>
                    <div class="form-group col-md-6">
                     <label for="">{{ Lang::get('msg.subdepart', array(), 'th') }}</label>
                     <select name="txt_subdepart" id="subdepart" class="form-control"></select>
                   </div>
                   
                   </div>

                     <div class="row">
                         <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_usertype', array(), 'th') }}</label>
                        <select name="txt_usertype" id="usertype" class="form-control" data-validetta="required">
                          <option value=""></option>
                          <option value="1">{{ Lang::get('msg.msg_administrator',array(),'th') }}</option>
                          <option value="2">{{ Lang::get('msg.msg_public_user',array(),'th') }}</option>
                        </select>
                        </div>
                         <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.content_picture', array(), 'th') }}</label>
                         <input type="file" name="picture">
                        </div>
                        </div>
                       <div class="row">
                         
                         <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_permission', array(), 'th') }}</label>
                      <br>
                          <input type="checkbox" name="permission[]" value="1" class="menu_permission">
                           {{ Lang::get('msg.msg_allpermission', array(), 'th') }}
                            <input type="checkbox" name="permission[]" value="2" class="menu_permission">
                           {{ Lang::get('msg.msg_addpermission', array(), 'th') }}
                            <input type="checkbox" name="permission[]" value="3" class="menu_permission">
                           {{ Lang::get('msg.msg_editpermission', array(), 'th') }}
                            <input type="checkbox" name="permission[]" value="4" class="menu_permission">
                           {{ Lang::get('msg.msg_delpermission', array(), 'th') }}
                            <input type="checkbox" name="permission[]" value="5" class="menu_permission">
                           {{ Lang::get('msg.msg_readpermission', array(), 'th') }}
                           
                        </div>
                          <div class="form-group col-md-6">
                      <label for="">{{ Lang::get('msg.msg_show', array(), 'th') }}</label>
                       <br>
                        <input type="radio" name="txt_show" value="1" checked> เผยแพร่
                        <input type="radio" name="txt_show" value="0" > ยังไม่เผยแพร่             
                              
                    </div>
                        
                        </div>
                       
                      </div>
                      
                    </div>
                    
                    
                

                 </div><!-- /.box-body -->

                  <div class="box-footer" >
                     <button type="button" id="bt-reset" class="btn btn-danger  pull-right">
              <i class="fa fa-close"></i> {{ Lang::get('msg.msg_cancle',array(), 'th')}}</button>
              <button type="submit" class="btn btn-primary ">
              <i class="fa fa-check-circle"></i> {{ Lang::get('msg.msg_submit',array(), 'th')}}
               </button>
              </div><!-- /.box -->
              <input type="hidden" name="key" value="{{ Str::random(16,'numberic') }}" >
          
            </div><!-- /.box-body -->
             
                
              {{ Form::close()}}
                  </div>
                
          </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
 
   <input type="hidden" id="lang" value="{{ Lang::get('msg.msg_input_content',array(),'th') }}">
   <input type="hidden" id="status" name="status" value="{{ Session::get('status') }}">

@stop
@section('script')

         <script type="text/javascript">
             $(document).ready(function(){
          $("#form-content").validetta({ 
            display : 'inline',
           errorTemplateClass : 'validetta-inline'});
         });              
       
         var msg = $('#lang').val();
         var status =$('#status').val();
          $('#usertype').change(function(){
            type = $(this).val();
            if(type==1){
              $('.menu_system').prop('disabled',true);
              $('.menu_permission').prop('disabled',true);
              $('.button-add-permission').prop('disabled',true);
            }else if(type==2){
              $('.menu_system').prop('disabled',false);
              $('.menu_permission').prop('disabled',false);
              $('.button-add-permission').prop('disabled',false);
            }else{
              return false;
            }
          });
            
              $('#status_save').show(0).delay(2000).slideUp();
               
               
              $('#bt-reset').click(function(){
               
                window.location.href='{{ URL::to("backend/user")}}';
              });

              $('#maindepart').change(function(){
                  var MID =$(this).val();
                  $('#subdepart').load('/backend/datasubdepart/'+MID,{
                    ajax:true,id:$(this).val()
                  });
              });



        </script>

          <script src="{{ asset('js/libs.js?='.Str::random(8,'numberic').'')}}"></script>
          <script src="{{ asset('js/dropzone.js?='.Str::random(16,'numberic').'') }}"></script>
           <script src="{{ asset('js/chosen.jquery.js') }}"></script>
      
         
@stop
