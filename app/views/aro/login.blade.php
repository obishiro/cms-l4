<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ARO System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::to('css/bootstrap.min.css') }} ">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::to('css/font-awesome.min.css') }} ">
    <!-- Ionicons -->
  
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('css/AdminLTE.min.css') }} ">
    <!-- iCheck -->
 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">ระบบสนับสนุนกิจการสายงานสัสดี <span class="small">กองการสัสดี หน่วยบัญชาการรักษาดินแดน &copy; 2016 All rights reserved</span></a>
    </div>

   
  </div><!-- /.container-fluid -->
</nav>
    <div class="login-box">
      
      <div class="login-box-body">
       <!-- <p class="login-box-msg">Sign in to start your session</p>-->
       <center>
       <img src="{{URL::to('img/logo-aro-lg.png')}}" style="width:60%">
       </center>
       <p></p>
        {{ Form::open(array('name'=>'frm_login','id'=>'FrmLogin','method'=>'POST','url'=>'login'))}}
          <div class="form-group has-feedback @if ($errors->has('username')) has-error @endif">
            @if ($errors->has('username'))
            <label class="control-label" for="inputError">
              <span class="glyphicon glyphicon-question-sign"></span> {{ Lang::get('msg.txt-please-input',array(),'th')}}
            </label>
            @endif
            <input type="text"   class="form-control username" name="username" placeholder="{{ Lang::get('msg.username',array(),'th')}}">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            
          </div>
          
          <div class="form-group has-feedback @if ($errors->has('password')) has-error @endif">
            @if ($errors->has('username'))
            <label class="control-label" for="inputError">
              <span class="glyphicon glyphicon-question-sign"></span> {{ Lang::get('msg.txt-please-input',array(),'th')}}
            </label>
            @endif
            <input type="password" name="password" class="form-control" placeholder="{{ Lang::get('msg.password',array(),'th')}}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
           
          <div class="row">
             
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-lg btn-block btn-flat">
                <span class="glyphicon glyphicon-log-in"></span> 
                {{ Lang::get('msg.signin',array(),'th')}}
              </button>
            </div><!-- /.col -->

          </div>

        {{ Form::close()}}
        
        @if(Session::has('error_login_incorrect'))
        <div class="callout callout-danger" style="margin-top:10px"><h4>
          <span class="glyphicon glyphicon-question-sign"></span>
           {{ Lang::get('msg.error_login_incorrect',array(),'th')}}</h4></div>
        @endif            
                  
        <!--
          '1001'
          'bkk1001'
          ; }} -->
       
        <!--
        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>-->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ URL::to('plugins/jQuery/jQuery-2.1.4.min.js') }} "></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ URL::to('js/bootstrap.min.js') }} "></script>
    <!-- iCheck -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
      <script src="{{ asset('js/jquery.validate.laravel.js') }}"></script>
      <script type="text/javascript">
      $(document).ready(function() {
        $('.username').focus();
      });
      </script>
   
  </body>
</html>
