     <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>ARO</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>{{ Lang::get('msg.aro_system_name_en',array(),'th')}}</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          

          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
              <li class="pull-left">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 {{ Lang::get('msg.aro_system_name_th',array(),'th')}}
                   
                </a>
                 </li>
               <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ URL::to('uploadfiles/users/thumb',Session::get('photoURL'))}}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ Session::get('firstName')}} {{Session::get('lastName')}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ URL::to('uploadfiles/users',Session::get('photoURL'))}}" class="img-circle" alt="User Image">
                    <p>
                   {{ Session::get('firstName')}} {{Session::get('lastName')}}
                      <small>{{ Lang::get('msg.config-member_sign',array(),'th') }} 
                        {{ Helpers::DateFormat(Auth::user()->created_at) }}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    
                       <a href="{{ URL::to('backend/user/edituser',array(Auth::user()->id)) }}">
                    <i class="fa fa-user"></i> {{ Lang::get('msg.config-profile',array(),'th') }}
                    </a>
                 
                       <a href="{{ URL::to('backend/config/password') }}">
                    <i class="fa fa-lock"></i> {{ Lang::get('msg.config-password',array(),'th') }}
                    </a>
                 
                 
                  </li>
                 
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div  >
                      <a href="{{ URL::to('backend/logout') }}" class="btn btn-block btn-flat btn-danger"><i class="fa fa-power-off"></i> <span>{{ Lang::get('msg.msg_logout',array(),'th') }}</span></a>
                    </div>
                  </li>
                </ul>
              </li>
          
              
            </ul>
          </div>

        </nav>
      </header>