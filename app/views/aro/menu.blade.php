      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            
            
          </div>
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           
      
            <li>
              <a href="{{ URL::to('aro/m16') }}">
                <i class="fa fa-circle-o text-blue" aria-hidden="true"></i> <span>การลงบัญชีฯ ตาม ม.16</span>
              </a>
             
            </li>
            <li>
              <a href="{{ URL::to('aro/m25') }}">
                <i class="fa fa-circle-o text-blue" aria-hidden="true"></i> <span>การรับหมายเรียกฯ ตาม ม.25</span>
              </a>
             
            </li>


            
            
            <li><a href="#" id="btn-exit"><i class="fa fa-power-off text-red"></i> <span>{{ Lang::get('msg.msg_logout',array(),'th') }}</span></a></li>
            
          </ul>

        </section>
        <!-- /.sidebar -->
      </aside>
