@extends('masterbackend_aro')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-institution text-green"></i>
      {{ Session::get('name')}}
       
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Examples</a></li>
      <li class="active">Blank page</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-edit text-yellow"></i> รายงานการลงบัญชีฯ ตาม ม.16 {{ Session::get('name')}}</h3>
        <div class="box-tools pull-right">
   
        </div>
      </div>
      <div class="box-body">
        <form role="form" id="fupForm">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            
        @if($count_thai_man==0)
        
        <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">ข้อมูลชายไทยตามทะเบียนราษฎร์</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                    <label class="text-red">พ.ศ. เกิด</label>
                  <input type="text" name="thai_birth" class="form-control number" placeholder="" value="{{$thai_year}}" required>
                </div>
                <div class="col-xs-4">
                    <label class="text-red">จำนวนชายไทยตามทะเบียนราษฎร์</label>
                  <input type="text" class="form-control number" name="thai_man" placeholder="" required>
                </div>
                
              </div>
            </div><!-- /.box-body -->
          </div><!-- /.box -->

          @else
          <input type="hidden" name="thai_number" value="{{$thai_number}}">
          <input type="hidden" name="thai_birth" value="{{$thai_birth}}">
                
        @endif
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">ข้อมูลการลงบัญชีฯ ตาม ม.16 ประจำปี พ.ศ. {{$now_year}}</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                    <label class="text-blue">ประจำเดือน</label>
                  <select class="form-control" name="month_add">
                    {{Helpers::GetMonth()}}
                  </select>
                </div>
                <div class="col-xs-3">
                    <label class="text-blue">รับลงบัญชีฯ แล้ว</label>
                  <input type="text" class="form-control number" name="add_number" placeholder="" required>
                </div>
                <div class="col-xs-3">
                    <label class="text-blue">ลงบัญชีฯ ที่อื่น</label>
                  <input type="text" class="form-control number" placeholder="" name="add_another" required>
                </div>
                <div class="col-xs-3">
                    <label class="text-blue">รับลงบัญชีฯ จากที่อื่น</label>
                  <input type="text" class="form-control number" name="add_from_another" placeholder="" required>
                </div>
                
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-floppy-o"></i> บักทึกข้อมูล</button>
                <button type="reset" class="btn btn-danger btn-lg"><i class="fa fa-power-off"></i> ยกเลิก</button>
              </div>
            </form>
          </div><!-- /.box -->
        
      </div><!-- /.box-body -->
     
    </div><!-- /.box -->
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-database text-blue"></i> ข้อมูลการลงบัญชีฯ
          ชายไทยตามทะเบียนราษฎร์ จำนวน {{number_format($thai_man)}} คน {{$add_total}}</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                <th style="text-align: center">ลำดับ</th>
                  <th style="text-align: center">เดือน</th>
                  <th style="text-align: center">รับลงบัญชีฯ แล้ว</th>
                  <th style="text-align: center">คงเหลือ</th>
                  <th style="text-align: center">ลงบัญชีฯ ที่อื่น</th>
                  <th style="text-align: center"> รับลงบัญชีฯ จากที่อื่น
                    <th style="text-align: center"> รวม
                </th>
                </tr>
                </thead>
                <tbody>
                    <?php $i=1;?>
                    @foreach($data_m16 as $datam=>$d)
                    <tr>
                        <td align="center">{{$i}}</td>
                        <td align="center">{{Helpers::Cmonth($d->month_add)}}</td>
                        <td align="center">{{$d->add_number}}</td>
                        <td align="center">{{$thai_man-$d->add_number}}</td>
                        <td align="center">{{$d->add_another}}</td>
                        <td align="center">{{$d->add_from_another}}</td>
                        
                        <td align="center"></td>
                    </tr>
                    <?php $i++;?>
                    @endforeach
                </tbody>
                   
            </table>
        </div>
      </div><!-- /.box -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop

@section('script')
<script type="text/javascript">

    $(document).ready(function(e){
        $(function () {
    
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
 
$("#fupForm").on('submit', function(e){
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '{{URL::to("aro/add-thai-man")}}',
        data: new FormData(this),
        dataType: 'json',
        contentType: false,
        cache: false,
        processData:false,
        beforeSend: function(){
            $('.submitBtn').attr("disabled","disabled");
            $('#fupForm').css("opacity",".5");
        },
        success: function(response){ //console.log(response);
            $('.statusMsg').html('');
            if(response.status == 1){
      
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'บันทึกข้อมูลเรียบร้อยแล้ว',
                showConfirmButton: false,
                timer: 2500
                })
                $('#fupForm')[0].reset();
                setTimeout(function(){
                location.reload(true);
                },2600);
            }else{
                 
            }
            $('#fupForm').css("opacity","");
            $(".submitBtn").removeAttr("disabled");
        }
    });
});
    });
</script>
 


@stop

@stop