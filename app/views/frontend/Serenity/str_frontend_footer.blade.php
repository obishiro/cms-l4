  <!-- JavaScript Library Files -->
  <script src="{{ URL::to('frontend/Serenity/assets/js/jquery.min.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/jquery.easing.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/google-code-prettify/prettify.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/modernizr.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/bootstrap.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/jquery.elastislide.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/sequence/sequence.jquery-min.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/sequence/setting.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/jquery.prettyPhoto.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/application.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/jquery.flexslider.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/jquery.nivo.slider.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/hover/jquery-hover-effect.js')}}"></script>
  <script src="{{ URL::to('frontend/Serenity/assets/js/hover/setting.js')}}"></script>

  <!-- Template Custom JavaScript File -->
  <script src="{{ URL::to('frontend/Serenity/assets/js/custom.js')}}"></script>
  <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
          effect:'random',
         pauseOnHover:true
        });
    });
    </script> 

</body>
</html>