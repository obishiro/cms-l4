<div class="span3">
          <aside>
          @foreach($Mainmenu_right as $mainmenuright =>$mr)
          @if($mr->mainmenu_type=="1") 
            <div class="widget">
              <h4><a href="{{ URL::to('view/menu',$mr->m_url)}}" >{{ $mr->mainmenu_name }}</a></h4>
           
            </div>
            @elseif($mr->mainmenu_type=="2")
            <div class="widget">
              <h4><a href="{{ $mr->mainmenu_url }}">{{ $mr->mainmenu_name }}</a></h4>
                       </div>
 @elseif($mr->mainmenu_type=="3")
            <div class="widget">
              <h4>{{ $mr->mainmenu_name }}</h4>
              <ul class="cat">
              <?php
                $Submenu = Submenu::where(array('submenu_categories'=>$mr->id,'submenu_show'=>'1'))->orderBy('submenu_sorting','asc')->get(); 
                ?>
                 @foreach($Submenu as $submenu =>$sm)
                       @if($sm->submenu_type=="1")
                          @if($sm->submenu_picshow !="0")
                          <a href="{{ URL::to('submenu',$sm->s_url)}}">
                            <img src="{{URL::to('uploadfiles/picmenu',$sm->submenu_pic)}}"  class="zoom" style="width:350px;height:80px;padding-bottom:5px">
                           
                          </a>
                          @else
						  <ul class="unstyled">
                          <li><a href="{{ URL::to('submenu',$sm->s_url)}}">
                          <i class="icon-chevron-right"></i>
                              {{ $sm->submenu_name}}</a>
                              </li> 
							  </ul>
                          @endif
                       @elseif($sm->submenu_type=="2")
                          @if($sm->submenu_picshow !="0")
                          <a href="{{ $sm->submenu_url }}">
                            <img src="{{URL::to('uploadfiles/picmenu',$sm->submenu_pic)}}" class="zoom" style="width:350px;height:80px;padding-bottom:5px">
                            <br>
                          </a>
                          @else
						  <ul class="unstyled">
                          <li ><a href="{{  $sm->submenu_url }}">
						  
                          <i class="icon-chevron-right"></i>
                              {{ $sm->submenu_name}}</a>
                              </li> 
							  </ul>
                          @endif
                       @endif
                 @endforeach                      
               
              @elseif($mr->mainmenu_type=="4") {{-- Embed --}}
            <div class="widget">
              <h4>{{ $mr->mainmenu_name }}</h4>
              <ul class="recent-posts">
                <li><a href="#"> {{ $mr->mainmenu_embed }}</a>
                  <div class="clear">
                  </div>
                  
                </li>
                
              </ul>
            </div>
            <!-- <div class="widget">
              <h4>Tags</h4>
              <ul class="tags">
                <li><a href="#" class="btn">Tutorial</a></li>
                <li><a href="#" class="btn">Tricks</a></li>
                <li><a href="#" class="btn">Design</a></li>
                <li><a href="#" class="btn">Trends</a></li>
                <li><a href="#" class="btn">Online</a></li>
              </ul>
            </div> -->
              @endif
            
           
         
          @endforeach
          <!-- @yield('right') -->
</div>
          </aside>
        </div>
      </div>
    </div>
  </section>