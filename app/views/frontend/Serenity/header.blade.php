  <header>
    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <!-- logo -->
          <a class="brand logo" href="{{URL::to('/')}}"><img src="{{ URL::to('img/logo-aro.png')}}"  alt=""></a>
         <a class="brand" href="{{URL::to('/')}}"><h2>{{ $dataenv->web_name_lo}}</h2></a> 
          <!-- end logo -->
          <!-- top menu -->
          <div class="navigation">
            <nav>
              <ul class="nav topnav">
                <li class="dropdown ">
                  <a href="{{URL::to('/')}}">หน้าแรก</a>
                </li>
                @foreach($mainmenu_top as $menu =>$m)
                    @if($m->mainmenu_type=="1")
                        <li><a href="{{ URL::to('menu',array($m->m_url))}}">{{$m->mainmenu_name}}</a></li>
                        @elseif($m->mainmenu_type=="2")
                        <li><a href="{{ $m->mainmenu_url}}">{{$m->mainmenu_name}}</a></li>
                        @elseif($m->mainmenu_type=="3")
                            <?php   $num = Mainmenu::where('parent_id',$m->id)->count(); ?>
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        {{$m->mainmenu_name}} <span class="caret"></span></a>
                             <ul class="dropdown-menu">
                                <?php 
                                $Submenu = Submenu::where(array('submenu_categories'=>$m->id,'submenu_show'=>'1'))->orderBy('submenu_sorting','asc')->get(); 
                                ?>
                                @foreach($Submenu as $submenu =>$sm)
                                @if($sm->submenu_type=="1")
                                <li><a href="{{ URL::to('submenu',$sm->s_url)}}">
                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                {{ $sm->submenu_name}}</a>
                                </li>  
                                @elseif($sm->submenu_type=="2")
                                <li><a href="{{ $sm->submenu_url}}" target="_blank"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                {{ $sm->submenu_name}}</a></li>  
                                @endif
                                @endforeach
                              </ul>              
                        </li>
                    @endif
@endforeach
               
              </ul>
            </nav>
          </div>
          <!-- end menu -->
        </div>
      </div>
    </div>
  </header>
  <section id="intro">
    <div class="jumbotron masthead">
      <div class="container">
        <!-- slider navigation -->
        <div class="sequence-nav">
          <div class="prev">
            <span></span>
          </div>
          <div class="next">
            <span></span>
          </div>
        </div>
        <!-- end slider navigation -->
        <div class="row">
          
          <div class="span12">
            <div class="slider-wrapper theme-default" >
              <div id="slider" class="nivoSlider"> 
                @foreach($Banner as $banner => $b)
              <a href="{{ URL::to('slide',$b->banner_url)}}">
              <img  src="{{ URL::to('uploadfiles/banner',$b->files_newname)}}" data-thumb="{{ URL::to('uploadfiles/banner',$b->files_newname)}}" alt=""  />
              </a> 
              @endforeach
              </div>
            <!-- Sequence Slider::END-->
          </div>
        </div>
      </div>
    </div>
  </section>