@extends('masterfrontend',['mainmenu_top'=>$Mainmenu_top])
@section('title',$env->web_name_lo)
@section('description',$env->web_detail)
@section('keyword',$env->web_keyword)
@section('content')
<section id="maincontent">
    <div class="container">
      <div class="row">
        <div class="span9">
          <!-- start article full post -->
         
          @foreach($Categories as $cat=>$c)
                          @if($c->display_type==1)
                          @include('viewmodule/'.Config::get('app.template').'/display_pic', array('id'=>$c->id,'title'=>$c->categories_name,'show'=>$c->show_title,'icon'=>$c->categories_icon,'url'=>$c->categories_url,'mainurl'=>$env->web_url))
                          @elseif($c->display_type==2)
                          @include('viewmodule/'.Config::get('app.template').'/display_list', array('id'=>$c->id,'title'=>$c->categories_name,'show'=>$c->show_title,'icon'=>$c->categories_icon,'url'=>$c->categories_url,'mainurl'=>$env->web_url))

                          @elseif($c->display_type==3)
                          @include('viewmodule/'.Config::get('app.template').'/display_embed', array('id'=>$c->id,'title'=>$c->categories_name,'show'=>$c->show_title,'media'=>$c->display_embed,'icon'=>$c->categories_icon,'url'=>$c->categories_url,'mainurl'=>$env->web_url))
                              
                          @endif
                    @endforeach

 
        </div>
 
    
  @stop
 