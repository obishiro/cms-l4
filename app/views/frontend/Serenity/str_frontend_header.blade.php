<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
   <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keyword" content="@yield('keyword')">
    <meta name="viewport" content="width=device-width">

  <!-- styles -->
  <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/bootstrap.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/bootstrap-responsive.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/docs.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/prettyPhoto.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/js/google-code-prettify/prettify.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/flexslider.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/sequence.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/style.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/nivo-slider.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/css/default/default.css')}}" rel="stylesheet">
  <link href="{{ URL::to('frontend/Serenity/assets/color/default.css')}}" rel="stylesheet">

  <!-- fav and touch icons -->
  <!-- <link rel="shortcut icon" href="{{ URL::to('frontend/Serenity/assets/ico/favicon.ico')}}"> -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::to('frontend/Serenity/assets/ico/apple-touch-icon-144-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::to('frontend/Serenity/assets/ico/apple-touch-icon-114-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::to('frontend/Serenity/assets/ico/apple-touch-icon-72-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" href="{{ URL::to('frontend/Serenity/assets/ico/apple-touch-icon-57-precomposed.png')}}">

  <!-- =======================================================
    Theme Name: Serenity
    Theme URL: https://bootstrapmade.com/serenity-bootstrap-corporate-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
  
</head>

<body>