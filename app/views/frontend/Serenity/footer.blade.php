 <!-- Footer
 ================================================== -->
 <footer>
     
    <div class="verybottom">
      <div class="container">
        <div class="row">
          <div class="span12">
            <p>
              &copy; {{$env->web_name_lo}} 
               
              {{$env->web_address}} 
              {{$env->web_tel}}<br>
                All right reserved Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </p>
          </div>
          
        </div>
      </div>
    </div>
  </footer>


   