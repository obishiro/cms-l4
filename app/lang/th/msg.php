<?php

return array(
	'system_name' 			=> 'Easysite',
	'aro_system_name_en' 			=> 'ARO System',

	'aro_system_name_th' 			=> 'ระบบสนับสนุนกิจการสายงานสัสดี',
	'txt-please-input' 		=>'กรุณากรอกข้อมูล',
	'username'			=> 'ชื่อผู้ใช้',
	'password'			=> 'รหัสผ่าน',
	'signin'				=> 'เข้าสู่ระบบ',
	

	/////////// process display  //////////
	'error_login_incorrect' 		=> 'ชื่อผู้ใช้ และ รหัสผ่าน ผิด!',
	'msg_add'			=> 'เพิ่มข้อมูลใหม่',
	'msg_edit'			=> '',
	'msg_del'			=> '',
	'msg_error'			=> 'มีข้อผิดพลาด',
	'msg_cancle'		=> 'ยกเลิก',
	'msg_submit'		=> 'บันทึกการเปลี่ยนแปลง',
	'msg_save_success'		=> 'บันทึกข้อมูลเรียบร้อย',
	'msg_edit_success'		=> 'แก้ไขข้อมูลเรียบร้อย',
	'msg_del_success'		=> 'ลบข้อมูลเรียบร้อย',
	'msg_no'				=> 'ลำดับ',
	'msg_show'				=> 'แสดง',
	'msg_unshow'			=> 'ไม่แสดง',
	'msg_created'			=> 'บันทึกเมื่อ',
	'msg_updated'			=> 'แก้ไขเมื่อ',
	'msg_tools'				=> 'เครื่องมือ',
	'msg_confirm'			=> 'ยืนยันการทำงาน ?',
	'msg_result'			=> 'ผลการทำงาน',
	'msg_logout'			=> 'จบการทำงาน',
	'msg_type'				=> 'ประเภท',
	'msg_content'			=> 'แบบเนื้อหา',
	'msg_url'				=> 'แบบลิงค์เชื่อมโยง',
	 'msg_embed'			=> 'แบบฝั่งโค้ด',
	 'msg_submenu'			=> 'เมนูย่อย',
	 'msg_subdepart'			=> 'หน่วยงานย่อย',
	'msg_sort'				=> 'เรียงลำดับ',
	'msg_position'			=> 'ตำแหน่ง',
	'msg_position_top'		=> 'ด้านบน',
	'msg_position_right'	=> 'ด้านขวา',
	'msg_position_left'		=> 'ด้านซ้าย',
	'msg_download'			=> 'ดาวน์โหลดไฟล์/เอกสารประกอบ',
	
	/////////// Main Menu ////////
	'dashboard'		=> 'ตั้งค่าระบบ',
	'config-env'		=> 'ตั้งค่าพื้นฐาน',
	'config-categories'	=> 'ตั้งค่าหมวดหมู่',
	'config-tag'		=> 'ตั้งค่าแท๊ก',
	'config-prefix'		=> 'ตั้งค่าคำนำหน้า',
	'config-banner'		=> 'ตั้งค่าป้ายแบนเนอร์',
	'config-slide'		=> 'ตั้งค่าป้ายสไลด์',
	'slide_name'		=> 'ชื่อสไลด์',
	'config-password'		=> 'เปลี่ยนรหัสผ่าน',
	'config-profile'		=> 'แก้ไขข้อมูลส่วนตัว',
	'config-member_sign'	=> 'เป็นสมาชิกเมื่อ',
	'msg_old_password'		=> 'รหัสผ่านเดิม',
	'msg_new_password'		=> 'รหัสผ่านใหม่',
	'list-item'		=> 'รายการ',
	'mainmenu'		=> 'เมนูหลัก',
	'submenu'		=> 'เมนูย่อย',
	'maindepart'		=> 'หน่วยงานหลัก',
	'subdepart'		=> 'หน่วยงานย่อย',
	'categories_name' => 'ชื่อหมวดหมู่',
	'tag_name' => 'ชื่อแท๊ก',
	'prefix_name' => 'ชื่อคำนำหน้า',
	'mainmenu_name' => 'ชื่อเมนูหลัก',
	'maindepart_name' => 'ชื่อหน่วยงานหลัก',

	'msg_input_categories' => 'Categories name is required',
	'msg_input_tag' => 'Tag name is required',
	'msg_input_mainmenu' => 'This field is required',

	/////////// Content  ///////////
	'content'		=> 'บทความ',
	'content_name'		=> 'ชื่อบทความ',
	'content_author'		=> 'Author name',
	'content_year'		=> 'Year',
	'content_categories'	=> 'หมวดหมู่',
	'content_detail'		=> 'รายละเอียด',
	'content_file'		=> 'ไฟล์',
	'content_type'		=> 'ประเภท',
	'content_view'		=> 'อ่าน',
	'msg_showhome'		=> 'หน้าหลัก',
	'content_picture'	=> 'รูปภาพ',
	'content_alldetail' => 'รายละเอียดทั้งหมด',

	/////////////////////// Enviroment ///////////////////
	'web_name_lo'		=> 'ชื่อเว็บไซต์ภาษาไทย',
	'web_name_en'		=> 'ชื่อเว็บไซต์ภาษาอังกฤษ (ถ้ามี)',
	'web_address'		=> 'ที่อยู่',
	'web_tel'			=> 'เบอร์โทรติดต่อ',
	'web_email'			=> 'อีเมล์',
	'web_keyword'		=> 'เว็บคีย์เวิร์ด',
	'web_detail'		=> 'รายละเอียดเว็บไซต์',
	'subcategories'		=> 'หมวดหมู่ย่อย',
	'parent_item'		=> 'รายการบนสุด',

	/////////////// User //////////////////////

	'msg_user'			=> 'จัดการผู้ใช้งาน',
	'msg_firstname'		=> 'ชื่อผู้ใช้งาน',
	'msg_lastname'		=> 'นามสกุลผู้ใช้งาน',
	'msg_usertype'		=> 'ประเภทผู้ใช้งาน',
	'msg_system'		=> 'ระบบ',
	'msg_permission'	=> 'สิทธิ์การใช้งาน',
	'msg_allpermission'	=> 'ทั้งหมด',
	'msg_addpermission'	=> 'เพิ่มข้อมูล',
	'msg_editpermission'	=> 'แก้ไขข้อมูล',
	'msg_delpermission'	=> 'ลบข้อมูล',
	'msg_readpermission'	=> 'อ่านอย่างเดียว',
	'msg_addmenupermission'	=> 'เพิ่มสิทธิ์การใช้งาน',
	'msg_administrator'	=> 'ผู้ดูแลระบบ',
	'msg_public_user'	=> 'ผู้ใช้งานทั่วไป',
	'input_file'		=>	'อัปโหลดไฟล์',




/////////////////// gallery //////////////
	'gallery'				=> 'อัลบั้มรูป',
	'gallery_name'			=> 'ชื่ออัลบั้มรูป',
	'config-theme'			=> 'รูปแบบหน้าหลัก',
	'config-css'			=> 'ตั้งค่า CSS',
	'msg_displaytype'		=> 'รูปแบบการแสดงผล',
	'msg_picture_type'		=> 'แบบแสดงรูปภาพ',
	'msg_list_type'			=> 'แบบลิสรายการ',
	'msg_icon'			=> 'ไอคอน fontawesome',
	


 

	);