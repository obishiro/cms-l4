<?php

class Subdepart extends \Eloquent {
	protected $table = 'tb_subdepart';
	public $timestamps = true;

	public function categories_name ()
    	{
        	return $this->hasOne('Maindepart');
    	}
}