<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
//Route::get('/{url}', 'HomeController@index');
// Route::get('/', function(){
//     return View::make('masterfrontend');
// });
Route::get('signup', function(){
	 return View::make('frontend.signup')->with('title','Signup');
});

Route::get('auth/loginfacebook','AuthController@getLoginfacebook');
Route::group(array('prefix'=>'backend','before' => 'auth'), function()
{
     
    Route::get('backend','BackendController@index');
    Route::get('logout','BackendController@getLogout');

    // Route::get('config/{type}',function($type){
    	 
    // 	return Redirect::to('config/categories');
    // })->where(array('type' =>'[A-Za-z]+'));
    Route::get('config/{type}', 'BackendController@getConfig')->where(array('type' =>'[A-Za-z]+'));
    Route::post('config/{type}', 'BackendController@postConfig')->where(array('type' =>'[A-Za-z]+'));
    Route::post('menu/{type}', 'BackendController@postConfig')->where(array('type' =>'[A-Za-z]+'));

    Route::get('menu/{type}', 'MenuContoller@getMenu')->where(array('type' =>'[A-Za-z]+'));
    Route::get('menu/add/{type}', 'MenuContoller@getAdd')->where(array('type' =>'[A-Za-z]+'));
    Route::get('menu/edit/{type}/{id}', 'MenuContoller@getEdit')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::post('menu/add/{type}', 'MenuContoller@postAdd')->where(array('type' =>'[A-Za-z]+'));
    Route::post('menu/edit/{type}', 'MenuContoller@postEdit')->where(array('type' =>'[A-Za-z]+'));
    
    Route::get('depart/{type}', 'DepartContoller@getDepart')->where(array('type' =>'[A-Za-z]+'));
    Route::get('depart/add/{type}', 'DepartContoller@getAdd')->where(array('type' =>'[A-Za-z]+'));
    Route::get('depart/edit/{type}/{id}', 'DepartContoller@getEdit')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    
    Route::post('depart/add/{type}', 'DepartContoller@postAdd')->where(array('type' =>'[A-Za-z]+'));
    Route::post('depart/edit/{type}', 'DepartContoller@postEdit')->where(array('type' =>'[A-Za-z]+'));
    

    Route::get('data/categories','ApiController@getDatacategories');
    Route::get('data/tag','ApiController@getDatatag');
    Route::get('data/prefix','ApiController@getDataprefix');
    Route::get('data/mainmenu','ApiController@getDatamainmenu');
    Route::get('data/submenu','ApiController@getDatasubmenu');
    Route::get('data/maindepart','ApiController@getDatamaindepart');
    Route::get('data/subdepart','ApiController@getDatasubdepart');
    Route::get('data/content','ApiController@getDatacontent');
    Route::get('data/gallery','ApiController@getDatagallery');
    Route::get('data/banner','ApiController@getDatabanner');
    Route::get('data/user','ApiController@getDatauser');

    
    Route::post('datasubdepart/{id}','ApiController@postSubdepart');
    

    Route::get('del/{type}/{id}','BackendController@getDel')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::get('show/{type}/{id}','BackendController@getShow')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::get('unshow/{type}/{id}','BackendController@getUnshow')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::post('edit/{type}/','BackendController@postEdit')->where(array('type' =>'[A-Za-z]+'));
    Route::get('edit/{type}/{id}','BackendController@getEdit')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));

    Route::get('content','ContentController@index');
    Route::get('content/addcontent','ContentController@getAdd');
    Route::get('content/editcontent/{id}','ContentController@getEdit')->where(array('id'=>'[0-9]+'));
    Route::post('content/addcontent','ContentController@postAdd');
    Route::post('content/editcontent','ContentController@postEdit');
    Route::post('content/addfile/{key}','ContentController@postAddfile');
    Route::post('content/editfile/{key}','ContentController@postEditfile');
    Route::post('content/dropimages/{key}','ContentController@postDropimages');
    Route::post('ckupload','ContentController@postCkupload');
    Route::post('data/submenu/{id}', 'ApiController@postDataSubmenu')->where(array('id'=>'[0-9]+'));

    Route::get('gallery','GalleryController@index');
    Route::get('gallery/addgallery','GalleryController@getAdd');
    Route::get('gallery/editgallery/{id}','GalleryController@getEdit')->where(array('id'=>'[0-9]+'));
    Route::post('gallery/addgallery','GalleryController@postAdd');
    Route::post('gallery/editgallery','GalleryController@postEdit');
    Route::post('gallery/addfile/{key}','GalleryController@postAddfile');
    Route::post('gallery/editfile/{key}','GalleryController@postEditfile');
    Route::post('gallery/dropimages/{key}','GalleryController@postDropimages');
    Route::post('gallery/deleteeditgallery','GalleryController@postDeleteeditgallery');
    Route::get('gallery/dataeditgallery/{key}','GalleryController@getDataeditgallery');


     Route::get('banner','BannerController@index');
    Route::get('banner/addbanner','BannerController@getAdd');
    Route::get('banner/editbanner/{id}','BannerController@getEdit')->where(array('id'=>'[0-9]+'));
    Route::post('banner/addbanner','BannerController@postAdd');
    Route::post('banner/editbanner','BannerController@postEdit');
    Route::post('banner/addfile/{key}','BannerController@postAddfile');
    Route::post('banner/editfile/{key}','BannerController@postEditfile');
    Route::post('banner/dropimages/{key}','BannerController@postDropimages');
    Route::post('banner/deleteeditbanner','BannerController@postDeleteeditbanner');
    Route::get('banner/dataeditbanner/{key}','BannerController@getDataeditgallery');


    Route::get('user','UserController@index');
    Route::get('user/adduser','UserController@getAdd');
    Route::get('user/edituser/{id}','UserController@getEdit')->where(array('id'=>'[0-9]+'));
    Route::post('user/adduser','UserController@postAdd');
    Route::post('user/edituser','UserController@postEdit');

    Route::get('sorttop/{type}/{id}', 'BackendController@getSorttop')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
    Route::get('sortdown/{type}/{id}', 'BackendController@getSortdown')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));


});
Route::group(array('prefix'=>'aro','before' => 'auth'), function()
{    
//    / Route::get('/','AroController@index');
Route::get('/', 'AroController@index');
Route::get('m16', 'AroController@getM16');
Route::post('add-thai-man', 'AroController@PostAddm16');
Route::get('logout','AroController@getLogout');
});
 

Route::post('login','BackendController@postLogin');



 
    Route::get('login',function(){
        return View::make('backend.login');
    });
    Route::get('aro-login',function(){
        return View::make('aro.login');
    });

 
    Route::get('{type}/{url}', 'ViewController@getView');
// Route::get('{type}/{url}', 'ViewController@getView');
// Route::get('{type}/{url}', 'ViewController@getView');
// Route::get('{type}/{url}', 'ViewController@getView');
// Route::get('{type}/{url}', 'ViewController@getView');
// Route::get('{type}/{url}', 'ViewController@getView');
// Route::get('{type}/{url}', 'ViewController@getView');


Route::get('data/viewcategories/{id}/{url}','ApiController@getDataviewcategories')->where(array('id'=>'[0-9]+'));
Route::get('data/viewtag/{id}/{url}','ApiController@getDataviewtag')->where(array('id'=>'[0-9]+'));
Route::get('data/viewsearch/{text}','ApiController@getDataviewsearch');
Route::get('data/allvdo','ApiController@getDataallvdo');
Route::get('data/recommend','ApiController@getDatarecommend');
Route::get('recommended','ViewController@getRecommended');

Route::post('search','ViewController@postViewsearch');

Route::get('allgallery','ViewController@getAllgallery');


 

 




 
 
