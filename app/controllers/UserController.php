<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /user
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = Lang::get('msg.msg_user',array(),'th');
		$api = URL::to('backend/data/user');
		return View::make('backend.user.index')->with(
			array(
			'title' 	=>$title,
			'api'	=> $api,
			'status'	=> 'null'
			));

		//return '=>'.Auth::user()->user_status;
	}
	public function getAdd()
	{
		 		$title = Lang::get('msg.msg_user',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$prefix = Prefix::orderBy('prefix_name','asc')->get();
				$depart = Maindepart::orderBy('maindepart_name','asc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.user.adduser')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
					  'sql'		=> $sql,
					  'maindepart' =>$depart,
				  	'prefix'		=> $prefix,
				 	'status'	=> 'null'
				       ));
	}
	public function getEdit($id)
	{
		 		$title = Lang::get('msg.msg_user',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$prefix = Prefix::orderBy('prefix_name','asc')->get();
				$depart = Maindepart::orderBy('maindepart_name','asc')->get();

				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				$c = User::select('users.username','users.user_type','users.user_status','tb_profiles.firstName','tb_profiles.lastName','tb_profiles.email','tb_profiles.phone','tb_profiles.photoURL','tb_permission.p_permission')
				->join('tb_profiles','tb_profiles.user_id','=','users.id')
				->join('tb_permission','tb_permission.user_id','=','users.id')
				->where('users.id',$id)->first();
				 
				 
				return View::make('backend.user.edituser')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'prefix'		=> $prefix,
					 'status'	=> 'null',
					 'maindepart' =>$depart,
				 	'c'			=>$c,
				 	'id'		=>$id
				       ));
	}
	public function postAdd()
	{
		 	$state_user = Input::get('txt_maindepart');
			if ($state_user =='0')
				{
					$state = '1';
				}else{
					$state ='2';
				}
	 		$token = Input::get('key');
	 		$Usertype = Input::get('txt_usertype');
	 		$password = Input::get('txt_password');
		  	$c = new User;
		  	$c->username 			= Input::get('txt_username');
		  	$c->password 			= Hash::make($password);
	  		$c->user_type			= Input::get('txt_usertype');
	  		$c->user_status			= Input::get('txt_show');
		   	$c->created_at			= date('Y-m-d H:i:s');
			   $c->updated_at			= date('Y-m-d H:i:s');
			   $c->user_maindepart = Input::get('txt_maindepart');
			   $c->user_subdepart 	= Input::get('txt_subdepart');
		 //  	$c->created_by			= Auth::user()->id;
		 	$c->user_state = $state;
	 	   	$c->remember_token 		= Input::get('key');
			 $c->save();
			 $depart_id = Input::get('txt_subdepart');
			 $geturl = Subdepart::find($depart_id);
			 $getid = User::where(array('username'=>Input::get('txt_username')))->first();
			 $e = new Enviroment;
			 $e->created_by = $getid->id;
			 $e->web_url = $geturl->subdepart_url;
			 $e->save();
 				  if (Input::hasFile('picture')){
 				  	
		  			$Path='uploadfiles/users'; 
		  			$Path_thumb = $Path.'/thumb';
		  			
		  			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('picture'))->height();
		  			 // $width= Image::make(Input::file('picture'))->width();
		  			 
		  			
		  			Image::make(Input::file('picture'))->resize(100, 100)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('picture'))->resize(50,50)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('picture')->getClientOriginalName();
	  			 }else{
	  			 	$img_name='';
	  			 }
	  		$uid = User::where('remember_token',Input::get('key'))->first();
 			DB::table('tb_profiles')->insert(array(
 				'user_id'	=> $uid->id,
 				'firstName'	=> Input::get('txt_firstname'),
 				'lastName'	=> Input::get('txt_lastname'),
 				'email'		=> Input::input('txt_email'),
 				'phone'		=> Input::input('txt_tel'),
 				'photoURL'	=> $img_name,
 				'token'		=> Input::get('key')
 				));
 			if($Usertype=="2")
 			{
 		//	$user_sys = Input::get('txt-usertype');
 			$arr_permis = Helpers::ListArray(Input::get('permission'));
 		 	$permis = Helpers::Permiss($arr_permis);
 				DB::table('tb_permission')->insert(array(
 					'token'	=>Input::get('key'),
 					'user_id'	=> $uid->id,
 					 'p_permission' =>$permis

 				));
 			}else{
 				DB::table('tb_permission')->insert(array(
 					'token'	=>Input::get('key'),
 					'user_id'	=> $uid->id,
 					 'p_permission' =>'1'

 				));
 			}
 		
 		return Redirect::to('backend/user')->with(
				array(
					'save-success' => 'save'
				       ));


	}

	public function postEdit()
	{
		 
	 
			 $id = Input::get('id');
			 $state_user = Input::get('txt_maindepart');
				if ($state_user =='0')
				{
					$state = '1';
				}else{
					$state ='2';
				}
	  	 
			$token = Input::get('key');
	 		$Usertype = Input::get('txt_usertype');
	 		$password = Input::get('txt_password');
	 		$newpassword = Hash::make($password);
	 		 if($password =="" || $password ==null){

			$update=	DB::table('users')
			->where('id',$id)
			->update(
                 array(
                 	'username'	=> Input::get('txt_username')
                 	 
                 	));

			}else{
			$update = DB::table('users')
			->where('id',$id)
			->update(
                 array(
                 	'username'	=> Input::get('txt_username'),
                 	'password'	=> $newpassword
                 	 
                 	));
			}
		  	$c = User::find($id);
		   
	  		$c->user_type			= Input::get('txt_usertype');
			  $c->user_status			= Input::get('txt_show');
			  $c->user_maindepart = Input::get('txt_maindepart');
			  $c->user_subdepart 	= Input::get('txt_subdepart');
		    
		 //  	$c->created_by			= Auth::user()->id;
		 $c->user_state = $state;
	 	   	$c->remember_token 		= Input::get('key');
			 $c->save();
		 


 				  if (Input::hasFile('picture')){
		  			$Path='uploadfiles/users'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$old_img = Input::get('img');
		  			if($old_img !='user.png'){
		  				File::delete($Path.'/'.$old_img);
			 			File::delete($Path_thumb.'/'.$old_img);
		  			}
		  			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('picture'))->height();
		  			 // $width= Image::make(Input::file('picture'))->width();
		  			 
		  			
		  			Image::make(Input::file('picture'))->resize(100, 100)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('picture'))->resize(50,50)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('picture')->getClientOriginalName();
	  			 	DB::table('tb_profiles')->where('user_id',$id)->update(array(
	  			 		'photoURL' => $img_name
	  			 		));
	  			 }else{
	  			  
	  			 }
	  		$uid = User::where('remember_token',Input::get('key'))->first();
 			DB::table('tb_profiles')->where('user_id',$id)->update(array(
 				'user_id'	=> $id,
 				'firstName'	=> Input::get('txt_firstname'),
 				'lastName'	=> Input::get('txt_lastname'),
 				'email'		=> Input::input('txt_email'),
 				'phone'		=> Input::input('txt_tel')
 				));
 			if($Usertype=="2")
 			{
 		//	$user_sys = Input::get('txt-usertype');
 			$arr_permis = Helpers::ListArray(Input::get('permission'));
 		 	$permis = Helpers::Permiss($arr_permis);
 				DB::table('tb_permission')->where('user_id',$id)->update(array(
 					 
 					'user_id'	=> $id,
 					 'p_permission' =>$permis

 				));
 			}else{
 				DB::table('tb_permission')->where('user_id',$id)->update(array(
 				 
 					'user_id'	=> $id,
 					 'p_permission' =>'1'

 				));
 			}
 				if($id == Auth::user()->id)
 			{
 			 					$profiles = DB::table('tb_profiles')
		       				    ->select('tb_profiles.firstName','tb_profiles.lastName','tb_profiles.photoURL','tb_permission.p_permission')
		       				    ->join('tb_permission','tb_permission.user_id','=','tb_profiles.user_id')
		       				    ->where('tb_profiles.user_id',$id)->first();
 								Session::put('permission', $profiles->p_permission);
		       				    Session::put('firstName', $profiles->firstName);
		       				    Session::put('lastName', $profiles->lastName);
		       				    Session::put('photoURL', $profiles->photoURL);
		    }
 	 
 
   	if(Auth::user()->user_type==1){
      return Redirect::to('backend/user')->with(
				array(
					'edit-success' => 'edit'
				       ));
   	}else{
   		  return Redirect::action('UserController@getEdit', array($id));
   	}

	}


	public function postAddfile($key)
	{

		  		
		  		if (Input::hasFile('uploadfile')){
		  			$Path='uploadfiles/user'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 
		  			
		  			Image::make(Input::file('uploadfile'))->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/user'; 
					// $upload_success=	Input::file('uploadfile')->move($Path,$img_name);


	     



	    			$Images 	= new Uploadfilesuser;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
 
		  			 
		  		}
 

	}
	public function postEditfile($key)
	{

		  		$oldfile = Uploadfilesuser::where('token',$key)->first();
		  		$Path='uploadfiles/user'; 
		  		$Path_thumb = $Path.'/thumb';
		  		if (Input::hasFile('uploadfile')){

		  	 
		  		
		  			
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 $imagesize=getimagesize(Input::file('uploadfile'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
		  			
		  			Image::make(Input::file('uploadfile'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/user'; 
    			 		$Images = new Uploadfilesuser;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
    			 }

	}

	public function postDropimages($key){

			 $img = Uploadfilesuser::where('files_oldname','=',$key)->first();
		 	 $filename='uploadfiles/user/'.$img->files_newname; 
		 	 $filename_thumb='uploadfiles/user/thumb/'.$img->files_thumb; 
			 File::delete($filename);
			 File::delete($filename_thumb);
			 $img = Uploadfilesuser::where(
			 	array(
			 	//	'token'=>$key,
			 		'files_newname'=>$img->files_newname
			 		))->delete();
			 
		}
		public function postDeleteedituser()
		{
			//$user_file = Uploadfilesuser::where('token',$c->user_file)->get();
		//	 var_dump($key);
			$del_img =Input::get('del_img');
			foreach ($del_img as $del =>$d)
			{
				$filename='uploadfiles/user/'.$d;
		 	 	$filename_thumb='uploadfiles/user/thumb/'.$d; 
			 	File::delete($filename);
			 	File::delete($filename_thumb);
				Uploadfilesuser::where(array('files_newname'=>$d))->delete();

			}
			return 'ok';
		}

		public function getDataedituser($key)
		{
			$user = Uploadfilesuser::where(array('token'=>$key))->get();
			return View::make('backend.user.dataedituser')->with(array(
				'user_file' => $user
				));
		}
	}