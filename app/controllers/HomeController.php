<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 


	public function index()
	{

			//if (Request::segment(1)!="") 
			// {
            // 	$Enviroment = Enviroment::where('web_url',Request::segment(1))->first();
			// }else{
			// 	$Enviroment = Enviroment::where('created_by','1')->first();	
			// }
			$Enviroment = Enviroment::where('created_by','1')->first();	
		$template =  Config::get('app.template');
		//$Enviroment = Enviroment::first();
		$Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1','create_by'=>$Enviroment->created_by) )->orderBy('sort','asc')->get();//all();
		 
		$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
	 	->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
	 	->where(array('tb_banner.banner_show'=>'1','tb_banner.created_by'=>$Enviroment->created_by))
	 	->orderBy('tb_banner.id','desc')
	 	->get();
		 $Gallery = Gallery::select(DB::raw('tb_gallery.gallery_sorting,tb_gallery.gallery_name,tb_gallery.gallery_url,tb_files_gallery.files_newname'))
		 
		->join('tb_files_gallery','tb_files_gallery.token','=','tb_gallery.gallery_file')	 	
		 ->where(array('tb_gallery.gallery_show'=>'1','tb_gallery.created_by'=>$Enviroment->created_by))
		//  ->where('tb_gallery.gallery_sorting','>','0')
		 ->groupBy('tb_gallery.id')
		 ->orderBy('tb_gallery.gallery_sorting','asc')
		 ->skip(0)->take(5)
	 	->get();

	 	$Count_Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1','create_by'=>$Enviroment->created_by) )->count();//all();
	 	 
		$Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0','create_by'=>$Enviroment->created_by))->orderBy('mainmenu_sorting','asc')->get();
		$Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2','create_by'=>$Enviroment->created_by))->orderBy('mainmenu_sorting','asc')->get();
		$Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3','create_by'=>$Enviroment->created_by))->orderBy('mainmenu_sorting','asc')->get();
		$Maindepart = Maindepart::where(array('maindepart_show'=>'1'))->orderBy('maindepart_sorting','asc')->get();
		$HomeNews = Content::select('tb_content.content_name','tb_content.content_fast','tb_content.content_url','tb_content.created_at')
		->where(array('tb_content.content_showhome'=>'0','tb_content.content_show'=>'1','tb_content.created_by'=>$Enviroment->created_by))
		->orderBy('tb_content.id','desc')
		->skip(0)->take(10)
		->get();
		$HotNews = Content::select('tb_content.content_name','tb_content.content_fast','tb_content.content_picture','tb_content.content_url','tb_content.created_at')
		->where(array('tb_content.content_showhome'=>'1','tb_content.content_show'=>'1','tb_content.created_by'=>$Enviroment->created_by))
		->orderBy('tb_content.id','desc')
		->skip(0)->take(10)
		->get();

		// if (Request::segment(1)=="")
		// {

		// }else{
			
		// }
		return View::make('frontend.'.$template.'.index')->with(array(
			'env'				=> $Enviroment,
			'Categories'		=> $Categories,
			'Mainmenu_top'		=> $Mainmenu_top,
			'Mainmenu_right'	=> $Mainmenu_right,
			'Mainmenu_left'		=> $Mainmenu_left,
			'Banner'			=>$Banner,
			'i'					=>1,
			'Gallery'			=>$Gallery,
			'Count_Categories'	=>$Count_Categories,
			'Maindepart'		=>$Maindepart,
			'HomeNews'			=>$HomeNews,
			'HotNews'			=>$HotNews
			

			));
	 }

          
}
