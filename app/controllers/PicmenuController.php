<?php

class PicmenuController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /banner
	 *
	 * @return Response
	 */
 


	public function postAddfile($key)
	{

		  		
		  		if (Input::hasFile('uploadfile')){
		  			$Path='uploadfiles/picmenu'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 
		  			
		  			Image::make(Input::file('uploadfile'))->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/picmenu'; 
					// $upload_success=	Input::file('uploadfile')->move($Path,$img_name);


	     



	    			$Images 	= new Uploadfilesbanner;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
 
		  			 
		  		}
 

	}
	public function postEditfile($key)
	{

		  		$oldfile = Uploadfilesbanner::where('token',$key)->first();
		  		$Path='uploadfiles/picmenu'; 
		  		$Path_thumb = $Path.'/thumb';
		  		if (Input::hasFile('uploadfile')){

		  	 
		  		
		  			
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 $imagesize=getimagesize(Input::file('uploadfile'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
		  			
		  			Image::make(Input::file('uploadfile'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/picmenu'; 
    			 		$Images = new Uploadfilesbanner;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
    			 }

	}

	public function postDropimages($key){

			 $img = Uploadfilesbanner::where('files_oldname','=',$key)->first();
		 	 $filename='uploadfiles/picmenu/'.$img->files_newname; 
		 	 $filename_thumb='uploadfiles/picmenu/thumb/'.$img->files_thumb; 
			 File::delete($filename);
			 File::delete($filename_thumb);
			 $img = Uploadfilesbanner::where(
			 	array(
			 	//	'token'=>$key,
			 		'files_newname'=>$img->files_newname
			 		))->delete();
			 
		}
		public function postDeleteeditbanner()
		{
			//$banner_file = Uploadfilesbanner::where('token',$c->banner_file)->get();
		//	 var_dump($key);
			$del_img =Input::get('del_img');
			foreach ($del_img as $del =>$d)
			{
				$filename='uploadfiles/picmenu/'.$d;
		 	 	$filename_thumb='uploadfiles/picmenu/thumb/'.$d; 
			 	File::delete($filename);
			 	File::delete($filename_thumb);
				Uploadfilesbanner::where(array('files_newname'=>$d))->delete();

			}
			return 'ok';
		}

		public function getDataeditbanner($key)
		{
			$banner = Uploadfilesbanner::where(array('token'=>$key))->get();
			return View::make('backend.banner.dataeditbanner')->with(array(
				'banner_file' => $banner
				));
		}
	}