<?php

class ViewController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /view
	 *
	 * @return Response
	 */
	public function __construct()
	{
		# code...
	}
	public function index()
	{
		//
	}
	
	public function getAllgallery()
	{
		// if (Request::segment(1)!="") 
		// {
		// 				$Enviroment = Enviroment::where('web_url',Request::segment(1))->first();
		// }else{
		// 			$Enviroment = Enviroment::where('created_by','1')->first();	
		// }
		$sql = Gallery::select('tb_gallery.gallery_name','tb_gallery.gallery_url','tb_files_gallery.files_newname')
		->join('tb_files_gallery','tb_files_gallery.token','=','tb_gallery.gallery_file')	 	
		->where(array('tb_gallery.gallery_show'=>'1'))
	
	 	->groupBy('tb_gallery.id')
	 	->orderBy('tb_gallery.gallery_sorting','asc')
	    ->paginate(25);
	 
		 $template =  Config::get('app.template');
		$Enviroment = Enviroment::first();
		 
	 	$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
	 	->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
	 	->where(array('tb_banner.banner_show'=>'1'))
	 	->orderBy('tb_banner.id','desc')
	 	->get();
			 
		 $Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
		  return View::make('viewmodule.'.$template.'.allgallery')->with(
             array(
             		'Gallery' 			=>$sql,
             	'env'		=> $Enviroment,
					//'Categories'	=> $Categories,
					'Mainmenu_top'	=> $Mainmenu_top,
					'Mainmenu_right'	=> $Mainmenu_right,
					'Mainmenu_left'	=> $Mainmenu_left,
					'Banner'	=>$Banner,
					'Title'		=>'ภาพกิจกรรมทั้งหมด'
					// 'tags'		=>$tag,
					// 'Datarandom' =>$sql_converse,
					// 'Numfiles'	=>$sql_numfile,
					// 'Files'		=>$sql_files
             	));
	}
	public function getView($type,$url)
	{
		// if (Request::segment(1)!="") 
		// {
		// 				$Enviroment = Enviroment::where('web_url',Request::segment(1))->first();
		// }else{
		// 			$Enviroment = Enviroment::where('created_by','1')->first();	
		// }
		 switch($type):
		 	case 'news':
		$sql = Content::select('tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_content.content_author','tb_content.content_year','tb_content.content_view',
        'tb_content.content_url','content_detail','content_alldetail','tb_categories.categories_name','tb_categories.id as cid','tb_categories.categories_url'
        )
				->where('tb_content.content_url','=',$url)
			//	->where('tb_content.created_by','=',$Enviroment->created_by)
        ->where('tb_content.content_show','=','1')
	  // ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
			->first();
			//return var_dump($sql);
	    $sql_numfile = Uploadfiles::select(
	    'tb_files.files_type','tb_files.files_newname','tb_files.token'
        ,'tb_content.id','tb_files.files_size','tb_files.files_oldname'
        )
        ->where('tb_content.content_url','=',$url)
        ->where('tb_content.content_show','=','1')
        ->where('tb_files.files_size','!=','0')
	    ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->count();
	    $sql_files = Uploadfiles::select(
	    'tb_files.files_type','tb_files.files_newname','tb_files.files_oldname','tb_files.token'
        ,'tb_content.id','tb_files.files_size','tb_files.files_oldname'
        )
        ->where('tb_content.content_url','=',$url)
        ->where('tb_content.content_show','=','1')
        ->where('tb_files.files_size','!=','0')
	    ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->first();
		 $template =  Config::get('app.template');
		$Enviroment = Enviroment::first();
		$Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1') )->get();//all();
	 	$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
	 	->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
		 ->where('tb_banner.banner_show','1')
		 //	->where(array('tb_banner.banner_show'=>'1','tb_banner.created_by'=>$Enviroment->created_by))
	 	->orderBy('tb_banner.id','desc')
	 	->get();
		 $Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
			$tag = Tagcontent::select('tb_tag.tag_name','tb_tag.id as tagid','tb_tag.tag_url')
			->join('tb_tag','tb_tag.id','=','tb_tagcontent.tag_id')
			->where('tb_tagcontent.content_id',$sql->id)->get();
			$updateview = Content::find($sql->id);
			$oldview = $updateview->content_view;
			$updateview->content_view = $oldview+1;
			
		 	$updateview->save();

			// $fsize = $sql->files_size;
			// $files_size = $fsize/1024;
		$sql_converse = Content::select('tb_content.content_name','tb_content.id','tb_content.content_picture'
        ,'tb_content.created_by','tb_content.created_at','tb_content.content_author','tb_content.content_year','tb_content.content_view',
        'tb_content.content_url','content_detail','tb_categories.categories_name','tb_categories.id as cid','tb_categories.categories_url'
        )
         ->where('tb_content.content_url','!=',$url)
        ->where('tb_content.content_show','=','1')
	  //  ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
	    ->orderBy(DB::raw('RAND()'))
	    ->take(3)->skip(0)
		->get();
		 
		 $sql_user  = User::select('tb_profiles.firstName','tb_profiles.lastName')
		 ->join('tb_profiles','tb_profiles.user_id','=','users.id')
		 
		 ->where('users.id',$sql->created_by)->first();
		  return View::make('viewmodule.'.$template.'.content')->with(
             array(
             		'data' 			=>$sql,
             	'env'		=> $Enviroment,
					'Categories'	=> $Categories,
					'Mainmenu_top'	=> $Mainmenu_top,
					'Mainmenu_right'	=> $Mainmenu_right,
					'Mainmenu_left'	=> $Mainmenu_left,
					'Banner'	=>$Banner,
			//		'tags'		=>$tag,
					'Datarandom' =>$sql_converse,
					'Numfiles'	=>$sql_numfile,
					'Files'		=>$sql_files,
				//	'filename' =>$sql_files->files_newname,
					'user'		=>$sql_user
             	));
		 	break;
		 case 'menu':
		  	$template =  Config::get('app.template');
			$Enviroment = Enviroment::first();
			$Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1') )->get();//all();
		 	$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
		 	->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
			 ->where('tb_banner.banner_show','1')
			 	->where(array('tb_banner.banner_show'=>'1'))
		 	->orderBy('tb_banner.id','desc')
			 	->get();
			$Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
			$Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
			$Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
			$sql = Mainmenu::where(array(
				'm_url'=>$url
				))->first();
				$sql_user  = User::select('tb_profiles.firstName','tb_profiles.lastName')
		 ->join('tb_profiles','tb_profiles.user_id','=','users.id')
		 
		 ->where('users.id',$sql->create_by)->first();
		 return View::make('viewmodule.'.$template.'.mainmenu')->with(
             array(
             		'data' 			=>$sql,
             	'env'		=> $Enviroment,
					'Categories'	=> $Categories,
					'Mainmenu_top'	=> $Mainmenu_top,
					'Mainmenu_right'	=> $Mainmenu_right,
					'Mainmenu_left'	=> $Mainmenu_left,
					'Banner'	=>$Banner,
					'user'		=>$sql_user
					
					//'size'			=>$files_size
             	));

		 break;
		 case 'submenu':
		  	$template =  Config::get('app.template');
			$Enviroment = Enviroment::first();
			$Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1') )->get();//all();
		 	$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
		 	->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
			 ->where('tb_banner.banner_show','1')
			 	->where(array('tb_banner.banner_show'=>'1'))
		 	->orderBy('tb_banner.id','desc')
			 	->get();
			$Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
			$Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
			$Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
			$sql = Submenu::select('tb_mainmenu.mainmenu_name','tb_mainmenu.m_url'
				,'tb_submenu.submenu_name','tb_submenu.created_at','tb_submenu.submenu_detail','tb_submenu.create_by')
			->where(array('s_url'=>$url))
			->join('tb_mainmenu','tb_mainmenu.id','=','tb_submenu.submenu_categories')
			->first();
			$sql_user  = User::select('tb_profiles.firstName','tb_profiles.lastName')
			->join('tb_profiles','tb_profiles.user_id','=','users.id')
			
			->where('users.id',$sql->create_by)->first();
		 return View::make('viewmodule.'.$template.'.submenu')->with(
             array(
             		'data' 			=>$sql,
        'env'		=> $Enviroment,
					'Categories'	=> $Categories,
					'Mainmenu_top'	=> $Mainmenu_top,
					'Mainmenu_right'	=> $Mainmenu_right,
					'Mainmenu_left'	=> $Mainmenu_left,
					'Banner'	=>$Banner,
					'user'		=>$sql_user
					
					//'size'			=>$files_size
             	));

		 break;
		  case 'slide':
		  	$template =  Config::get('app.template');
			$Enviroment = Enviroment::first();
			$Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1') )->get();//all();
		 	$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
		 	->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
			 ->where('tb_banner.banner_show','1')
			 	->where(array('tb_banner.banner_show'=>'1'))
		 	->orderBy('tb_banner.id','desc')
			 	->get();
			$Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
			$Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
			$Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
	
			
		 	
			$sql = Banner::where(array(
				'banner_url'=>$url,
				'banner_show'=>'1'
				))->first();
				$sql_user  = User::select('tb_profiles.firstName','tb_profiles.lastName')
				->join('tb_profiles','tb_profiles.user_id','=','users.id')
				
				->where('users.id',$sql->created_by)->first();
				$updateview = Banner::find($sql->id);
				$oldview = $updateview->banner_view;
				$updateview->banner_view = $oldview+1;
				$updateview->save();
		 return View::make('viewmodule.'.$template.'.slide')->with(
             array(
             		'data' 			=>$sql,
        'env'		=> $Enviroment,
					'Categories'	=> $Categories,
					'Mainmenu_top'	=> $Mainmenu_top,
					'Mainmenu_right'	=> $Mainmenu_right,
					'Mainmenu_left'	=> $Mainmenu_left,
					'Banner'	=>$Banner,
					'user'		=>$sql_user
					
					//'size'			=>$files_size
             	));

		 break;
		 case 'allnews':
		 	$sql = Content::select('tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_content.content_author','tb_content.content_year','tb_content.content_view',
        'tb_content.content_url','tb_content.content_picture','content_detail','content_alldetail','tb_categories.categories_name','tb_categories.id as cid','tb_categories.categories_url'
        )
        ->where('tb_categories.categories_url','=',$url)
        ->where('tb_content.content_show','=','1')
	  //  ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
	    ->paginate(25);
	 
		$template =  Config::get('app.template');
		$Enviroment = Enviroment::first();
		$Categories = Categories::where(array('categories_show'=>'1','categories_url'=>$url) )->first();//all();
		$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
		->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
		->where(array('tb_banner.banner_show'=>'1'))
		->orderBy('tb_banner.id','desc')
		->get();
			
		 $Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
		 
	 
		  return View::make('viewmodule.'.$template.'.allnews')->with(
             array(
             		'data' 			=>$sql,
        'env'		=> $Enviroment,
					'Categories'	=> $Categories,
					'Mainmenu_top'	=> $Mainmenu_top,
					'Mainmenu_right'	=> $Mainmenu_right,
					'Mainmenu_left'	=> $Mainmenu_left,
					'Banner'	=>$Banner,
					'Title'		=>$Categories->categories_name
					// 'tags'		=>$tag,
					// 'Datarandom' =>$sql_converse,
					// 'Numfiles'	=>$sql_numfile,
					// 'Files'		=>$sql_files
             	));
		 	break;
		 case 'gallery':
		 	 	$template =  Config::get('app.template');
		//		$Enviroment = Enviroment::first();
				$Categories = Categories::where('parent_id','0')->where(array('categories_show'=>'1','categories_showhome'=>'1') )->get();//all();
	 			$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
	 			->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
				 ->where('tb_banner.banner_show','1')
				 	->where(array('tb_banner.banner_show'=>'1'))
	 			->orderBy('tb_banner.id','desc')
	 			->get();
				$Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
				$Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
				$Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
				$sql = Gallery::select('tb_gallery.gallery_name','tb_gallery.gallery_url','tb_gallery.gallery_file')
			 	->where(array('tb_gallery.gallery_show'=>'1','tb_gallery.gallery_url'=>$url))
				->first();
				$Gallery_file = Uploadfilesgallery::where(array('token'=>$sql->gallery_file))->get();
				$sql_user  = User::select('tb_profiles.firstName','tb_profiles.lastName')
				->join('tb_profiles','tb_profiles.user_id','=','users.id')
				
				->where('users.id',$sql->created_by)->first();
			 return View::make('viewmodule.'.$template.'.gallery')->with(
		             array(
		             		'data' 			=>$sql,
		        'env'		=> $Enviroment,
							'Categories'	=> $Categories,
							'Mainmenu_top'	=> $Mainmenu_top,
							'Mainmenu_right'	=> $Mainmenu_right,
							'Mainmenu_left'	=> $Mainmenu_left,
							'Banner'	=>$Banner,
							'Title'		=>$sql->gallery_name,
							'Gallery_file'	=> $Gallery_file,
							'user'		=>$sql_user
							// 'tags'		=>$tag,
							// 'Datarandom' =>$sql_converse,
							// 'Numfiles'	=>$sql_numfile,
							// 'Files'		=>$sql_files
		             	));

		 break;
		 case 'download':
		  $file = Uploadfiles::where('files_newname',$url)->first();
		 if($file->files_newname==" "){
		 	return redirect()->back();
		 }else{
		 $downloadfiles = 'uploadfiles/'.$file->files_newname;
		 return Response::download($downloadfiles);
		}
		 break;
		 case 'submaindepart':
		 $sql = Subdepart::select('tb_subdepart.subdepart_name','tb_subdepart.subdepart_type','tb_subdepart.subdepart_url','tb_subdepart.s_url','tb_maindepart.maindepart_name')
        ->where('tb_maindepart.m_url','=',$url)
        ->where('tb_subdepart.subdepart_show','=','1')
	  //  ->join('tb_content','tb_content.content_file','=','tb_files.token')
	    ->join('tb_maindepart','tb_maindepart.id','=','tb_subdepart.subdepart_categories')
	    ->paginate(25);
	 
		$template =  Config::get('app.template');
	//	$Enviroment = Enviroment::first();
		$Categories = Maindepart::where(array('maindepart_show'=>'1','m_url'=>$url) )->first();//all();
		$Banner = Banner::select('tb_banner.banner_name','tb_banner.banner_detail','tb_files_banner.files_newname','tb_banner.banner_url')
		->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
		->where(array('tb_banner.banner_show'=>'1'))
		->orderBy('tb_banner.id','desc')
		->get();
			
		 $Mainmenu_top = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'1','parent_id'=>'0'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_right = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'2'))->orderBy('mainmenu_sorting','asc')->get();
		 $Mainmenu_left = Mainmenu::where(array('mainmenu_show'=>'1','mainmenu_position'=>'3'))->orderBy('mainmenu_sorting','asc')->get();
		 
	 
		  return View::make('viewmodule.'.$template.'.submaindepart')->with(
             array(
             		'data' 			=>$sql,
        'env'		=> $Enviroment,
					'Categories'	=> $Categories,
					'Mainmenu_top'	=> $Mainmenu_top,
					'Mainmenu_right'	=> $Mainmenu_right,
					'Mainmenu_left'	=> $Mainmenu_left,
					'Banner'	=>$Banner,
					'Title'		=>$Categories->maindepart_name
					// 'tags'		=>$tag,
					// 'Datarandom' =>$sql_converse,
					// 'Numfiles'	=>$sql_numfile,
					// 'Files'		=>$sql_files
             	));
		 break;
	 
		 endswitch;
	} 
	 
	 

}