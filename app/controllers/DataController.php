<?php

class DataController extends BaseController {

		 public function getDataviewcategories($id,$url)
      {
       $sql = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url'
        ])
        ->where('tb_categories.id','=',$id)->where('tb_categories.categories_url','=',$url)
    ->join('tb_content','tb_content.content_file','=','tb_files.token')
    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
    ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->addColumn('no','')
    ->removeColumn('id')
    ->editColumn('created_by','Administrator')
    ->editColumn('content_name','<a href="{{ URL::to("content",array($id,$content_url))}}">{{ $content_name }}</a>')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('files_size','{{number_format($files_size/1024)}} kb.')
    ->removeColumn('content_url');
    return $datatables->make(true);
      }
}