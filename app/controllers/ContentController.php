<?php

class ContentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /content
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = Lang::get('msg.content',array(),'th');
		$api = URL::to('backend/data/content');
		return View::make('backend.content.maincontent')->with(
			array(
			'title' 	=>$title,
			'api'	=> $api,
			'status'	=> 'null'
			));

		//return '=>'.Auth::user()->user_status;
	}
	public function getAdd()
	{
		 		$title = Lang::get('msg.content',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$depart = Depart::orderBy('id','asc')->get();
				$tag = Tag::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.content.addcontent')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
					 'status'	=> 'null',
					 'depart'	=>$depart
				       ));
	}
	public function getEdit($id)
	{
		 		$title = Lang::get('msg.content',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$tag = Tag::orderBy('id','desc')->get();
				$tagcontent = Tagcontent::where('content_id',$id)->get();
				$depart = Depart::orderBy('id','asc')->get();
				$c = Content::find($id);
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.content.editcontent')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
				 	'status'	=> 'null',
				 	'tagcontent' => $tagcontent,
					 'c'			=> $c,
					 'depart'    =>$depart
				       ));
	}
	public function postAdd()
	{
		$url = Helpers::create_url(Input::get('txt_name'));
		 $tag = Input::get('txt_tag');
	 	$token = Input::get('key');
	 	
	 	  $pid = Input::get('txt_type');
	 	  $parent_id = Helpers::get_parent($pid);
	 	//  return Helpers::getParent($cat);

	 	 // for($ii=0;$ii<=2;$ii++):

	 	 // endfor;
	 		// $ch = Categories::find($cat);
	 		// 	if($ch->parent_id=="0") {
	 		// 		$id = $ch->id;
	 		// 	}else if ($ch->parent_id !="0") {
	 		// 		$ch1 = Categories::find($ch->id);
	 		// 	}
	 	  if (Input::hasFile('picture')){
		  			$Path='uploadfiles/news'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('picture'))->height();
		  			 // $width= Image::make(Input::file('picture'))->width();
		  			 $imagesize=getimagesize(Input::file('picture'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
					//$height = 300;
		  			
		  			Image::make(Input::file('picture'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('picture'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('picture')->getClientOriginalName();
	  			 }else{
	  			 	$img_name='';
	  			 }
	  			 	 
	 	
		 $c = new Content;
		 $c->content_name 			= Input::get('txt_name');
		 $c->content_categories		= Input::get('txt_type');
		 $c->content_detail			= Input::get('txt_detail');
		 $c->content_file			= $token;
		 $c->content_view			= '0';
		 $c->content_show			= Input::get('txt_show');
		 $c->content_showhome		= Input::get('txt_showhome');
		 $c->content_alldetail		= Input::get('txt_alldetail');
	  	 $c->content_picture		= $img_name;
		  $c->content_author			= Input::get('txt_depart');
		 // $c->content_year			= Input::get('txt_year');
		 $c->content_fast			= Input::get('txt_fast');
		 $c->created_at			= date('Y-m-d H:i:s');
		 $c->updated_at			= date('Y-m-d H:i:s');
		 $c->created_by			= Auth::user()->id;
		 $c->content_url			= $url;
		 $c->content_keyword			= Input::get('web_keyword');
		 $c->parent_id				= $parent_id;

		 $c->save();
		 $cid = Content::where(array(
		 	'content_url'	=> $url,
		 	'created_by'	=> Auth::user()->id
		 	))->first();
		 $t = new Tagcontent;
		 if(!empty($tag)){
       
        foreach($tag as $tags =>$tt):
          DB::table('tb_tagcontent')->insert(array(
              'tag_id'  => $tt,
              'content_id' => $cid->id,
               'tag_count' =>'0'
            ));
        endforeach;
      }
      $filenum = Uploadfiles::where('token',$token )->count();
      if($filenum > 0){
      }else{
      	$file_type = '0';
		  			$Images 	= new Uploadfiles;
    		 
    			$Images 	->files_newname 		="";
    			$Images 	->files_oldname			="";
    			$Images 	->files_type			=$file_type;
    			$Images 	->token 				= Input::get('key');
    			$Images 	->save();
      }
      
	return Redirect::to('backend/content')->with(
				array(
					'save-success' => 'save'
				       ));

	}

	public function postEdit()
	{
		$url = Helpers::create_url(Input::get('txt_name'));
		 $tag = Input::get('txt_tag');
		 $id = Input::get('id');
	  		$pid = Input::get('txt_type');
	 	  $parent_id = Helpers::get_parent($pid);


		 			$c = Content::find($id);
		 	 	  if (Input::hasFile('picture')){
		  			$Path='uploadfiles/news'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$file_delete_name = $Path."/".Input::get('old_picture');
		  			$file_delete_thumb_name = $Path_thumb."/".Input::get('old_picture');

		  			File::delete($file_delete_name);
		  			File::delete($file_delete_thumb_name);
		  			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('picture'))->height();
		  			 // $width= Image::make(Input::file('picture'))->width();
		  			 $imagesize=getimagesize(Input::file('picture'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
				//	 $height = 300;
		  			
		  			Image::make(Input::file('picture'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('picture'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('picture')->getClientOriginalName();
	  			 }else{
	  			 	$img_name=Input::get('old_picture');
	  			 }


		 $c->content_name 			= Input::get('txt_name');
		 $c->content_categories		= Input::get('txt_type');
		 $c->content_detail			= Input::get('txt_detail');
		 $c->content_author			= Input::get('txt_depart');
		// $c->content_year			= Input::get('txt_depart');
		 $c->content_show			= Input::get('txt_show');
		 $c->content_showhome		= Input::get('txt_showhome');
		  $c->content_alldetail		= Input::get('txt_alldetail');
		  $c->content_picture		= $img_name;
		// $c->content_file			= Input::get('key');
		// $c->content_view			= '0';
		// $c->content_tag			= '';
		// $c->created_at				= date('Y-m-d H:i:s');
		$c->content_fast			= Input::get('txt_fast');
		 $c->updated_at				= date('Y-m-d H:i:s');
		 $c->created_by				= Auth::user()->id;
		 $c->content_url			= $url;
		 $c->content_keyword			= Input::get('web_keyword');
		 $c->parent_id				= $parent_id;
		 $c->save();
		 
		 $t = new Tagcontent;
		 if(!empty($tag)){
       	
        foreach($tag as $tags =>$tt):
        	$num = Tagcontent::where(array('tag_id'=>$tt,'content_id'=>$id))->count();
        	  if($num <= 0){
          		DB::table('tb_tagcontent')->insert(array(
              'tag_id'  => $tt,
              'content_id' => $id,
               'tag_count' =>'0'
            ));
       
      			}else{
      				Tagcontent::where('content_id','=',$id)
      				->where('tag_id','!=',$tt)->delete();
      			}
        endforeach;
      }
   
      return Redirect::to('backend/content')->with(
				array(
					'edit-success' => 'edit'
				       ));

	}


	public function postAddfile($key)
	{

		  		
		  		if (Input::hasFile('uploadfile')){

	  			$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 $file= Input::file('uploadfile');
	  			$files_size= File::size($file);
		  		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
				$Path='uploadfiles'; 
	    			$upload_success=	Input::file('uploadfile')->move($Path,$img_name);

	    			$Images 	= new Uploadfiles;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images	->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();

		  				if($file_type=='mp4'||
						$file_type=='MP4'||
						$file_type=='wmv'||
						$file_type=='WMV'||
						$file_type=='avi'||
						$file_type=='AVI'||
						$file_type=='mkv'||
						$file_type=='MKV'||
						$file_type=='mov'||
						$file_type=='MOV')	 {
		  			$video = $img_name;
					
					//Video Path
					$videopath='uploadfiles/'.$video;
					 
					//where to save the image
					$image = 'uploadfiles/thumbnail/'.$video.'.jpg';
					 
					//time to take screenshot at
					$interval = 15;
					 
					//screenshot size
					$size = "625x325";
					 
					//ffmpeg command to generate thumb jpg file
					$ffmpegcmd = "/usr/local/bin/ffmpeg -i $videopath -deinterlace -an -ss $interval -f mjpeg -t 1 -r 1 -y -s $size $image 2>&1";      
					shell_exec($ffmpegcmd);
		  		}
		  			 
		  		}

	}
	public function postEditfile($key)
	{

		  		$oldfile = Uploadfiles::where('token',$key)->first();

		  		if (Input::hasFile('uploadfile')){

		  			if($oldfile->files_type=="0") {

					//	 $img = Uploadfiles::where('token',$key)->delete();
						}else{
						// $img = Uploadfiles::where('token',$key)->delete();
						 $filename='uploadfiles/'.$oldfile->files_newname; 
						 File::delete($filename);
						}

		  		$filename = Input::file('uploadfile')->getClientOriginalName();
		  		$file= Input::file('uploadfile');
		  			$files_size= File::size($file);
		  		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
				$Path='uploadfiles'; 
    			$upload_success=	Input::file('uploadfile')->move($Path,$img_name);

    			 DB::table('tb_files')
	            ->where('token', $key)
	            ->update(array(
	    		 	'files_newname'			=>$img_name,
	    			'files_oldname'			=>$filename,
	    			'files_type'			=>$file_type,
	    			'files_size'			=>$files_size
	    			
	    		));
	    			if($file_type=='mp4'||
						$file_type=='MP4'||
						$file_type=='wmv'||
						$file_type=='WMV'||
						$file_type=='avi'||
						$file_type=='AVI'||
						$file_type=='mkv'||
						$file_type=='MKV'||
						$file_type=='mov'||
						$file_type=='MOV')	 {
		  			$video = $img_name;
					
					//Video Path
					$videopath='uploadfiles/'.$video;
					 
					//where to save the image
					$image = 'uploadfiles/thumbnail/'.$video.'.jpg';
					 
					//time to take screenshot at
					$interval = 15;
					 
					//screenshot size
					$size = "625x325";
					 
					//ffmpeg command to generate thumb jpg file
					$ffmpegcmd = "/usr/local/bin/ffmpeg -i $videopath -deinterlace -an -ss $interval -f mjpeg -t 1 -r 1 -y -s $size $image 2>&1";      
					shell_exec($ffmpegcmd);
		  		}
	    		 
    			 }

	}

	public function postDropimages($key){

			 $img = Uploadfiles::where('token','=',$key)->first();
		 	 $filename='uploadfiles/'.$img->files_newname; 
	 
			 File::delete($filename);
			 $img = Uploadfiles::where('token',$key)->delete();
			 
		}
		public function postCkupload()
		{
			$url = 'uploadfiles/images/'.time()."_".$_FILES['upload']['name'];
 //extensive suitability check before doing anything with the file…
		    if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name'])) )
		    {
		       $message = "No file uploaded.";
		    }
		    else if ($_FILES['upload']["size"] == 0)
		    {
		       $message = "The file is of zero length.";
		    }
		    else if (($_FILES['upload']["type"] != "image/pjpeg") AND ($_FILES['upload']["type"] != "image/jpeg") AND ($_FILES['upload']["type"] != "image/png"))
		    {
		       $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
		    }
		    else if (!is_uploaded_file($_FILES['upload']["tmp_name"]))
		    {
		       $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
		    }
		    else {
		      $message = "";
		      $move = @ move_uploaded_file($_FILES['upload']['tmp_name'], $url);
		      if(!$move)
		      {
		         $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
		      }
		      //$url = "../" . $url;
		      $url =  URL::to($url);
		    }
		$funcNum = $_GET['CKEditorFuncNum'] ;
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
		}
		public function getBrowse($txt)
		{
		return View::make('browse');
		}
}