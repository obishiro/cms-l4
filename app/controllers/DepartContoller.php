<?php

class DepartContoller extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /menucontoller
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
	public function getDepart($type)
	{
		switch ($type):
			case 'maindepart':
			$title = "หน่วยงานหลัก";
			$sql = Maindepart::orderBy('id','desc')->get();
			 
			$api = URL::to('backend/data/maindepart');
			return View::make('backend.depart.maindepart')->with(
				 array(
				 	'title' 	=>$title,
				 
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

			break;
			case 'subdepart':
			$title = Lang::get('msg.subdepart',array(),'th');
			$api = URL::to('backend/data/subdepart');
			return View::make('backend.depart.subdepart')->with(
				 array(
				 	'title' 	=>$title,
				  	'api'	=> $api,
				 	'status'	=> 'null'
				       ));

				break;
		endswitch;
	}

	public function getAdd($type)
	{
		switch($type):
			case 'maindepart':
				$title = Lang::get('msg.maindepart',array(),'th');
				$sql = Maindepart::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.depart.addmaindepart')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	 'sql'		=> $sql,
				 	'status'	=> 'null'
				       ));
			break;
			case 'subdepart':
				$title = Lang::get('msg.subdepart',array(),'th');
				 $sql = Maindepart::where('maindepart_type','3')->orderBy('id','desc')->get();
			 
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.depart.addsubdepart')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'	=>$rules,
				  	'sql'	=> $sql,
				 	'status'	=> 'null'
				       ));
			break;

		endswitch;
	}
	public function postAdd($type)
	{
		switch($type):
			case 'maindepart':

					  if (Input::hasFile('picture')){
		  			$Path='uploadfiles/depart'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('picture'))->height();
		  			 // $width= Image::make(Input::file('picture'))->width();
		  			 $imagesize=getimagesize(Input::file('picture'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
		  			
		  			Image::make(Input::file('picture'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('picture'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('picture')->getClientOriginalName();
	  			 }else{
	  			 	$img_name='';
	  			 }

				$max = Maindepart::where('maindepart_position',Input::get('txt_position'))->max('maindepart_sorting');
				$maxsorting = $max + 1;
				$url = Helpers::create_url(Input::get('txt_name'));
				 $m = new Maindepart;
				 $m->maindepart_name = Input::get('txt_name') ;
				 $m->maindepart_type = Input::get('txt_type');
				 $m->maindepart_detail = Input::get('txt_detail');
				 $m->maindepart_url = Input::get('txt_url');
				 $m->maindepart_logo = $img_name;
				 $m->maindepart_embed = Input::get('txt_embed');
				 $m->m_url 			= $url;
			//	 $m->maindepart_position = Input::get('txt_position');
				 $m->maindepart_sorting = $maxsorting;
				 $m->maindepart_show			= Input::get('txt_show');
		 		 $m->maindepart_showhome		= Input::get('txt_showhome');
				 $m->parent_id = Input::get('parent_id');
				 $m->created_at = date('Y-m-d H:i:s');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->create_by = Auth::user()->id;
				 $m->save();
				return Redirect::to('backend/depart/add/maindepart')->with(
				array(
					'save-success' => 'save'
				       ));
			break;
			case 'subdepart':
				$max = Subdepart::where('subdepart_categories',Input::get('txt_categories'))->max('subdepart_sorting');
				$maxsorting = $max + 1;
				$url = Helpers::create_url(Input::get('txt_name'));
				 $m = new Subdepart;
				 $m->subdepart_name = Input::get('txt_name') ;
				 $m->subdepart_categories = Input::get('txt_categories');
				 $m->subdepart_type = Input::get('txt_type');
				 $m->subdepart_detail = Input::get('txt_detail');
				 $m->subdepart_all_detail = Input::get('txt_all_detail');
				 $m->subdepart_url = Input::get('txt_url');
				 $m->subdepart_keyword = Input::get('web_keyword');
				 $m->s_url = $url;
				 $m->subdepart_show	= Input::get('txt_show');
				 $m->subdepart_sorting	= $maxsorting;
				 $m->created_at = date('Y-m-d H:i:s');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->create_by = Auth::user()->id;
				 $m->save();

				 
				return Redirect::to('backend/depart/add/subdepart')->with(
				array(
					'save-success' => 'save'
				       ));
			break;

		endswitch;
	}
	public function getEdit($type,$id)
	{
		switch($type):
			case 'maindepart': 
				$title = Lang::get('msg.msg_edit',array(),'th').' '.Lang::get('msg.maindepart',array(),'th');
				$m = Maindepart::find($id);
				$sql = Maindepart::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				return View::make('backend.depart.editmaindepart')->with(
					array(
					'title' 	=>$title,
                     'm' => $m,
                     'rules' => $rules,
                     'sql'=>$sql,
                     'id' => $id
					));
			break;

			case 'subdepart': 
				$title = Lang::get('msg.msg_edit',array(),'th').' '.Lang::get('msg.subdepart',array(),'th');
				$m = Subdepart::find($id);
				$sql = Maindepart::where('maindepart_type','3')->orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				return View::make('backend.depart.editsubdepart')->with(
					array(
					'title' 	=>$title,
                                                    'm' 	=> $m,
                                                    'sql'	=> $sql,
                                                    'rules' 	=> $rules
					));
			break;

		endswitch;	
	}

	public function postEdit($type)
	{
		   $id = Input::get('id');
		 switch($type):
			case 'maindepart': 
				$m = Maindepart::find($id);

				if (Input::hasFile('picture')){
					$Path='uploadfiles/depart'; 
					$Path_thumb = $Path.'/thumb';
					$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
					//return $img_name;
					 // $height= Image::make(Input::file('picture'))->height();
					 // $width= Image::make(Input::file('picture'))->width();
					 $imagesize=getimagesize(Input::file('picture'));
					if($imagesize[0] >'1024'){
						$width ='1024';
					}else{
						$width = $imagesize[0];
					}
					 
					 $height=round($width*$imagesize[1]/$imagesize[0]);
					
					Image::make(Input::file('picture'))->resize($width, $height)->save($Path.'/'.$img_name);
					 
					Image::make(Input::file('picture'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
					 $filename = Input::file('picture')->getClientOriginalName();
				 }else{
					 $img_name=$m->maindepart_logo;
				 }

				$url = Helpers::create_url(Input::get('txt_name'));
				$m->maindepart_name = Input::get('txt_name') ;
				 $m->maindepart_type = Input::get('txt_type');
				 $m->maindepart_detail = Input::get('txt_detail');
				 $m->maindepart_url = Input::get('txt_url');
				  $m->maindepart_embed = Input::get('txt_embed');
				 $m->maindepart_logo = $img_name;
				 $m->maindepart_show			= Input::get('txt_show');
		 		 $m->maindepart_showhome		= Input::get('txt_showhome');
				 $m->parent_id = Input::get('parent_id');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->m_url = $url;
				 $m->save();
				 return Redirect::to('backend/depart/maindepart')->with(
				array(
					'edit-success' => 'edit'
				       ));
			break;
			case 'subdepart': 
				$m = Subdepart::find($id);
				$url = Helpers::create_url(Input::get('txt_name'));
				$m->subdepart_name = Input::get('txt_name') ;
				 $m->subdepart_categories = Input::get('txt_categories');
				 $m->subdepart_type = Input::get('txt_type');
				 $m->subdepart_detail = Input::get('txt_detail');
				 $m->subdepart_all_detail = Input::get('txt_all_detail');
				 $m->subdepart_url = Input::get('txt_url');
				 $m->subdepart_keyword = Input::get('web_keyword');
				 $m->subdepart_show	= Input::get('txt_show');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->s_url = $url;
				 $m->save();
				 return Redirect::to('backend/depart/subdepart')->with(
				array(
					'edit-success' => 'edit'
				       ));
			break;
		 endswitch;
	}
	 

	

}