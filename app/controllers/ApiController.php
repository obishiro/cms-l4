<?php

class ApiController extends BaseController {

	public function getDatacategories ()
	{
 	//$sql = Categories::orderBy('id','desc')->get();
		$sql = Categories::select([
			 
			'id','categories_name','categories_url','sort','categories_show','categories_showhome'])
		->orderBy('sort','asc');
		$datatables = Datatables::of($sql)
         ->filter(function($query){
      if(Auth::user()->user_state==2) {
        $query ->where('tb_categories.create_by',Auth::user()->id);
      }
    })
		->removeColumn('id')
		->addColumn('no','')
          ->editColumn('categories_name','<a href="{{ URL::to(\'allnews\',array($categories_url))}}" target="_blank">{{$categories_name}}</a> ')
          ->editColumn('sort','{{Helpers::Sorting("categories","$id")}}')
         // ->editColumn('sort','<button class="text-danger" id=\"topsorting\">sort</button>')
          ->editColumn('categories_show','@if($categories_show==1)  <a class="topsorting" href=\'{{ URL::to(\'backend/unshow/categories\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/categories\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
          ->editColumn('categories_showhome','@if($categories_showhome==1)  <a href=\'{{ URL::to(\'backend/unshow/categoriesshowhome\',array($id))}}\'><i class="fa fa-home fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/categoriesshowhome\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')

          ->removeColumn('categories_url')
		->addColumn('tools',
       '
      @if(Session::get(\'permission\')==1 || Session::get(\'permission\')==3 || Session::get(\'permission\')==6)
			<a href="{{URL::to(\'backend/edit/categories\',array($id))}}" class="btn btn-info" >
			<i class="fa fa-pencil"></i>'.
			  Lang::get('msg.msg_edit',array(),'th').'
			</a>
			<a href="{{URL::to(\'backend/del/categories\',array($id))}}" class="btn btn-danger" onclick="javascript:return confirm(\''.Lang::get('msg.msg_confirm',array(),'th').'\')" >
			<i class="fa fa-trash"></i>
			 '.Lang::get('msg.msg_del',array(),'th').'
			</a>
      @elseif (Session::get(\'permission\')=="2")
      <a href="{{URL::to(\'backend/edit/categories\',array($id))}}"class="btn btn-info" >
      <i class="fa fa-pencil"></i>'.
        Lang::get('msg.msg_edit',array(),'th').'
      </a>
      @elseif (Session::get(\'permission\')=="4")
      @elseif (Session::get(\'permission\')=="5")
      <a href="{{URL::to(\'backend/del/categories\',array($id))}}" class="btn btn-danger" onclick="javascript:return confirm(\''.Lang::get('msg.msg_confirm',array(),'th').'\')" >
      <i class="fa fa-trash"></i>
       '.Lang::get('msg.msg_del',array(),'th').'
      </a>
      @endif
			
			');
		 

        return $datatables->make(true);
		 
		
		 
	}
  public function getDatatag ()
  {
  
    $sql = Tag::select([
       
      'id','tag_name','updated_at'])
    ->orderBy('id','desc');
   // $datatables = Datatables::of($sql)
    $datatables = Datatables::of($sql)
    ->filter(function($query){
 if(Auth::user()->user_state==2) {
   $query ->where('create_by',Auth::user()->id);
 }
})
    ->removeColumn('id')
    ->addColumn('no','')
    ->addColumn('tools','
      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#Modal-add-{{$id}}">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/tag\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
      <div class="modal fade" id="Modal-add-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-dialog">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                 <i class="fa fa-pencil"></i> {{ Lang::get(\'msg.msg_edit\',array(),\'th\') }}                                         
                                                     
                                                </h4>
                                              </div>
                                              <div class="modal-body" style="border-bottom:0px">
                                                 
                                                {{ Form::open(array(
                                                  \'method\'=>\'POST\',\'name\'=>\'editform\'
                                                  ,\'url\'=>\'backend/edit/tag\'
                                                   
                                                  ))}}
                                                 
                                                {{Form::label(\'label\',Lang::get(\'msg.tag_name\', array(), \'th\'))}}
                                                {{Form::input(\'text\', \'txt_name\', $tag_name, 
                                                array(
                                                  \'class\'=>\'form-control col-xs-12\',
                                                  \'style\'=>\'width:100%;margin-bottom:15px\'

                                                  ))}}
                                                 
                        
                                                 <div class="modal-footer"  style="border:0px">
                                                
                                               <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <i class="fa fa-close"></i>
                                                {{ Lang::get(\'msg.msg_cancle\',array(), \'th\')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-eye"></i>
                                                {{ Lang::get(\'msg.msg_submit\',array(), \'th\')}}
                                            </button>
                                            {{Form::hidden("id",$id)}}
                                            {{ Form::close()}}
                                              </div>
                                                
                                            </div>
                                              </div>

                                          </div>
                                        </div>
      ');
     

        return $datatables->make(true);
     
    
     
  }
    public function getDataprefix ()
  {
  
    $sql = Prefix::select([
       
      'id','prefix_name','updated_at'])
    ->orderBy('id','desc');
    $datatables = Datatables::of($sql)
    ->removeColumn('id')
    ->addColumn('no','')
    ->addColumn('tools','
      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#Modal-add-{{$id}}">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/prefix\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
      <div class="modal fade" id="Modal-add-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                          <div class="modal-dialog">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">
                                                 <i class="fa fa-pencil"></i> {{ Lang::get(\'msg.msg_edit\',array(),\'th\') }}                                         
                                                     
                                                </h4>
                                              </div>
                                              <div class="modal-body" style="border-bottom:0px">
                                                 
                                                {{ Form::open(array(
                                                  \'method\'=>\'POST\',\'name\'=>\'editform\'
                                                  ,\'url\'=>\'backend/edit/prefix\'
                                                   
                                                  ))}}
                                                 
                                                {{Form::label(\'label\',Lang::get(\'msg.prefix_name\', array(), \'th\'))}}
                                                {{Form::input(\'text\', \'txt_name\', $prefix_name, 
                                                array(
                                                  \'class\'=>\'form-control col-xs-12\',
                                                  \'style\'=>\'width:100%;margin-bottom:15px\'

                                                  ))}}
                                                 
                        
                                                 <div class="modal-footer"  style="border:0px">
                                                
                                               <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <i class="fa fa-close"></i>
                                                {{ Lang::get(\'msg.msg_cancle\',array(), \'th\')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                              <i class="fa fa-eye"></i>
                                                {{ Lang::get(\'msg.msg_submit\',array(), \'th\')}}
                                            </button>
                                            {{Form::hidden("id",$id)}}
                                            {{ Form::close()}}
                                              </div>
                                                
                                            </div>
                                              </div>

                                          </div>
                                        </div>
      ');
     

        return $datatables->make(true);
     
    
     
  }
  public function getDatamainmenu ()
  {
  
    $sql = Mainmenu::select([
       
      'id','mainmenu_name','mainmenu_type','mainmenu_sorting','mainmenu_position','mainmenu_show','m_url','created_at','updated_at'])
    ->orderBy('mainmenu_position', 'asc')
    ->orderBy('mainmenu_sorting','asc');
    $datatables = Datatables::of($sql)
     ->filter(function($query){
      if(Auth::user()->user_state==2) {
        $query ->where('tb_mainmenu.create_by',Auth::user()->id);
      }
    })
    ->removeColumn('id')
    ->removeColumn('m_url')
    ->editColumn('mainmenu_name','@if($mainmenu_type==1) <a target="_blank" href="{{ URL::to(\'menu\',array($m_url))}}">{{$mainmenu_name}}</a> @else {{$mainmenu_name}} @endif ')
    ->editColumn('mainmenu_type','{{ Helpers::mainmenutype($mainmenu_type) }}')
    ->editColumn('mainmenu_position','{{ Helpers::mainmenuposition($mainmenu_position) }}')
    ->editColumn('mainmenu_sorting','{{Helpers::Sorting("mainmenu","$id")}}')
    ->editColumn('mainmenu_show','@if($mainmenu_show==1)  <a href=\'{{ URL::to(\'backend/unshow/mainmenu\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/mainmenu\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
    ->addColumn('no','')
    ->addColumn('tools','
       @if(Session::get(\'permission\')==1 || Session::get(\'permission\')==3 || Session::get(\'permission\')==6)
      <a href="{{ URL::to(\'backend/menu/edit/mainmenu\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/mainmenu\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
      @elseif(Session::get(\'permission\')==2)
        <a href="{{ URL::to(\'backend/menu/edit/mainmenu\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      @elseif(Session::get(\'permission\')==4)
      @elseif(Session::get(\'permission\')==5)
      <a href="{{ URL::to(\'backend/del/mainmenu\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
      @endif
       
      ');
     

        return $datatables->make(true);
     
    
     
  }
  public function postDataSubmenu($id)
  {
      $sql = Submenu::orderBy('id','desc')->where( array('submenu_categories' => $id ))->get();
      Helpers::get_submenu($sql);
  }
  public function getDatasubmenu ()
  {
  
    $sql = Submenu::select([
       
      'tb_submenu.id','tb_submenu.submenu_name','tb_submenu.submenu_show','tb_mainmenu.mainmenu_name'
      ,'tb_submenu.submenu_type','tb_submenu.submenu_sorting'
      ])

    ->join('tb_mainmenu','tb_mainmenu.id','=','tb_submenu.submenu_categories')
    ->orderBy('tb_submenu.submenu_categories','asc')
    ->orderBy('tb_submenu.submenu_sorting','asc');
    
    $datatables = Datatables::of($sql)
     ->filter(function($query){
      if(Auth::user()->user_state==2) {
        $query ->where('tb_submenu.create_by',Auth::user()->id);
      }
    })
    ->removeColumn('id')
  //  ->editColumn('submenu_categories','{{ Helpers::mainmenutype($submenu_type) }}')
     ->editColumn('submenu_show','@if($submenu_show==1)  <a href=\'{{ URL::to(\'backend/unshow/submenu\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/submenu\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
    ->editColumn('submenu_type','{{ Helpers::mainmenutype($submenu_type) }}')
    ->editColumn('submenu_sorting','{{Helpers::Sorting("submenu","$id")}}')
    ->addColumn('no','')
    ->addColumn('tools','
       @if(Session::get(\'permission\')==1 || Session::get(\'permission\')==3 || Session::get(\'permission\')==6)
      <a href="{{ URL::to(\'backend/menu/edit/submenu\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      <a href="{{ URL::to(\'backend/del/submenu\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
      @elseif(Session::get(\'permission\')==2)
      <a href="{{ URL::to(\'backend/menu/edit/submenu\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
       {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
      </a>
      @elseif(Session::get(\'permission\')==4)
      @elseif(Session::get(\'permission\')==5)
       <a href="{{ URL::to(\'backend/del/submenu\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
       {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
      </a>
      @endif
       
      ');
             return $datatables->make(true);
      }

    public function getDatacontent ()
  {
     
  
    $sql = Content::select([
       
      'tb_content.id','tb_content.content_name','tb_categories.categories_name'
      ,'tb_content.content_categories','tb_content.content_url','tb_content.content_show','tb_content.content_showhome'

      ])
    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
   
    //->join('tb_files','tb_files.token','=','tb_content.content_file')
    ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->filter(function($query){
      if(Auth::user()->user_state==2) {
        $query ->where('tb_content.created_by',Auth::user()->id);
      }
    })

    ->removeColumn('id')
    ->removeColumn('content_url')
    ->editColumn('content_name','<a href="{{ URL::to(\'news\',array($content_url))}}" target="_blank">{{$content_name}}</a> ')
   
    ->editColumn('content_categories','{{$categories_name}}')
   
    ->editColumn('content_show','@if($content_show==1)  <a href=\'{{ URL::to(\'backend/unshow/content\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/content\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
    ->editColumn('content_showhome','@if($content_showhome==1)  <a href=\'{{ URL::to(\'backend/unshow/showhome\',array($id))}}\'><i class="fa fa-home fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/showhome\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')

    ->addColumn('no','')
    ->addColumn('tools','
      @if(Session::get(\'permission\')==1)
      <a href="{{ URL::to(\'backend/content/editcontent\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i></a>
      <a href="{{ URL::to(\'backend/del/content\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
      
      </a>
      @elseif (Session::get(\'permission\')=="2")
      <a href="{{ URL::to(\'backend/content/editcontent\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i></a>
       @elseif(Session::get(\'permission\')==3)
      <a href="{{ URL::to(\'backend/content/editcontent\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i></a>
      <a href="{{ URL::to(\'backend/del/content\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
      </a>
      @elseif(Session::get(\'permission\')==4)
      
      @elseif(Session::get(\'permission\')==5)
      
      <a href="{{ URL::to(\'backend/del/content\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
      </a>
       @elseif(Session::get(\'permission\')==6)
      <a href="{{ URL::to(\'backend/content/editcontent\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i></a>
      <a href="{{ URL::to(\'backend/del/content\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
      </a>
      @endif

       
      ');
             return $datatables->make(true);
      }



       public function getDataviewcategories($id,$url)
      {
       $sql = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url'
        ])
        ->where('tb_categories.id','=',$id)->where('tb_categories.categories_url','=',$url)
    ->join('tb_content','tb_content.content_file','=','tb_files.token')
    ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
    ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->addColumn('no','')
    ->removeColumn('id')
    ->editColumn('created_by','Administrator')
    ->editColumn('content_name','<a href="{{ URL::to("content",array($id,$content_url))}}">{{ $content_name }}</a>')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('files_size','{{number_format($files_size/1024)}} kb.')
    ->removeColumn('content_url');
    return $datatables->make(true);
      }
       public function getDataviewtag($id,$url)
      {
       $sql = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url'
        ])
        ->where('tb_tagcontent.tag_id','=',$id)
        ->join('tb_content','tb_content.content_file','=','tb_files.token')
       // ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
        ->join('tb_tagcontent','tb_tagcontent.content_id','=','tb_content.id')
        ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->addColumn('no','')
    ->removeColumn('id')
    ->editColumn('created_by','Administrator')
    ->editColumn('content_name','<a href="{{ URL::to("content",array($id,$content_url))}}">{{ $content_name }}</a>')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('files_size','{{number_format($files_size/1024)}} kb.')
    ->removeColumn('content_url');
    return $datatables->make(true);
      }

       public function getDataviewsearch($text)
      {
       $sql = Uploadfiles::select(['tb_files.files_type'
        ,'tb_content.content_name','tb_content.id'
        ,'tb_content.created_by','tb_content.created_at','tb_files.files_size','tb_content.content_view',
        'tb_content.content_url','tb_categories.categories_name'
        ])
        ->where('tb_content.content_name','like','%'.$text.'%')
        ->orWhere('tb_categories.categories_name','like','%'.$text.'%')
        ->join('tb_content','tb_content.content_file','=','tb_files.token')
        ->join('tb_categories','tb_categories.id','=','tb_content.content_categories')
        ->join('tb_tagcontent','tb_tagcontent.content_id','=','tb_content.id')
        ->groupBy('tb_content.id')
        ->orderBy('tb_content.id','desc');
    $datatables = Datatables::of($sql)
    ->addColumn('no','')
    ->removeColumn('id')
    ->editColumn('created_by','Administrator')
    ->editColumn('content_name','<a href="{{ URL::to("content",array($id,$content_url))}}">{{ $content_name }}</a>')
    ->editColumn('content_view','{{ number_format($content_view) }}')
    ->editColumn('files_type','{{ Helpers::filestype($files_type) }}')
    ->editColumn('files_size','{{number_format($files_size/1024)}} kb.')
    ->removeColumn('content_url');
    return $datatables->make(true);
      }



      public function getDatagallery ()
  {
  
    $sql = Gallery::select([
       
      'tb_gallery.id','tb_gallery.gallery_name','tb_gallery.gallery_url','tb_gallery.gallery_sorting','tb_gallery.gallery_show'
      ])
   // ->join('tb_categories','tb_categories.id','=','tb_gallery.gallery_categories')
   // ->join('tb_files_gallery','tb_files_gallery.token','=','tb_gallery.gallery_file')
    //->groupBy('tb_gallery.id')
   // ->orderBy('tb_submenu.submenu_sorting','asc');
    ->orderBy('tb_gallery.gallery_sorting','asc');
    $datatables = Datatables::of($sql)
         ->filter(function($query){
      if(Auth::user()->user_state==2) {
        $query ->where('tb_gallery.created_by',Auth::user()->id);
      }
    })
    ->removeColumn('id')
    ->removeColumn('gallery_url')
    ->editColumn('gallery_sorting','{{Helpers::Sorting("gallery","$id")}}')
      ->editColumn('gallery_show','@if($gallery_show==1)  <a href=\'{{ URL::to(\'backend/unshow/gallery\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/gallery\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
     
    ->editColumn('gallery_name','<a href="{{ URL::to(\'gallery\',array($id,$gallery_url))}}" target="_blank">{{$gallery_name}}</a> ')
   ->addColumn('no','')
    ->addColumn('tools','
       @if(Session::get(\'permission\')==1 || Session::get(\'permission\')==3 || Session::get(\'permission\')==6)
      <a href="{{ URL::to(\'backend/gallery/editgallery\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i></a>
      <a href="{{ URL::to(\'backend/del/gallery\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i></a>
       @elseif(Session::get(\'permission\')==2)
        <a href="{{ URL::to(\'backend/gallery/editgallery\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i></a>
       @elseif(Session::get(\'permission\')==4)
       @elseif(Session::get(\'permission\')==5)
       <a href="{{ URL::to(\'backend/del/gallery\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i></a>
      @endif
       
      ');
             return $datatables->make(true);
      }


          public function getDatabanner ()
     {
  
    $sql = Banner::select([
       
      'tb_banner.id','tb_banner.banner_name','tb_banner.banner_show','tb_banner.updated_at'
      ])
   // ->join('tb_categories','tb_categories.id','=','tb_banner.banner_categories')
   // ->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
    //->groupBy('tb_banner.id')
    ->orderBy('tb_banner.id','desc');
    $datatables = Datatables::of($sql)
    ->filter(function($query){
 if(Auth::user()->user_state==2) {
   $query ->where('tb_banner.created_by',Auth::user()->id);
 }
})
    ->removeColumn('id')
  
      ->editColumn('banner_show','@if($banner_show==1)  <a href=\'{{ URL::to(\'backend/unshow/banner\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/banner\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
    ->editColumn('banner_name','{{$banner_name}} ')
   ->addColumn('no','')
    ->addColumn('tools','
      
      <a href="{{ URL::to(\'backend/banner/editbanner\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
      
      </a>
      <a href="{{ URL::to(\'backend/del/banner\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
      
      </a>
       
      ');
             return $datatables->make(true);
      }

      public function getDatauser()
      {
        $sql = User::select([
       
          'users.id','tb_profiles.firstName','tb_profiles.lastName','users.user_type','users.user_status','tb_profiles.photoURL',
          'tb_profiles.email','tb_profiles.phone'
      ])
    ->join('tb_profiles','tb_profiles.user_id','=','users.id')
   // ->join('tb_files_banner','tb_files_banner.token','=','tb_banner.banner_file')
    //->groupBy('tb_banner.id')
    ->where('users.user_type',1)
    ->orderBy('users.id','desc');
    $datatables = Datatables::of($sql)
    ->removeColumn('id')
  
      ->editColumn('photoURL','<img src="{{ URL::to(\'uploadfiles/users/thumb\',array($photoURL)) }}">')
    ->editColumn('user_type','{{ Helpers::usertype($user_type) }}')
    ->editColumn('user_status','@if($user_status==1)  <a href=\'{{ URL::to(\'backend/unshow/user\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/user\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
   ->addColumn('no','')
    ->addColumn('tools','
      
      <a href="{{ URL::to(\'backend/user/edituser\',array($id)) }}" class="btn btn-info">
      <i class="fa fa-pencil"></i>
      
      </a>
      <a href="{{ URL::to(\'backend/del/user\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
      <i class="fa fa-trash"></i>
      
      </a>
       
      ');
             return $datatables->make(true);
      }
      public function getDatamaindepart ()
      {
      
        $sql = Maindepart::select([
           
          'id','maindepart_name','maindepart_type','maindepart_sorting','maindepart_position','maindepart_show','m_url','created_at','updated_at'])
        ->orderBy('maindepart_position', 'asc')
        ->orderBy('maindepart_sorting','asc');
        $datatables = Datatables::of($sql)
         ->filter(function($query){
          if(Auth::user()->user_state==2) {
            $query ->where('tb_maindepart.create_by',Auth::user()->id);
          }
        })
        ->removeColumn('id')
        ->removeColumn('m_url')
        ->editColumn('maindepart_name','@if($maindepart_type==1) <a target="_blank" href="{{ URL::to(\'depart\',array($m_url))}}">{{$maindepart_name}}</a> @else {{$maindepart_name}} @endif ')
        ->editColumn('maindepart_type','{{ Helpers::maindeparttype($maindepart_type) }}')
      //  ->editColumn('maindepart_position','{{ Helpers::maindepartposition($maindepart_position) }}')
        ->editColumn('maindepart_sorting','{{Helpers::Sorting("maindepart","$id")}}')
        ->editColumn('maindepart_show','@if($maindepart_show==1)  <a href=\'{{ URL::to(\'backend/unshow/maindepart\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/maindepart\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
        ->addColumn('no','')
        ->addColumn('tools','
           @if(Session::get(\'permission\')==1 || Session::get(\'permission\')==3 || Session::get(\'permission\')==6)
          <a href="{{ URL::to(\'backend/depart/edit/maindepart\',array($id)) }}" class="btn btn-info">
          <i class="fa fa-pencil"></i>
           {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
          </a>
          <a href="{{ URL::to(\'backend/del/maindepart\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
          <i class="fa fa-trash"></i>
           {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
          </a>
          @elseif(Session::get(\'permission\')==2)
            <a href="{{ URL::to(\'backend/depart/edit/maindepart\',array($id)) }}" class="btn btn-info">
          <i class="fa fa-pencil"></i>
           {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
          </a>
          @elseif(Session::get(\'permission\')==4)
          @elseif(Session::get(\'permission\')==5)
          <a href="{{ URL::to(\'backend/del/maindepart\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
          <i class="fa fa-trash"></i>
           {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
          </a>
          @endif
           
          ');
         
    
            return $datatables->make(true);
         
        
         
      }
      public function getDatasubdepart ()
      {
      
        $sql = Subdepart::select([
           
          'tb_subdepart.id','tb_subdepart.subdepart_name','tb_subdepart.subdepart_show','tb_maindepart.maindepart_name'
          ,'tb_subdepart.subdepart_type','tb_subdepart.subdepart_sorting'
          ])
    
        ->join('tb_maindepart','tb_maindepart.id','=','tb_subdepart.subdepart_categories')
        ->orderBy('tb_subdepart.subdepart_categories','asc')
        ->orderBy('tb_subdepart.subdepart_sorting','asc');
        
        $datatables = Datatables::of($sql)
         ->filter(function($query){
          if(Auth::user()->user_state==2) {
            $query ->where('tb_subdepart.create_by',Auth::user()->id);
          }
        })
        ->removeColumn('id')
      //  ->editColumn('subdepart_categories','{{ Helpers::maindeparttype($subdepart_type) }}')
         ->editColumn('subdepart_show','@if($subdepart_show==1)  <a href=\'{{ URL::to(\'backend/unshow/subdepart\',array($id))}}\'><i class="fa fa-eye fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'backend/show/subdepart\',array($id))}}\'><i class="fa fa-eye-slash fa-2x"></i></i></a> @endif')
        ->editColumn('subdepart_type','{{ Helpers::maindeparttype($subdepart_type) }}')
        ->editColumn('subdepart_sorting','{{Helpers::Sorting("subdepart","$id")}}')
        ->addColumn('no','')
        ->addColumn('tools','
           @if(Session::get(\'permission\')==1 || Session::get(\'permission\')==3 || Session::get(\'permission\')==6)
          <a href="{{ URL::to(\'backend/depart/edit/subdepart\',array($id)) }}" class="btn btn-info">
          <i class="fa fa-pencil"></i>
           {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
          </a>
          <a href="{{ URL::to(\'backend/del/subdepart\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
          <i class="fa fa-trash"></i>
           {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
          </a>
          @elseif(Session::get(\'permission\')==2)
          <a href="{{ URL::to(\'backend/depart/edit/subdepart\',array($id)) }}" class="btn btn-info">
          <i class="fa fa-pencil"></i>
           {{ Lang::get(\'msg.msg_edit\',array(),\'th\')}}
          </a>
          @elseif(Session::get(\'permission\')==4)
          @elseif(Session::get(\'permission\')==5)
           <a href="{{ URL::to(\'backend/del/subdepart\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.msg_confirm\',array(),\'th\') }}\')" >
          <i class="fa fa-trash"></i>
           {{ Lang::get(\'msg.msg_del\',array(),\'th\')}}
          </a>
          @endif
           
          ');
                 return $datatables->make(true);
          }
  
    public function postSubdepart($id)
    {
     $data =  Subdepart::where('subdepart_categories',$id)->get();
        echo "<option value='0'>- เลิอก -</option>";
     foreach ($data as $newdata=>$d)
     {
        echo "<option value=\"$d->id\">".$d->subdepart_name."</option>";
     }
    }

}