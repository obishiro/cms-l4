<?php

class BackendController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backend
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('backend.content');
	}

	 
	public function postLogin()
		{   
     		 
    	 	$rules = array(
			'username'	=> 'required',
			'password'	=> 'required'

								);
				$validator = Validator::make(Input::all(), $rules);

    		if ($validator->fails())
    			{
       		 		return Redirect::to('login')->withErrors($validator)->withInput();
     			}else{
 
		       if(Auth::attempt(array('username'=>Input::get('username'),'password'=>Input::get('password')))) {
				
					
		       			switch(Auth::user()->user_status):
		       				case '0':
		       					return Redirect::to((Request::segment(1).'/login'));
		       				break;
		       				case '1':
								if(Auth::user()->user_type<=2)
								{

		       				    $profiles = DB::table('tb_profiles')
		       				    ->select('tb_profiles.firstName','tb_profiles.lastName','tb_profiles.photoURL','tb_permission.p_permission')
		       				    ->join('tb_permission','tb_permission.user_id','=','tb_profiles.user_id')
		       				    ->where('tb_profiles.user_id',Auth::user()->id)->first();
		       				    Session::put('permission', $profiles->p_permission);
		       				    Session::put('firstName', $profiles->firstName);
		       				    Session::put('lastName', $profiles->lastName);
		       				    Session::put('photoURL', $profiles->photoURL);
								
		       					return Redirect::to('backend/content');
								
								}else{ //go to arosystem
									switch(Auth::user()->user_type):
										case '3': //army
											$sql = Army::where('army_number',Auth::user()->army_id)->first();
											$name = $sql->army_name;
											$short_name = $sql->army_shortname;
											$uid = Auth::user()->army_id;

											break;
										case '4': //mtb
											$sql = Mtb::where('id',Auth::user()->mtb_id)->first();
											$name = $sql->mtb_name;
											$short_name = $sql->mtb_shortname;
											$uid =Auth::user()->mtb_id;

											break;
										case '5': //prov
											$sql = Prov::where('PROVINCE_ID',Auth::user()->prov_id)->first();
											$name = "สำนักงานสัสดี/จังหวัด". $sql->PROVINCE_NAME;
											$short_name = $sql->Prov_shortname;
											$uid=Auth::user()->prov_id;
											break;
										case '6': //district
											$sql = Amphur::where('AMPHUR_CODE',Auth::user()->district_id)->first();
											$name = "หน่วยสัสดีเขต/อำเภอ ".$sql->AMPHUR_NAME;
											$short_name = '';
											$uid = Auth::user()->district_id;
											break;
										endswitch;
										Session::put('name', $name);
										Session::put('short_name', $short_name);
										Session::put('uid', $uid);


									return Redirect::to('aro');
									//return Redirect::action('AroController@index')->array($name,$short_name);
								
								}
		       				break;
							


		       			endswitch;
		       	
		       
		            }else{
									return Redirect::to((Request::segment(1).'/login'))
				        ->with('error_login_incorrect',Lang::get('msg.error_login_incorrect',array(),'th'));
				      }
				  }
			  } 

public function getShow($type,$id)
{
	switch ($type) {
		case 'categories':
			$c= Categories::find($id);
			$c->categories_show = 1;
			$c->save();
			return Redirect::action('BackendController@getConfig',array($type));
			break;
		case 'content':
			$c= Content::find($id);
			$c->content_show = 1;
			$c->save();
			return Redirect::to('backend/content');
			break;
		case 'mainmenu':
			$c= Mainmenu::find($id);
			$c->mainmenu_show = 1;
			$c->save();
			return Redirect::to('backend/menu/mainmenu');
			break;
		case 'submenu':
			$c= Submenu::find($id);
			$c->submenu_show = 1;
			$c->save();
			return Redirect::to('backend/menu/submenu');
			break;
			case 'maindepart':
			$c= Maindepart::find($id);
			$c->maindepart_show = 1;
			$c->save();
			return Redirect::to('backend/depart/maindepart');
			break;
		case 'subdepart':
			$c= Subdepart::find($id);
			$c->subdepart_show = 1;
			$c->save();
			return Redirect::to('backend/depart/subdepart');
			break;
		case 'showhome':
			$c= Content::find($id);
			$c->content_showhome = 1;
			$c->content_show = 1;
			$c->save();
			return Redirect::to('backend/content');
			break;
		case 'gallery':
			$c= Gallery::find($id);
			$c->gallery_show = 1;
			$c->save();
			return Redirect::to('backend/gallery');
			break;
		case 'banner':
			$c= Banner::find($id);
			$c->banner_show = 1;
			$c->save();
			return Redirect::to('backend/banner');
			break;
		case 'categoriesshowhome':
			$c= Categories::find($id);
			$c->categories_showhome = 1;
			$c->save();
			return Redirect::to('backend/config/categories');
			break;
		case 'user':
			$c= User::find($id);
			$c->user_status = 1;
			$c->save();
			return Redirect::to('backend/user');
			break;
		 
		
	}
		
}
public function getUnshow($type,$id)
{
	switch ($type) {
		case 'categories':
			$c= Categories::find($id);
			$c->categories_show = 0;
			$c->save();
			return Redirect::action('BackendController@getConfig',array($type));
			break;
		case 'content':
			$c= Content::find($id);
			$c->content_show = 0;
			$c->content_showhome = 0;
			$c->save();
			return Redirect::to('backend/content');
			break;
		case 'showhome':
			$c= Content::find($id);
			$c->content_showhome = 0;
			$c->save();
			return Redirect::to('backend/content');
			break;
		case 'gallery':
			$c= Gallery::find($id);
			$c->gallery_show = 0;
			$c->save();
			return Redirect::to('backend/gallery');
			break;
		case 'banner':
			$c= Banner::find($id);
			$c->banner_show = 0;
			$c->save();
			return Redirect::to('backend/banner');
			break;
		case 'mainmenu':
			$c= Mainmenu::find($id);
			$c->mainmenu_show = 0;
			$c->save();
			return Redirect::to('backend/menu/mainmenu');
			break;
		case 'submenu':
			$c= Submenu::find($id);
			$c->submenu_show = 0;
			$c->save();
			return Redirect::to('backend/menu/submenu');
			break;
			case 'maindepart':
			$c= Maindepart::find($id);
			$c->maindepart_show = 0;
			$c->save();
			return Redirect::to('backend/depart/maindepart');
			break;
		case 'subdepart':
			$c= Subdepart::find($id);
			$c->subdepart_show = 0;
			$c->save();
			return Redirect::to('backend/depart/subdepart');
			break;
		case 'categoriesshowhome':
			$c= Categories::find($id);
			$c->categories_showhome = 0;
			$c->save();
			return Redirect::to('backend/config/categories');
			break;
		case 'user':
			$c= User::find($id);
			$c->user_status = 0;
			$c->save();
			return Redirect::to('backend/user');
			break;
	 

	}
	
	 
}
public function getConfig($type)
{
	$id = Auth::user()->id;
	switch($type):
		case 'enviroment':
			$title= Lang::get('msg.config-env',array(),'th');
			//$rows = Enviroment::where('user_id',Auth::user()->id)->count();
			 
			$sql =Enviroment::where('created_by',$id)->first();
			
			return View::make('backend.config.enviroment')->with(
				 array(
				 	'title' =>$title,
				 	'data'	=>$sql,
				 	'rules' =>''
				       ));
		break;
		case 'password':
			$title= Lang::get('msg.config-password',array(),'th');
			$sql= User::find(Auth::user()->id);
			return View::make('backend.config.password')->with(
				 array(
				 	'title' =>$title,
				 	'data'	=>$sql,
				 	'rules' =>''
				       ));
		break;
		case 'categories':
			$title = Lang::get('msg.config-categories',array(),'th');
			$sql = Categories::where('create_by',Auth::user()->id)->orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required'];
			$api = URL::to('backend/data/categories');
			return View::make('backend.config.categories')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	'api'		=> $api,
				 	'status'	=> 'null',
				 	'sql'		=> $sql
				       ));

		break;
		case 'tag':
			$title = Lang::get('msg.config-tag',array(),'th');
			$sql = Tag::orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required','txt_type'=>'required'];
			$api = URL::to('backend/data/tag');
			return View::make('backend.config.tag')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

		break;
		case 'prefix':
			$title = Lang::get('msg.config-prefix',array(),'th');
			$sql = Prefix::orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required','txt_type'=>'required'];
			$api = URL::to('backend/data/prefix');
			return View::make('backend.config.prefix')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

		break;
		case 'mainmenu':
			$title = Lang::get('msg.mainmenu',array(),'th');
			$sql = Mainmenu::orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required'];
			$api = URL::to('backend/data/mainmenu');
			return View::make('backend.config.mainmenu')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	'api'		=> $api,
				 	'status'	=> 'null',
				 	'sql'		=> $sql
				       ));

		break;
		case 'css':
			$title = Lang::get('msg.config-css',array(),'th');
			return View::make('backend.config.css')->with(
				 array(
				 	'title' 	=>$title,
				 	
				       ));
		break;

	endswitch;


}
public function postConfig($type)
{
	switch($type):
		case 'enviroment':

		if (Input::hasFile('picture')){
			$sql = Enviroment::where('created_by',Auth::user()->id)->first();

			$Path='uploadfiles/logo'; 
		//	$Path_thumb = $Path.'/thumb';
		 
			@unlink($Path.'/'.$sql->web_logo);
			$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
			//return $img_name;
			 // $height= Image::make(Input::file('picture'))->height();
			 // $width= Image::make(Input::file('picture'))->width();
			 
			
			Image::make(Input::file('picture'))->save($Path.'/'.$img_name);
			 
		//	Image::make(Input::file('picture'))->resize(50,50)->save($Path_thumb.'/'.$img_name);
			 $filename = Input::file('picture')->getClientOriginalName();
		 }else{
			 $sql = Enviroment::where('created_by',Auth::user()->id)->first();
			 $img_name=$sql->web_logo;
		 }
			DB::table('tb_enviroment')
			->where('created_by',Auth::user()->id)
			->update(
                 array(
                 	'web_name_lo'	=> Input::get('web_name_lo'),
                 	'web_name_en'	=> Input::get('web_name_en'),
                 	'web_address'	=> Input::get('web_address'),
                 	'web_tel'		=> Input::get('web_tel'),
                 //	'web_url'		=> Input::get('web_url'),
                 	'web_email'	=> Input::get('web_email'),
                 	'web_keyword'	=> Input::get('web_keyword'),
                 	'web_detail'	=> Input::get('web_detail'),
                 	 'web_logo'		=> $img_name
                 	));
			return Redirect::to('backend/config/enviroment')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
		case 'password':
		 	$password = Input::get('password');
		 	$newpassword = Hash::make($password);
			  if($password ='' || $password ==null){

			$update=	DB::table('users')
			->where('id',Auth::user()->id)
			->update(
                 array(
                 	'username'	=> Input::get('username')
                 	 
                 	));

			}else{
			$update = DB::table('users')
			->where('id',Auth::user()->id)
			->update(
                 array(
                 	'username'	=> Input::get('username'),
                 	'password'	=> $newpassword
                 	 
                 	));
			}
			return Redirect::to('backend/config/password')->with(
				array(
					'save-success' => 'save'
				       ));
			 
		break;
		case 'categories':
			$sorting = Categories::max('sort');
			$maxsorting = $sorting + 1;
			$url = Helpers::create_url(Input::get('txt_name'));
			$c = new Categories;
			$c->categories_name = Input::get('txt_name');
			$c->categories_url =$url;
			$c->sort = $maxsorting;
			$c->display_type = Input::get('txt_displaytype');
			$c->display_embed = Input::get('txt_detail');
			$c->created_at = date('Y-m-d H:i:s');
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
		//	$c->parent_id = Input::get('parent_id');
			$c->categories_icon = Input::get('txt_icon');
			$c->parent_id =0;
			$c->categories_show = Input::get('txt_show');
			$c->show_title = Input::get('txt_show_title');
			$c->categories_showhome = Input::get('txt_showhome');
			$c->save();

			return Redirect::to('backend/config/categories')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
		case 'tag':
			$url = Helpers::create_url(Input::get('txt_name'));
			$c = new Tag;
			$c->tag_name = Input::get('txt_name');
			$c->tag_url =$url;
			$c->created_at = date('Y-m-d H:i:s');
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/tag')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
		case 'prefix':
			$url = Helpers::create_url(Input::get('txt_name'));
			$c = new Prefix;
			$c->prefix_name = Input::get('txt_name');
			$c->prefix_url =$url;
			$c->created_at = date('Y-m-d H:i:s');
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/prefix')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
		case 'css':
			$filename='styles.css';
			$template =  Config::get('app.template');
			$path =  'frontend/'.$template.'/css';
			$oldfiles = $path.'/'.$filename;
			$contents = Input::get('showcode');
		 	 File::delete($path.'/'.$filename);
			 
			File::put($path.'/'.$filename, $contents);
		return Redirect::to('backend/config/css')->with(
				array(
					'save-success' => 'save'
				       ));
		break;
		case 'getcss':
			$template =  Config::get('app.template');
			$path = URL::to('frontend/'.$template.'/css/styles.css');
			return  file_get_contents($path);
		break;
	endswitch;	
}
public function getDel($type,$id)
{
	switch($type):
		case 'enviroment':
		break;
		case 'categories':
		Categories::find($id)->delete();
		return Redirect::to('backend/config/categories')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'user':
		User::find($id)->delete();
		$pic = DB::table('tb_profiles')->where('user_id',$id)->first();
		$Path='uploadfiles/users'; 
		$Path_thumb = $Path.'/thumb';
		$file_delete_name = $Path."/".$pic->photoURL;
		$file_delete_thumb_name = $Path_thumb."/".$pic->photoURL;
		File::delete($file_delete_name);
		File::delete($file_delete_thumb_name);
		DB::table('tb_permission')->where('user_id',$id)->delete();
		DB::table('tb_profiles')->where('user_id',$id)->delete();
		return Redirect::to('backend/user')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'tag':
		Tag::find($id)->delete();
		return Redirect::to('backend/config/tag')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'prefix':
		Prefix::find($id)->delete();
		return Redirect::to('backend/config/prefix')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'prefix':
		Prefix::find($id)->delete();
		return Redirect::to('backend/config/prefix')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'mainmenu':
		Mainmenu::find($id)->delete();
		Submenu::where('submenu_categories',$id)->delete();
		return Redirect::to('backend/menu/mainmenu')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'submenu':
		$pic = Submenu::find($id);
		if($pic->submenu_pic !="")
		{
			@unlink('uploadfiles/picmenu/'.$pic->submenu_pic);
		}
		Submenu::find($id)->delete();
		return Redirect::to('backend/menu/submenu')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'maindepart':
		Maindepart::find($id)->delete();
		Subdepart::where('subdepart_categories',$id)->delete();
		return Redirect::to('backend/depart/maindepart')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'subdepart':
		 
		Subdepart::find($id)->delete();
		return Redirect::to('backend/depart/subdepart')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'content':
		 
			$c = Content::find($id);
			$token = $c->content_file;
			$img = Uploadfiles::where('token','=',$token)->first();
			$filename='uploadfiles/'.$img->files_newname; 
			File::delete($filename);
			$img = Uploadfiles::where('token',$token)->delete();
			$tag = Tagcontent::where('content_id',$id)->delete();
			$delcontent = Content::find($id)->delete();

		return Redirect::to('backend/content')->with(
				array(
					'del-success' => 'del'
				       ));
		break;
		case 'gallery':
			$g =Gallery::find($id);
			$token = $g->galley_file;
			$images = Uploadfilesgallery::where('token','=',$token)->get();
			foreach($images as $img_del=>$img)
			{
				$filename='uploadfiles/gallery/'.$img->files_newname;
		 	 	$filename_thumb='uploadfiles/gallery/thumb/'.$img->files_newname; 
			 	File::delete($filename);
			 	File::delete($filename_thumb);
				Uploadfilesgallery::where(array('token'=>$token))->delete();
			}
			Gallery::find($id)->delete();
			return Redirect::to('backend/gallery')->with(
				array(
					'del-success' => 'del'
				       ));

		break;
		case 'banner':
			$g =Banner::find($id);
			$token = $g->banner_file;
			$images = Uploadfilesbanner::where('token','=',$token)->get();
			foreach($images as $img_del=>$img)
			{
				$filename='uploadfiles/banner/'.$img->files_newname;
		 	 	$filename_thumb='uploadfiles/banner/thumb/'.$img->files_newname; 
			 	File::delete($filename);
			 	File::delete($filename_thumb);
				Uploadfilesbanner::where(array('token'=>$token))->delete();
			}
			Banner::find($id)->delete();
			return Redirect::to('backend/banner')->with(
				array(
					'del-success' => 'del'
				       ));

		break;
	endswitch;
}
public function getEdit($type,$id)
{
	 switch($type):
	 	case 'categories':
	 		$sql = Categories::orderBy('id','desc')->get();
			$rules = ['txt_name'=>'required'];
			$cat = Categories::find($id);

			return View::make('backend.config.editcategories')->with(array(
				'sql' => $sql,
				'rules' => $rules,
				'title' => $cat->categories_name,
				'name' =>$cat->categories_name,
				'categories_showhome' =>$cat->categories_showhome,
				'categories_show'	=>$cat->categories_show,
				'show_title'	=>$cat->show_title,
				'id' => $id,
				'display_type' =>$cat->display_type,
				'display_embed' =>$cat->display_embed,
				'icon'=>$cat->categories_icon
				));

	 	break;
	 endswitch;
}
public function postEdit($type)
{
	switch($type):
		case 'enviroment':
		break;
		case 'categories':
		$url = Helpers::create_url(Input::get('txt_name'));
			$id = Input::get('id');
			$c = Categories::find($id);
		
			$c->categories_name = Input::get('txt_name');
			$c->parent_id = Input::get('parent_id');
			$c->categories_url = $url;
			$c->updated_at = date('Y-m-d H:i:s');
			$c->categories_show = Input::get('txt_show');
			$c->show_title = Input::get('txt_show_title');
			$c->display_type = Input::get('txt_displaytype');
			$c->display_embed = Input::get('txt_detail');
			$c->categories_showhome = Input::get('txt_showhome');
			$c->categories_icon = Input::get('txt_icon');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/categories')->with(
				array(
					'edit-success' => 'edit'
				       ));
		break;
		case 'tag':
			$id = Input::get('id');
			$c = Tag::find($id);
			$url = Helpers::create_url(Input::get('txt_name'));
		
			$c->tag_name = Input::get('txt_name');
			$c->tag_url = $url;
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/tag')->with(
				array(
					'edit-success' => 'edit'
				       ));
		break;
		case 'prefix':
			$id = Input::get('id');
			$c = Prefix::find($id);
			$url = Helpers::create_url(Input::get('txt_name'));
		
			$c->prefix_name = Input::get('txt_name');
			$c->prefix_url = $url;
			$c->updated_at = date('Y-m-d H:i:s');
			$c->create_by =Auth::user()->id;
			$c->save();
			return Redirect::to('backend/config/prefix')->with(
				array(
					'edit-success' => 'edit'
				       ));
		break;
	endswitch;
}
 
public function getSorttop($type,$id)
{
	switch($type):
		case 'categories':
			 $s = Categories::find($id);
			 $p = $s->sort;
			 $isort = $p - 1;
             $s->sort = $isort;
             $s->save();
			 $usort = Categories::where('sort',$isort)->where('id','!=',$id)->first();
			 $up = $usort->sort+1;

			 Categories::where('id',$usort->id)
			 ->update(array('sort'=>$up));
			 return Redirect::to('backend/config/'.$type);

		break;
		case 'gallery':
		$s = Gallery::find($id);
		$p = $s->gallery_sorting;
		$isort = $p - 1;
					$s->gallery_sorting = $isort;
					$s->save();
		$usort = Gallery::where('gallery_sorting',$isort)->where('id','!=',$id)->first();
		$up = $usort->gallery_sorting+1;

		Gallery::where('id',$usort->id)
		->update(array('gallery_sorting'=>$up));
		return Redirect::to('backend/'.$type);

 break;
		case 'mainmenu':
			 $s = Mainmenu::find($id);
			 $p = $s->mainmenu_sorting;
			 $isort = $p - 1;
             $s->mainmenu_sorting = $isort;
             $s->save();
             $position = $s->mainmenu_position;
			 $usort = Mainmenu::where(array('mainmenu_sorting'=>$isort,'mainmenu_position'=>$position))->where('id','!=',$id)->first();
			 $up = $usort->mainmenu_sorting+1;

			 //DB::table('tb_mainmenu')
			 Mainmenu::where('id',$usort->id)
			 ->update(array('mainmenu_sorting'=>$up,'mainmenu_position'=>$position));
			 return Redirect::to('backend/menu/'.$type);

		break;
		case 'submenu':
			 $s = Submenu::find($id);
			 $p = $s->submenu_sorting;
			 $isort = $p - 1;
             $s->submenu_sorting = $isort;
             $s->save();
             $categories = $s->submenu_categories;
			 $usort = Submenu::where(array('submenu_sorting'=>$isort,'submenu_categories'=>$categories))->where('id','!=',$id)->first();
			 $up = $usort->submenu_sorting + 1;

			 //DB::table('tb_submenu')
			 Submenu::where('id',$usort->id)
			 ->update(array('submenu_sorting'=>$up,'submenu_categories'=>$categories));
			 return Redirect::to('backend/menu/'.$type);

		break;
		case 'maindepart':
		$s = Maindepart::find($id);
		$p = $s->maindepart_sorting;
		$isort = $p - 1;
					$s->maindepart_sorting = $isort;
					$s->save();
					$position = $s->maindepart_position;
		$usort = Maindepart::where(array('maindepart_sorting'=>$isort,'maindepart_position'=>$position))->where('id','!=',$id)->first();
		$up = $usort->maindepart_sorting+1;

		//DB::table('tb_maindepart')
		Maindepart::where('id',$usort->id)
		->update(array('maindepart_sorting'=>$up,'maindepart_position'=>$position));
		return Redirect::to('backend/depart/'.$type);

		 break;
		  
 case 'subdepart':
		$s = Subdepart::find($id);
		$p = $s->subdepart_sorting;
		$isort = $p - 1;
					$s->subdepart_sorting = $isort;
					$s->save();
					$categories = $s->subdepart_categories;
		$usort = Subdepart::where(array('subdepart_sorting'=>$isort,'subdepart_categories'=>$categories))->where('id','!=',$id)->first();
		$up = $usort->subdepart_sorting + 1;

		//DB::table('tb_subdepart')
		Subdepart::where('id',$usort->id)
		->update(array('subdepart_sorting'=>$up,'subdepart_categories'=>$categories));
		return Redirect::to('backend/depart/'.$type);

 break;
	endswitch;
}
public function getSortdown($type,$id)
{
	switch($type):
		case 'categories':
			 $s = Categories::find($id);
			 $p = $s->sort;
			 $isort = $p + 1;
             $s->sort = $isort;
             $s->save();
			 $usort = Categories::where('sort',$isort)->where('id','!=',$id)->first();
			 $up = $usort->sort-1;

			 //DB::table('tb_categories')
			 Categories::where('id',$usort->id)
			 ->update(array('sort'=>$up));
			 return Redirect::to('backend/config/'.$type);

		break;
		case 'gallery':
			 $s = Gallery::find($id);
			 $p = $s->gallery_sorting;
			 $isort = $p + 1;
             $s->gallery_sorting = $isort;
             $s->save();
			 $usort = Gallery::where('gallery_sorting',$isort)->where('id','!=',$id)->first();
			 $up = $usort->gallery_sorting-1;

			 //DB::table('tb_gallery')
			 Gallery::where('id',$usort->id)
			 ->update(array('gallery_sorting'=>$up));
			 return Redirect::to('backend/'.$type);

		break;
		case 'mainmenu':
			 $s = Mainmenu::find($id);
			 $p = $s->mainmenu_sorting;
			 $isort = $p + 1;
             $s->mainmenu_sorting = $isort;
             $s->save();
             $position = $s->mainmenu_position;
			 $usort = Mainmenu::where(array('mainmenu_sorting'=>$isort,'mainmenu_position'=>$position))->where('id','!=',$id)->first();
			 $up = $usort->mainmenu_sorting - 1;

			 //DB::table('tb_mainmenu')
			 Mainmenu::where('id',$usort->id)
			 ->update(array('mainmenu_sorting'=>$up,'mainmenu_position'=>$position));
			 return Redirect::to('backend/menu/'.$type);

		break;
		case 'submenu':
			 $s = Submenu::find($id);
			 $p = $s->submenu_sorting;
			 $isort = $p + 1;
             $s->submenu_sorting = $isort;
             $s->save();
             $categories = $s->submenu_categories;
			 $usort = Submenu::where(array('submenu_sorting'=>$isort,'submenu_categories'=>$categories))->where('id','!=',$id)->first();
			 $up = $usort->submenu_sorting - 1;

			 //DB::table('tb_submenu')
			 Submenu::where('id',$usort->id)
			 ->update(array('submenu_sorting'=>$up,'submenu_categories'=>$categories));
			 return Redirect::to('backend/menu/'.$type);

		break;
		case 'maindepart':
			 $s = Maindepart::find($id);
			 $p = $s->maindepart_sorting;
			 $isort = $p + 1;
             $s->maindepart_sorting = $isort;
             $s->save();
             $position = $s->maindepart_position;
			 $usort = Maindepart::where(array('maindepart_sorting'=>$isort,'maindepart_position'=>$position))->where('id','!=',$id)->first();
			 $up = $usort->maindepart_sorting - 1;

			 //DB::table('tb_maindepart')
			 Maindepart::where('id',$usort->id)
			 ->update(array('maindepart_sorting'=>$up,'maindepart_position'=>$position));
			 return Redirect::to('backend/depart/'.$type);

		break;
		case 'subdepart':
			 $s = Subdepart::find($id);
			 $p = $s->subdepart_sorting;
			 $isort = $p + 1;
             $s->subdepart_sorting = $isort;
             $s->save();
             $categories = $s->subdepart_categories;
			 $usort = Subdepart::where(array('subdepart_sorting'=>$isort,'subdepart_categories'=>$categories))->where('id','!=',$id)->first();
			 $up = $usort->subdepart_sorting - 1;

			 //DB::table('tb_subdepart')
			 Subdepart::where('id',$usort->id)
			 ->update(array('subdepart_sorting'=>$up,'subdepart_categories'=>$categories));
			 return Redirect::to('backend/depart/'.$type);

		break;
	endswitch;
}


function getLogout(){
	$sql =Enviroment::where('created_by',Auth::user()->id)->first();
    Auth::logout();
    return Redirect::to('/');
  }

		 
}