<?php

class AroController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backend
	 *
	 * @return Response
	 */
    public function index()
	{
		return View::make('aro.index');
       
	}
    public function getM16()
	{
		$year = (date('Y')+543) - 17;
		$now_year =(date('Y')+543);
		$count_thai_man = DB::table('tb_thai_man')
		->where(
			array(
				'district_id'=>Auth::user()->district_id,
				'thai_birth'=>$year
				)
		)->count();
		if($count_thai_man>0)
		{
			$d = DB::table('tb_thai_man')
			->select('id','thai_birth','thai_man')
			->where(
				array(
					'district_id'=>Auth::user()->district_id,
					'thai_birth'=>$year
					)
			)->first();
			$thai_number = $d->id;
			$thai_birth = $d->thai_birth;
			$thai_man = $d->thai_man;
			
			
		}else{
			$thai_number=0;
			$thai_birth='';
			$thai_man=0;
		}
		$data_m16 = M16::where(array(
			'district_id'=>Auth::user()->district_id
			))->orderBy('month_add','asc')
			->orderBy('year_add','desc')
			->get();

			$count_total = M16::where(array(
				'district_id'=>Auth::user()->district_id,
				'year_add' =>$year
	
			))->count();
			if($count_total==0)
			{
				$add_total = Input::get('thai_number')-Input::get('add_number');
			}else{
				$data_total = M16::select('add_total')->where(array(
					'district_id'=>Auth::user()->district_id,
					'year_add' =>$year
				))->take(1)->skip(0)
				->orderBy('id','desc')->first();
				$add_total = $data_total->add_total - Input::get('add_number');
			}
			  

		 
		return View::make('aro.m16')->with(array(
			'count_thai_man' => $count_thai_man,
			'thai_year'=>$year,
			'now_year'=>$now_year,
			'thai_number'=>$thai_number,
			'thai_birth'=>$thai_birth,
			'thai_man'=>$thai_man,
			'data_m16'=>$data_m16,
			'add_total' =>$add_total

		));
	}
	public function postAddm16()
	{
		$year = (date('Y')+543) - 17;
		 
		if(Input::get('thai_number')==0)
		{
			$t = new Thaiman;
			$t->thai_man =Input::get('thai_man');
			$t->thai_birth=Input::get('thai_birth');
			$t->army_id		= Auth::user()->army_id;
			$t->mtb_id	= Auth::user()->mtb_id;
   
			$t->prov_id			= Auth::user()->prov_id;
			$t->district_id		= Auth::user()->district_id;
			$t->save();

			$d = DB::table('tb_thai_man')
			->select('id','thai_birth')
			->where(
				array(
					'district_id'=>Auth::user()->district_id,
					'thai_birth'=>Input::get('thai_birth')
					)
			)->first();
			$thai_number = $d->id;
			$thai_birth = $d->thai_birth;
			
			 
			
		}else{
			$thai_number = Input::get('thai_number');
			$thai_birth = Input::get('thai_birth');
		}


		 $c = new M16;
		 $c->thai_man_id = $thai_number;
		 $c->add_number			= Input::get('add_number');
		 $c->add_another		= Input::get('add_another');
		 $c->add_total		= $add_total;
		 $c->add_from_another			= Input::get('add_from_another');
		 $c->army_id		= Auth::user()->army_id;
		 $c->mtb_id	= Auth::user()->mtb_id;

		 $c->prov_id			= Auth::user()->prov_id;
		 $c->district_id		= Auth::user()->district_id;
		 $c->month_add			= Input::get('month_add');
		 $c->year_add			= $thai_birth;

		 if($c->save())
		 {
		 $response = array( 'status' => 1, 'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"  ); 
		  }else{
		 $response = array( 'status' => 0, 'message' => 'ไม่สามารถบันทึกข้อมูลได้'); 
		 }
	 return json_encode($response);
	}


	function getLogout(){
		$sql =Enviroment::where('created_by',Auth::user()->id)->first();
		Auth::logout();
		return Redirect::to('/aro-login');
	  }
	
}