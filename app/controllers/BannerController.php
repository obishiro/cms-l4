<?php

class BannerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /banner
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = Lang::get('msg.config-slide',array(),'th');
		$api = URL::to('backend/data/banner');
		return View::make('backend.banner.mainbanner')->with(
			array(
			'title' 	=>$title,
			'api'	=> $api,
			'status'	=> 'null'
			));

		//return '=>'.Auth::user()->user_status;
	}
	public function getAdd()
	{
		 		$title = Lang::get('msg.config-slide',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$tag = Tag::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.banner.addbanner')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
				 	'status'	=> 'null'
				       ));
	}
	public function getEdit($id)
	{
		 		$title = Lang::get('msg.config-slide',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$tag = Tag::orderBy('id','desc')->get();
			//	$tagbanner = Tagbanner::where('banner_id',$id)->get();
				$c = Banner::find($id);
				$banner_file = Uploadfilesbanner::where('token',$c->banner_file)->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.banner.editbanner')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
				 	'status'	=> 'null',
				 	'banner_file' => $banner_file,
				 	'c'			=> $c
				       ));
	}
	public function postAdd()
	{
		 
	 
	 	$token = Input::get('key');
	 	$url = Helpers::create_url(Input::get('txt_name'));
	 	
		 $c = new Banner;
		 $c->banner_name 			= Input::get('txt_name');
	  	 $c->banner_file			= $token;
		 $c->banner_show			= Input::get('txt_show');
		 $c->banner_detail			= Input::get('txt_detail');
		 $c->banner_all_detail		= Input::get('txt_all_detail');

		 $c->banner_url				= $url;
		 $c->created_at			= date('Y-m-d H:i:s');
		 $c->updated_at			= date('Y-m-d H:i:s');
		 $c->created_by			= Auth::user()->id;
	 
 

		 $c->save();
 
		 
      // $filenum = Uploadfilesbanner::where('token',$token )->count();
      // if($filenum > 0){
      // }else{
      // 	$file_type = '0';
		  		// 	$Images 	= new Uploadfiles;
    		 
    		// 	$Images 	->files_newname 		="";
    		// 	$Images 	->files_oldname			="";
    		// 	$Images 	->files_type			=$file_type;
    		// 	$Images 	->token 				= Input::get('key');
    		// 	$Images 	->save();
      // }
      
	return Redirect::to('backend/banner')->with(
				array(
					'save-success' => 'save'
				       ));

	}

	public function postEdit()
	{
		 
	 
		 $id = Input::get('id');
	  	 
		 $c = Banner::find($id);
		 $c->banner_name 			= Input::get('txt_name');
	 
 		$c->banner_show			= Input::get('txt_show');
 		$c->banner_detail			= Input::get('txt_detail');
		 $c->banner_all_detail		= Input::get('txt_all_detail');
		// $c->banner_url				= $url;
		// $c->banner_tag			= '';
		// $c->created_at			= date('Y-m-d H:i:s');
		 $c->updated_at			= date('Y-m-d H:i:s');
		 $c->created_by			= Auth::user()->id;
	 
		 $c->save();
		 
 ;
 
   
      return Redirect::to('backend/banner')->with(
				array(
					'edit-success' => 'edit'
				       ));

	}


	public function postAddfile($key)
	{

		  		
		  		if (Input::hasFile('uploadfile')){
		  			$Path='uploadfiles/banner'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 
		  			
		  			Image::make(Input::file('uploadfile'))->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/banner'; 
					// $upload_success=	Input::file('uploadfile')->move($Path,$img_name);


	    			$Images 	= new Uploadfilesbanner;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
 
		  			 
		  		}
 

	}
	public function postEditfile($key)
	{

		  		$oldfile = Uploadfilesbanner::where('token',$key)->first();
		  		$Path='uploadfiles/banner'; 
		  		$Path_thumb = $Path.'/thumb';
		  		if (Input::hasFile('uploadfile')){

		  	 
		  		
		  			
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 $imagesize=getimagesize(Input::file('uploadfile'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
		  			
		  			Image::make(Input::file('uploadfile'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/banner'; 
    			 		$Images = new Uploadfilesbanner;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
    			 }

	}

	public function postDropimages($key){

			 $img = Uploadfilesbanner::where('files_oldname','=',$key)->first();
		 	 $filename='uploadfiles/banner/'.$img->files_newname; 
		 	 $filename_thumb='uploadfiles/banner/thumb/'.$img->files_thumb; 
			 File::delete($filename);
			 File::delete($filename_thumb);
			 $img = Uploadfilesbanner::where(
			 	array(
			 	//	'token'=>$key,
			 		'files_newname'=>$img->files_newname
			 		))->delete();
			 
		}
		public function postDeleteeditbanner()
		{
			//$banner_file = Uploadfilesbanner::where('token',$c->banner_file)->get();
		//	 var_dump($key);
		
			$del_img =Input::get('del_img');
			foreach ($del_img as $del =>$d)
			{
				$filename='uploadfiles/banner/'.$d;
		 	 	$filename_thumb='uploadfiles/banner/thumb/'.$d; 
			 	File::delete($filename);
			 	File::delete($filename_thumb);
				Uploadfilesbanner::where(array('files_newname'=>$d))->delete();

			}
			return 'ok';
		}

		public function getDataeditbanner($key)
		{
			$banner = Uploadfilesbanner::where(array('token'=>$key))->get();
			return View::make('backend.banner.dataeditbanner')->with(array(
				'banner_file' => $banner
				));
		}
	}