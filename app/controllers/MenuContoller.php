<?php

class MenuContoller extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /menucontoller
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
 

	public function getMenu($type)
	{
		switch ($type):
			case 'mainmenu':
			$title = Lang::get('msg.mainmenu',array(),'th');
			$sql = Mainmenu::orderBy('id','desc')->get();
			 
			$api = URL::to('backend/data/mainmenu');
			return View::make('backend.menu.mainmenu')->with(
				 array(
				 	'title' 	=>$title,
				 
				 	'api'		=> $api,
				 	'status'	=> 'null'
				       ));

			break;
			case 'submenu':
			$title = Lang::get('msg.submenu',array(),'th');
			$api = URL::to('backend/data/submenu');
			return View::make('backend.menu.submenu')->with(
				 array(
				 	'title' 	=>$title,
				  	'api'	=> $api,
				 	'status'	=> 'null'
				       ));

				break;
		endswitch;
	}

	public function getAdd($type)
	{
		switch($type):
			case 'mainmenu':
				$title = Lang::get('msg.mainmenu',array(),'th');
				$sql = Mainmenu::where('create_by',Auth::user()->id)->orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.menu.addmainmenu')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				 	 'sql'		=> $sql,
				 	'status'	=> 'null'
				       ));
			break;
			case 'submenu':
				$title = Lang::get('msg.submenu',array(),'th');
				 $sql = Mainmenu::where(array('mainmenu_type'=>'3','create_by'=>Auth::user()->id))->orderBy('id','desc')->get();
			 
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.menu.addsubmenu')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'	=>$rules,
				  	'sql'	=> $sql,
				 	'status'	=> 'null'
				       ));
			break;
			case 'poll':
				$title = Lang::get('msg.config-poll',array(),'th');
				 $sql = Mainpoll::orderBy('id','desc')->get();
			 
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.config.addpoll')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'	=>$rules,
				  	'sql'	=> $sql,
				 	'status'	=> 'null'
				       ));
			break;


		endswitch;
	}
	public function postAdd($type)
	{
		switch($type):
			case 'mainmenu':
				$max = Mainmenu::where('mainmenu_position',Input::get('txt_position'))->max('mainmenu_sorting');
				$maxsorting = $max + 1;
				$url = Helpers::create_url(Input::get('txt_name'));
				 $m = new Mainmenu;
				 $m->mainmenu_name = Input::get('txt_name') ;
				 $m->mainmenu_type = Input::get('txt_type');
				 $m->mainmenu_detail = Input::get('txt_detail');
				 $m->mainmenu_url = Input::get('txt_url');
				 $m->mainmenu_embed = Input::get('txt_embed');
				 $m->m_url 			= $url;
				 $m->mainmenu_position = Input::get('txt_position');
				 $m->mainmenu_sorting = $maxsorting;
				 $m->mainmenu_show			= Input::get('txt_show');
		 		 $m->mainmenu_showhome		= Input::get('txt_showhome');
				 $m->parent_id = Input::get('parent_id');
				 $m->created_at = date('Y-m-d H:i:s');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->create_by = Auth::user()->id;
				 $m->save();
				return Redirect::to('backend/menu/add/mainmenu')->with(
				array(
					'save-success' => 'save'
				       ));
			break;
			case 'submenu':
		//	$name = Input::file('picture')->getClientOriginalName();
			 
			if (Input::hasFile('picture')){
 				  	
				$Path='uploadfiles/picmenu'; 
				$Path_thumb = $Path.'/thumb';
				
				$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
				//return $img_name;
				 // $height= Image::make(Input::file('picture'))->height();
				 // $width= Image::make(Input::file('picture'))->width();
				 
				
				// Image::make(Input::file('picture'))->resize(300, 250)->save($Path.'/'.$img_name);
				Input::file('picture')->move($Path, $img_name); 
				// Image::make(Input::file('picture'))->resize(150,100)->save($Path_thumb.'/'.$img_name);
				//  $filename = Input::file('picture')->getClientOriginalName();
			 }else{
				 $img_name='';
			 }
			 
				$max = Submenu::where('submenu_categories',Input::get('txt_categories'))->max('submenu_sorting');
				$maxsorting = $max + 1;
				$url = Helpers::create_url(Input::get('txt_name'));
				 $m = new Submenu;
				 $m->submenu_name = Input::get('txt_name') ;
				 $m->submenu_categories = Input::get('txt_categories');
				 $m->submenu_type = Input::get('txt_type');
				 $m->submenu_detail = Input::get('txt_detail');
				 $m->submenu_url = Input::get('txt_url');
				 $m->s_url = $url;
				 $m->submenu_show	= Input::get('txt_show');
				 $m->submenu_sorting	= $maxsorting;
				 $m->submenu_pic = $img_name;
				 $m->submenu_picshow	= Input::get('txt_showpic');
				 $m->parent_id = Input::get('parent_id');
				 $m->created_at = date('Y-m-d H:i:s');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->create_by = Auth::user()->id;
				$m->save();
				return Redirect::to('backend/menu/add/submenu')->with(
				array(
					'save-success' => 'save'
				       ));
			break;
			case 'poll':
				 
				for($i=0;$i<count(array_filter(Input::get('txt_name')));$i++){
				$url = Helpers::create_url(Input::get('txt_name')[$i]);
				 $m = new Subpoll;
				 $m->subpoll_name = Input::get('txt_name')[$i] ;
				 $m->subpoll_categories = Input::get('txt_categories');
				  
				 
				 $m->s_url = $url;
				 $m->subpoll_show	= Input::get('txt_show');
				 
				 $m->created_at = date('Y-m-d H:i:s');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->create_by = Auth::user()->id;
				 $m->save();
				}



				return Redirect::to('backend/menu/add/poll')->with(
				array(
					'save-success' => 'save'
				       ));
			//return array_count_values(Input::get('txt_name'));
			break;

		endswitch;
	}
	public function getEdit($type,$id)
	{
		switch($type):
			case 'mainmenu': 
				$title = Lang::get('msg.msg_edit',array(),'th').' '.Lang::get('msg.mainmenu',array(),'th');
				$m = Mainmenu::find($id);
				$sql = Mainmenu::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				return View::make('backend.menu.editmainmenu')->with(
					array(
					'title' 	=>$title,
                     'm' => $m,
                     'rules' => $rules,
                     'sql'=>$sql,
                     'id' => $id
					));
			break;

			case 'submenu': 
				$title = Lang::get('msg.msg_edit',array(),'th').' '.Lang::get('msg.submenu',array(),'th');
				$m = Submenu::find($id);
				$sql = Mainmenu::where('mainmenu_type','3')->orderBy('id','desc')->get();
				$sql_submenu = Submenu::where('submenu_categories','=',$m->submenu_categories)->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				return View::make('backend.menu.editsubmenu')->with(
					array(
					'title' 	=>$title,
                                                    'm' 	=> $m,
                                                    'sql'	=> $sql,
                                                    'rules' 	=> $rules,
                                                    'sql_submenu'=>$sql_submenu
					));
			break;
			case 'subpoll': 
				$title = Lang::get('msg.msg_edit',array(),'th').' '.Lang::get('msg.config-poll',array(),'th');
				$m = Subpoll::find($id);
				$sql = Mainpoll::orderBy('id','desc')->get();
			 
				$rules = ['txt_name'=>'required'];
				return View::make('backend.config.editpoll')->with(
					array(
					'title' 	=>$title,
                                                    'm' 	=> $m,
                                                    'sql'	=> $sql,
                                                    'rules' 	=> $rules
					));
			break;


		endswitch;	
	}

	public function postEdit($type)
	{
		   $id = Input::get('id');
		 switch($type):
			case 'mainmenu': 
				$m = Mainmenu::find($id);
				$url = Helpers::create_url(Input::get('txt_name'));
				$m->mainmenu_name = Input::get('txt_name') ;
				 $m->mainmenu_type = Input::get('txt_type');
				 $m->mainmenu_detail = Input::get('txt_detail');
				 $m->mainmenu_url = Input::get('txt_url');
				  $m->mainmenu_embed = Input::get('txt_embed');
				 $m->mainmenu_position = Input::get('txt_position');
				 $m->mainmenu_show			= Input::get('txt_show');
		 		 $m->mainmenu_showhome		= Input::get('txt_showhome');
				 $m->parent_id = Input::get('parent_id');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->m_url = $url;
				 $m->save();
				 return Redirect::to('backend/menu/mainmenu')->with(
				array(
					'edit-success' => 'edit'
				       ));
			break;
			case 'submenu': 
			if (Input::hasFile('picture')){
 				  	
				$Path='uploadfiles/picmenu'; 
				$Path_thumb = $Path.'/thumb';
				
				$img_name = Str::random(16,'numberic').".".Input::file('picture')->getClientOriginalExtension();
				//return $img_name;
				 // $height= Image::make(Input::file('picture'))->height();
				 // $width= Image::make(Input::file('picture'))->width();
				 
				
				// Image::make(Input::file('picture'))->resize(300, 250)->save($Path.'/'.$img_name);
				Input::file('picture')->move($Path, $img_name); 
				@unlink($Path.'/'.Input::get('oldpic'));
				// Image::make(Input::file('picture'))->resize(150,100)->save($Path_thumb.'/'.$img_name);
				//  $filename = Input::file('picture')->getClientOriginalName();
			 }else{
				 $img_name=Input::get('oldpic');;
			 }
				$m = Submenu::find($id);
				$url = Helpers::create_url(Input::get('txt_name'));
				$m->submenu_name = Input::get('txt_name') ;
				 $m->submenu_categories = Input::get('txt_categories');
				 $m->submenu_type = Input::get('txt_type');
				 $m->submenu_detail = Input::get('txt_detail');
				 $m->submenu_url = Input::get('txt_url');
				 $m->submenu_pic = $img_name;
				 $m->submenu_picshow	= Input::get('txt_showpic');
				 $m->submenu_show	= Input::get('txt_show');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->s_url = $url;
				 $m->save();
				 return Redirect::to('backend/menu/submenu')->with(
				array(
					'edit-success' => 'edit'
				       ));
			break;
			case 'poll': 
				$m = Subpoll::find($id);
				$url = Helpers::create_url(Input::get('txt_name'));
				$m->subpoll_name = Input::get('txt_name') ;
				 $m->subpoll_categories = Input::get('txt_categories');
				 
				 $m->subpoll_show	= Input::get('txt_show');
				 $m->updated_at = date('Y-m-d H:i:s');
				 $m->s_url = $url;
				 $m->save();
				 return Redirect::to('backend/config/poll')->with(
				array(
					'edit-success' => 'edit'
				       ));
			break;
		 endswitch;
	}
	 

	

}