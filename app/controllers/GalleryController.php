<?php

class GalleryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /gallery
	 *
	 * @return Response
	 */
	public function index()
	{
		$title = Lang::get('msg.gallery',array(),'th');
		$api = URL::to('backend/data/gallery');
		return View::make('backend.gallery.maingallery')->with(
			array(
			'title' 	=>$title,
			'api'	=> $api,
			'status'	=> 'null'
			));

		//return '=>'.Auth::user()->user_status;
	}
	public function getAdd()
	{
		 		$title = Lang::get('msg.gallery',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$tag = Tag::orderBy('id','desc')->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.gallery.addgallery')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
				 	'status'	=> 'null'
				       ));
	}
	public function getEdit($id)
	{
		 		$title = Lang::get('msg.gallery',array(),'th');
				$sql = Categories::orderBy('id','desc')->get();
				$tag = Tag::orderBy('id','desc')->get();
			//	$taggallery = Taggallery::where('gallery_id',$id)->get();
				$c = Gallery::find($id);
				$gallery_file = Uploadfilesgallery::where('token',$c->gallery_file)->get();
				$rules = ['txt_name'=>'required','txt_type'=>'required'];
				 
				return View::make('backend.gallery.editgallery')->with(
				 array(
				 	'title' 	=>$title,
				 	'rules'		=>$rules,
				  	'sql'		=> $sql,
				  	'tag'		=> $tag,
				 	'status'	=> 'null',
				 	'gallery_file' => $gallery_file,
				 	'c'			=> $c
				       ));
	}
	public function postAdd()
	{
		$url = Helpers::create_url(Input::get('txt_name'));
	 
	 	$token = Input::get('key');
	 	
	 	
		 $c = new Gallery;
		 $c->gallery_name 			= Input::get('txt_name');
	 
		 $c->gallery_detail			= Input::get('txt_detail');
		 $c->gallery_file			= $token;
		 $c->gallery_view			= '0';
		 $c->gallery_show			= '1';

		// $c->gallery_tag			= '';
		 $c->created_at			= date('Y-m-d H:i:s');
		 $c->updated_at			= date('Y-m-d H:i:s');
		 $c->created_by			= Auth::user()->id;
		 $c->gallery_url			= $url;
 

		 $c->save();
 
		 
      // $filenum = Uploadfilesgallery::where('token',$token )->count();
      // if($filenum > 0){
      // }else{
      // 	$file_type = '0';
		  		// 	$Images 	= new Uploadfiles;
    		 
    		// 	$Images 	->files_newname 		="";
    		// 	$Images 	->files_oldname			="";
    		// 	$Images 	->files_type			=$file_type;
    		// 	$Images 	->token 				= Input::get('key');
    		// 	$Images 	->save();
      // }
      
	return Redirect::to('backend/gallery')->with(
				array(
					'save-success' => 'save'
				       ));

	}

	public function postEdit()
	{
		$url = Helpers::create_url(Input::get('txt_name'));
	 
		 $id = Input::get('id');
	  	 
		 $c = Gallery::find($id);
		 $c->gallery_name 			= Input::get('txt_name');
	 
		 $c->gallery_detail			= Input::get('txt_detail');
		// $c->gallery_file			= $token;
		 
		// $c->gallery_tag			= '';
		// $c->created_at			= date('Y-m-d H:i:s');
		 $c->updated_at			= date('Y-m-d H:i:s');
		 $c->created_by			= Auth::user()->id;
		 $c->gallery_url			= $url;
		 
		 $c->save();
		 
 ;
 
   
      return Redirect::to('backend/gallery')->with(
				array(
					'edit-success' => 'edit'
				       ));

	}


	public function postAddfile($key)
	{

		  		
		  		if (Input::hasFile('uploadfile')){
		  			$Path='uploadfiles/gallery'; 
		  			$Path_thumb = $Path.'/thumb';
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 $imagesize=getimagesize(Input::file('uploadfile'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
		  			
		  			Image::make(Input::file('uploadfile'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/gallery'; 
					// $upload_success=	Input::file('uploadfile')->move($Path,$img_name);


	     



	    			$Images 	= new Uploadfilesgallery;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
 
		  			 
		  		}
 

	}
	public function postEditfile($key)
	{

		  		$oldfile = Uploadfilesgallery::where('token',$key)->first();
		  		$Path='uploadfiles/gallery'; 
		  		$Path_thumb = $Path.'/thumb';
		  		if (Input::hasFile('uploadfile')){

		  	 
		  		
		  			
		  			$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();
		  			//return $img_name;
		  			 // $height= Image::make(Input::file('uploadfile'))->height();
		  			 // $width= Image::make(Input::file('uploadfile'))->width();
		  			 $imagesize=getimagesize(Input::file('uploadfile'));
		  			if($imagesize[0] >'1024'){
		  				$width ='1024';
		  			}else{
		  				$width = $imagesize[0];
		  			}
		  			 
		  			 $height=round($width*$imagesize[1]/$imagesize[0]);
		  			
		  			Image::make(Input::file('uploadfile'))->resize($width, $height)->save($Path.'/'.$img_name);
		  			 
		  			Image::make(Input::file('uploadfile'))->resize(384, 240)->save($Path_thumb.'/'.$img_name);
	  			 	$filename = Input::file('uploadfile')->getClientOriginalName();
	  			 	 $file= Input::file('uploadfile');
	  		 		$files_size= File::size($file);
		  	// 		$img_name = Str::random(16,'numberic').".".Input::file('uploadfile')->getClientOriginalExtension();

		  	 		$file_type = Input::file('uploadfile')->getClientOriginalExtension();
					// $Path='uploadfiles/gallery'; 
    			 		$Images = new Uploadfilesgallery;
	    		 
	    			$Images 	->files_newname 		=$img_name;
	    			$Images 	->files_oldname			=$filename;
	    			$Images 	->files_type			=$file_type;
	    			$Images 	->files_thumb			=$img_name;
	    			$Images		->files_size			=$files_size;
	    			$Images 	->token 				=$key;
	    			$Images 	->save();
    			 }

	}

	public function postDropimages($key){

			 $img = Uploadfilesgallery::where('files_oldname','=',$key)->first();
		 	 $filename='uploadfiles/gallery/'.$img->files_newname; 
		 	 $filename_thumb='uploadfiles/gallery/thumb/'.$img->files_thumb; 
			 File::delete($filename);
			 File::delete($filename_thumb);
			 $img = Uploadfilesgallery::where(
			 	array(
			 	//	'token'=>$key,
			 		'files_newname'=>$img->files_newname
			 		))->delete();
			 
		}
		public function postDeleteeditgallery()
		{
			//$gallery_file = Uploadfilesgallery::where('token',$c->gallery_file)->get();
		//	 var_dump($key);
			$del_img =Input::get('del_img');
			foreach ($del_img as $del =>$d)
			{
				$filename='uploadfiles/gallery/'.$d;
		 	 	$filename_thumb='uploadfiles/gallery/thumb/'.$d; 
			 	File::delete($filename);
			 	File::delete($filename_thumb);
				Uploadfilesgallery::where(array('files_newname'=>$d))->delete();

			}
			return 'ok';
		}

		public function getDataeditgallery($key)
		{
			$gallery = Uploadfilesgallery::where(array('token'=>$key))->get();
			return View::make('backend.gallery.dataeditgallery')->with(array(
				'gallery_file' => $gallery
				));
		}
	}