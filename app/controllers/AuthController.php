<?php

class AuthController extends \BaseController {

	public function getLoginfacebook() {

    // get data from input
    $code = Input::get( 'code' );

    // get fb service
    $fb = OAuth::consumer( 'Facebook' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from facebook, get the token
        $token = $fb->requestAccessToken( $code );

        // Send a request with it
        $result = json_decode( $fb->request( '/me' ), true );

        // $message = 'Your unique facebook user id is: ' . $result['gender'] . ' and your name is ' . $result['name'];
        // echo $message. "<br/>";

        //Var_dump
        //display whole array().
       var_dump($result);

    }
    // if not ask for permission first
    else {
        // get fb authorization
        $url = $fb->getAuthorizationUri();

        // return to facebook login url
         return Redirect::to( (string)$url );
    }

}
}