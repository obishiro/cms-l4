<?php
class Helpers {
	static function Cmonth($var)
{
	switch ($var) {
		case '01':
			return 'มกราคม';
		break;
		case '02':
			return 'กุมภาพันธ์';
		break;
		case '03':
			return 'มีนาคม';
		break;
		case '04':
			return 'เมษายน';
		break;
		case '05':
			return 'พฤษภาคม';
		break;
		case '06':
			return 'มิถุนายน';
		break;
		case '07':
			return 'กรกฎาคม';
		break;
		case '08':
			return 'สิงหาคม';
		break;
		case '09':
			return 'กันยายน';
		break;
		case '10':
			return 'ตุลาคม';
		break;
		case '11':
			return 'พฤศจิกายน';
		break;
		case '12':
			return 'ธันวาคม';
		break;
		 
}

}
	 static function GetMonth()
	{
		echo "<option value=\"0\">- เลือกเดือน -</option>";
		for($i=1;$i<=12;$i++)
		{
			echo '<option value='.$i.'>'.Helpers::Cmonth($i).' </option>';
		}
	}
	static function mainmenutype($type){
		switch ($type) {
			case '1':
				return Lang::get('msg.msg_content', array(), 'th');
				break;
			
			case '2':
				return Lang::get('msg.msg_url', array(), 'th');
				break;
			case '3':
				return Lang::get('msg.msg_submenu', array(), 'th');
				break;
			case '4':
				return Lang::get('msg.msg_embed', array(), 'th');
				break;
			 
		}
	}
	static function maindeparttype($type){
		switch ($type) {
			case '1':
				return Lang::get('msg.msg_content', array(), 'th');
				break;
			
			case '2':
				return Lang::get('msg.msg_url', array(), 'th');
				break;
			case '3':
				return Lang::get('msg.msg_subdepart', array(), 'th');
				break;
			case '4':
				return Lang::get('msg.msg_embed', array(), 'th');
				break;
			 
		}
	}
		static function usertype($type){
		switch ($type) {
			case '1':
				return 'ผู้ดูแลระบบ';
				break;
			
			case '2':
				return 'ผู้ใช้งานทั่วไป';
				break;
			 
			 
		}
	}
	static function mainmenuposition($position){
		switch ($position) {
			case '1':
				return Lang::get('msg.msg_position_top', array(), 'th');
				break;
			
			case '2':
				return Lang::get('msg.msg_position_right', array(), 'th');
				break;
			case '3':
				return Lang::get('msg.msg_position_left', array(), 'th');
				break;
			 
		}
	}
	public static function create_url($string){
	$con = array(' ','%','/','&','*','#','+');
	$rp = array('-','-','-','-','-','-','-');
  	$slug=str_replace($con, $rp, $string);
  	
   return $slug;
}
	public static function filestype($type) {
		$path = URL::to('img');
		if($type =="MP4" || $type =="mp4" ||$type =="WMV" ||$type =="wmv" 
			||$type =="AVI" ||$type =="avi" ||$type =="MKV" ||$type =="mkv" ||$type =="MOV" ||$type =="mov"  )
			 {
			 	return "<img src=\"$path/vdo.png\" border=\"0\"> ";
		}else if ($type=="PDF" || $type =="pdf"){
				return "<img src=\"$path/pdf.png\" border=\"0\"> ";
		}else if ($type=="DOC" || $type =="doc" || $type =="docx"|| $type =="DOCX"){
				return "<img src=\"$path/docx.png\" border=\"0\"> ";
		}else if ($type=="XLS" || $type =="xls" || $type =="xlsx"|| $type =="XLSX"){
				return "<img src=\"$path/xls.png\" border=\"0\"> ";
		}else if ($type=="PPT" || $type =="ppt" || $type =="pptx"|| $type =="PPTX"){
				return "<img src=\"$path/pptx.png\" border=\"0\"> ";
		}else if ($type=="JPG" || $type =="jpg" || $type =="PNG"|| $type =="png"){
				return "<img src=\"$path/jpeg.png\" border=\"0\"> ";
		}else if($type=="0"){
				return "<img src=\"$path/none.png\" border=\"0\"> ";
		}
	}

	public static function filestype_l($type) {
		$path = URL::to('img');
		if($type =="MP4" || $type =="mp4" ||$type =="WMV" ||$type =="wmv" 
			||$type =="AVI" ||$type =="avi" ||$type =="MKV" ||$type =="mkv" ||$type =="MOV" ||$type =="mov"  )
			 {
			 	return "<img src=\"$path/mkv-icon.png\" border=\"0\" style=\"height:155px\" class=\"img-responsive\"> ";
		}else if ($type=="PDF" || $type =="pdf"){
				return "<img src=\"$path/pdf-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}else if ($type=="DOC" || $type =="doc" || $type =="docx"|| $type =="DOCX"){
				return "<img src=\"$path/docx-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}else if ($type=="XLS" || $type =="xls" || $type =="xlsx"|| $type =="XLSX"){
				return "<img src=\"$path/xls-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\" > ";
		}else if ($type=="PPT" || $type =="ppt" || $type =="pptx"|| $type =="PPTX"){
				return "<img src=\"$path/pptx-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}else if ($type=="JPG" || $type =="jpg" || $type =="PNG"|| $type =="png"){
				return "<img src=\"$path/jpeg-icon.png\" border=\"0\"> ";
		}else if($type=="0"){
				return "<img src=\"$path/none-icon.png\" border=\"0\" style=\"height:155px\"  class=\"img-responsive\"> ";
		}
	}

	public static function create_tag()
	{
		$tagMax = Tag::max('tag_count');
		
		$tag = Tag::all();
		foreach($tag as $tags => $t){
			if($tagMax>0) {
			$percent = floor(($t['tag_count'] / $tagMax) * 100);
			}else{
				$percent = 0;
			}
			$url = URL::to('tag',array($t->id,$t->tag_url));
		//echo  $t->tag_name;
			if ($percent < 20): 
			   $class = 'smallest'; 
			 elseif ($percent >= 20 and $percent < 40):
			   $class = 'small'; 
			 elseif ($percent >= 40 and $percent < 60):
			   $class = 'medium';
			 elseif ($percent >= 60 and $percent < 80):
			   $class = 'large';
			 else:
			 $class = 'largest';
			 endif;

			 echo "<span class=\"$class\"><a href=\"$url\"><i class=\"fa fa-tag\"></i>".$t->tag_name."</a></span>";

		}

	}
	public static function DateFormat($strDate)
{
	// $time=strtotime($date);
	// return date("d M Y",$time);
	$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
	 
}
public static function DateDiff($strDate1)
{
			$strDate2 = date('Y-m-d H:i:s');
			   $cal = (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24);  // 1 day = 60*60*24
			   return intval($cal);

}


public static function get_menu($data,$id=null)
{
 
 foreach ($data as $key => $v) {

 
 	if($v->parent_id =="0") {
 			if($id !=null){
 				if($v->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 		 	echo "<option value=\"$v->id\" $select>";
 			echo "".$v->categories_name;
 
 	   $num = Categories::where('parent_id',$v->id)->count();
 	  	 if($num !='0') {
 	  		 	$sub1 = Categories::where('parent_id',$v->id)->get();
 	  		 	foreach($sub1 as $sub => $s1) {
 	  		 		if($id !=null){
 				if($s1->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 	  		 		 echo "<option value=\"$s1->id\" $select>";
 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp; - ".$s1->categories_name;

 	  		 		 $num2 = Categories::where('parent_id',$s1->id)->count();
 	  		 		 	if($num2 !='0') {
				 	  		 	$sub2 = Categories::where('parent_id',$s1->id)->get();
				 	  		 	foreach($sub2 as $subs2 => $s2) {
				 	  		 		if($id !=null){
					 				if($s2->id == $id) {
					 					$select = "selected";
					 				}else {
					 					$select ="";
					 				}
					 			}else{
					 				$select = "";
					 			}
				 	  		 		 echo "<option value=\"$s2->id\" $select>";
				 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -- ".$s2->categories_name;

				 	  		 		
				 	  		 			// $num3 = Categories::where('parent_id',$s2->id)->count();
				 	  		 		 // 	if($num3 !='0') {
								 	  		//  	$sub3 = Categories::where('parent_id',$s2->id)->get();
								 	  		//  	foreach($sub3 as $subs3 => $s3) {
								 	  		//  		 echo "<option>";
								 	  		//  		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; > ".$s3->categories_name;

								 	  		 		  
								 	  		//  	}
								 	  	 // }  
				 	  		 	}
				 	  	 }
 	  		 	}
 	  	 }


 	 
 		echo "</option>";
 }

}

 
}
	public static function get_parent($c){


		$c1 = DB::table('tb_categories')->where('id', $c)->first();
		 
		$p1 = $c1->parent_id;
		if($p1=="0") {
			return $c1->id;
		 }else if ($c1->parent_id !="0") {
		 	$c2 = DB::table('tb_categories')->where('id',$c1->parent_id)->first();
		 	 	if($c2->parent_id=="0") {
		 	 		return $c2->id;
		 	 	}else {
		 	 		$c3 = DB::table('tb_categories')->where('id',$c2->parent_id)->first();
		 	 		return $c3->id;
		 	 	}
		 }
	//	return $c;


	}

 	public static function update_parent() 
 	{


		$getzero = Content::where(array('parent_id'=>null,'parent_id'=>0))->get();
		foreach ($getzero as $key => $valz) {
		//$p = Helpers::getParent($valz->content_categories);
			// echo $p;
			//echo $v->content_categories;
		}
		 

 	}

 	public static function get_mainmenu($data,$id=null)
{
 
 foreach ($data as $key => $v) {

 
 	if($v->parent_id =="0") {
 			if($id !=null){
 				if($v->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 		 	echo "<option value=\"$v->id\" $select>";
 			echo "".$v->mainmenu_name;
 
 	   $num = Mainmenu::where('parent_id',$v->id)->count();
 	  	 if($num !='0') {
 	  		 	$sub1 = Mainmenu::where('parent_id',$v->id)->get();
 	  		 	foreach($sub1 as $sub => $s1) {
 	  		 		if($id !=null){
 				if($s1->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 	  		 		 echo "<option value=\"$s1->id\" $select>";
 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp; - ".$s1->mainmenu_name;

 	  		 		 $num2 = Mainmenu::where('parent_id',$s1->id)->count();
 	  		 		 	if($num2 !='0') {
				 	  		 	$sub2 = Mainmenu::where('parent_id',$s1->id)->get();
				 	  		 	foreach($sub2 as $subs2 => $s2) {
				 	  		 		if($id !=null){
					 				if($s2->id == $id) {
					 					$select = "selected";
					 				}else {
					 					$select ="";
					 				}
					 			}else{
					 				$select = "";
					 			}
				 	  		 		 echo "<option value=\"$s2->id\" $select>";
				 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -- ".$s2->mainmenu_name;

				 	  		 		
				 	  		 			// $num3 = Categories::where('parent_id',$s2->id)->count();
				 	  		 		 // 	if($num3 !='0') {
								 	  		//  	$sub3 = Categories::where('parent_id',$s2->id)->get();
								 	  		//  	foreach($sub3 as $subs3 => $s3) {
								 	  		//  		 echo "<option>";
								 	  		//  		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; > ".$s3->mainmenu_name;

								 	  		 		  
								 	  		//  	}
								 	  	 // }  
				 	  		 	}
				 	  	 }
 	  		 	}
 	  	 }


 	 
 		echo "</option>";
 }

}

 
}
public static function Sorting($type,$id)
{
	switch($type):
		case 'categories':
			$s = Categories::find($id);
			if($s->sort=="1"){
				echo "<a href=\"../sortdown/categories/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}else{
				echo "<a href=\"../sortdown/categories/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
				echo "&nbsp;&nbsp;";
				echo "<a href=\"../sorttop/categories/$id\" class=\"text-success\"> ";
				echo "<i class=\"fa fa-arrow-circle-up fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}
		break;
		case 'mainmenu':
			$s = Mainmenu::find($id);
			if($s->mainmenu_sorting=="1"){
				echo "<a href=\"../sortdown/mainmenu/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}else{
				echo "<a href=\"../sortdown/mainmenu/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
				echo "&nbsp;&nbsp;";
				echo "<a href=\"../sorttop/mainmenu/$id\" class=\"text-success\"> ";
				echo "<i class=\"fa fa-arrow-circle-up fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}
		break;
		case 'gallery':
			$s = Gallery::find($id);
			if($s->gallery_sorting=="1"){
				echo "<a href=\"sortdown/gallery/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}else{
				echo "<a href=\"sortdown/gallery/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
				echo "&nbsp;&nbsp;";
				echo "<a href=\"sorttop/gallery/$id\" class=\"text-success\"> ";
				echo "<i class=\"fa fa-arrow-circle-up fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}
		break;
		case 'maindepart':
		$s = Maindepart::find($id);
		if($s->maindepart_sorting=="1"){
			echo "<a href=\"../sortdown/maindepart/$id\" class=\"text-danger\"> ";
			echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
			echo "</a>";
		}else{
			echo "<a href=\"../sortdown/maindepart/$id\" class=\"text-danger\"> ";
			echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
			echo "</a>";
			echo "&nbsp;&nbsp;";
			echo "<a href=\"../sorttop/maindepart/$id\" class=\"text-success\"> ";
			echo "<i class=\"fa fa-arrow-circle-up fa-2x\" aria-hidden=\"true\"></i>";
			echo "</a>";
		}
	break;
	case 'submenu':
			$s = Submenu::find($id);
			if($s->submenu_sorting=="1"){
				echo "<a href=\"../sortdown/submenu/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}else{
				echo "<a href=\"../sortdown/submenu/$id\" class=\"text-danger\"> ";
				echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
				echo "&nbsp;&nbsp;";
				echo "<a href=\"../sorttop/submenu/$id\" class=\"text-success\"> ";
				echo "<i class=\"fa fa-arrow-circle-up fa-2x\" aria-hidden=\"true\"></i>";
				echo "</a>";
			}
		break;
		case 'subdepart':
		$s = Subdepart::find($id);
		if($s->subdepart_sorting=="1"){
			echo "<a href=\"../sortdown/subdepart/$id\" class=\"text-danger\"> ";
			echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
			echo "</a>";
		}else{
			echo "<a href=\"../sortdown/subdepart/$id\" class=\"text-danger\"> ";
			echo "<i class=\"fa fa-arrow-circle-down fa-2x\" aria-hidden=\"true\"></i>";
			echo "</a>";
			echo "&nbsp;&nbsp;";
			echo "<a href=\"../sorttop/subdepart/$id\" class=\"text-success\"> ";
			echo "<i class=\"fa fa-arrow-circle-up fa-2x\" aria-hidden=\"true\"></i>";
			echo "</a>";
		}
	break;
	endswitch;
}
public static function ListArray($array)
	{
		if(count($array)==1){
			foreach($array as $arr =>$a) 
		{

			return $a;
		}
		}else{
	 			return implode('', $array);
			}
	}
public static function Permiss($array)
{
	//$permission = str_split($array);
	switch($array){
		case '1': //all
			return  '1';
		break;
		case '23': //add/edit
			return '2';
		break;
		case '234': //add/edit/del
			return '3';
		break;
		case '5': //readonly
			return  '4';
		break;
		case '24': //add/del
			return  '5';
		break;
		case '34': //edit/del
			return  '6';
		break;
 

}
	 
}

static function Useronline() 
{

$count = DB::table('tb_counter')->where(array('UDATE'=>date("Y-m-d"),'IP'=>$_SERVER["REMOTE_ADDR"]))
->count();
//return $count;
if($count<=0)
{
$insert = DB::table('tb_counter')->insert(array('UDATE'=>date("Y-m-d"),'IP'=>$_SERVER["REMOTE_ADDR"]));
$insert_daily = DB::table('tb_daily')->insert(array('UDATE'=>date("Y-m-d"),'NUM'=>'0'));
}else{
$insert = DB::table('tb_counter')->insert(array('UDATE'=>date("Y-m-d"),'IP'=>$_SERVER["REMOTE_ADDR"]));
$count = DB::table('tb_counter')->where(array('UDATE'=>date("Y-m-d"),'IP'=>$_SERVER["REMOTE_ADDR"]))->count();
$update = DB::table('tb_daily')
->where(array('UDATE' => date("Y-m-d")))
	->update(array('NUM' => $count));
$Delete = DB::table('tb_counter')->where(array('UDATE' => date("Y-m-d",strtotime("-1 day"))))->delete();
}


 $StrToday = DB::table('tb_counter')->where(array('UDATE' => date("Y-m-d")))->count();
 $Yesterday = DB::table('tb_daily')->select('NUM')->where(array('UDATE' => date("Y-m-d",strtotime("-1 day"))))->first();
 if($Yesterday==null){
 	$CountYesterday ='0';
 }else{
 	$CountYesterday=number_format($Yesterday->NUM);
 }
  $month = date('Y-m');
  $ThisMonth = DB::table('tb_daily')
  ->whereRaw("DATE_FORMAT(UDATE,'%Y-%m')  ='$month' " )
  ->sum('NUM');
   
 if($ThisMonth==null){
 	$CountThisMonth ='0';
 }else{
 	$CountThisMonth=number_format($ThisMonth);
 }
 	$lmonth = date('Y-m',strtotime("-1 month"));
   $LastMonth = DB::table('tb_daily')
  ->whereRaw("DATE_FORMAT(UDATE,'%Y-%m')  = '$lmonth' " )
  ->sum('NUM');
  
 if($LastMonth==null){
 	$CountLastMonth ='0';
 }else{
 	$CountLastMonth=number_format($LastMonth);
 }

 $year = date('Y');
  $ThisYear = DB::table('tb_daily')
  ->whereRaw("DATE_FORMAT(UDATE,'%Y')  ='$year' " )
  ->sum('NUM');
   
 if($ThisYear==null){
 	$CountThisYear ='0';
 }else{
 	$CountThisYear=number_format($ThisYear);
 }
  	$lyear = date('Y',strtotime("-1 year"));
   $LastYear = DB::table('tb_daily')
  ->whereRaw("DATE_FORMAT(UDATE,'%Y')  = '$lyear' " )
  ->sum('NUM');
  
 if($LastYear==null){
 	$CountLastYear ='0';
 }else{
 	$CountLastYear=number_format($LastYear);
 }



 $text ="<ul class=\"list-unstyled\">";
 $text .="<li>วันนี้";
 $text .= "<span class=\"label label-primary\" style=\"margin-left:46px;margin-right:10px;\">".number_format($StrToday)."</span>";
 $text .="คน</li>";
 $text .="<li>เมื่อวานนี้";
  $text .= "<span class=\"label label-success\" style=\"margin-left:20px;margin-right:10px;\">".$CountYesterday."</span>";
 $text .="คน</li>";
 $text .="<li>เดือนนี้";
  $text .= "<span class=\"label label-info\" style=\"margin-left:32px;margin-right:10px;\">".$CountThisMonth."</span>";
 $text .="คน</li>";
 $text .="<li>เดือนที่แล้ว";
  $text .= "<span class=\"label label-warning\" style=\"margin-left:8px;margin-right:10px;\">".$CountLastMonth."</span>";
 $text .="คน</li>";
 $text .="<li>ปีนี้";
  $text .= "<span class=\"label label-danger\" style=\"margin-left:51px;margin-right:10px;\">".$CountThisYear."</span>";
 $text .="คน</li>";
 $text .="<li>ปีที่แล้ว";
  $text .= "<span class=\"label label-default\" style=\"margin-left:28px;margin-right:10px;\">".$CountLastYear."</span>";
 $text .="คน</li>";
 $text .="</ul>";
 return $text;

 /*
CREATE TABLE `tb_counter` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`UDATE` DATE NOT NULL,
	`IP` VARCHAR(30) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=0
; 


CREATE TABLE `tb_daily` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`UDATE` DATE NOT NULL,
	`NUM` VARCHAR(11) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=0
;
*/
 	
}

public static function get_maindepart($data,$id=null)
{
 
 foreach ($data as $key => $v) {

 
 	if($v->parent_id =="0") {
 			if($id !=null){
 				if($v->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 		 	echo "<option value=\"$v->id\" $select>";
 			echo "".$v->maindepart_name;
 
 	   $num = Maindepart::where('parent_id',$v->id)->count();
 	  	 if($num !='0') {
 	  		 	$sub1 = Maindepart::where('parent_id',$v->id)->get();
 	  		 	foreach($sub1 as $sub => $s1) {
 	  		 		if($id !=null){
 				if($s1->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 	  		 		 echo "<option value=\"$s1->id\" $select>";
 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp; - ".$s1->maindepart_name;

 	  		 		 $num2 = Maindepart::where('parent_id',$s1->id)->count();
 	  		 		 	if($num2 !='0') {
				 	  		 	$sub2 = Maindepart::where('parent_id',$s1->id)->get();
				 	  		 	foreach($sub2 as $subs2 => $s2) {
				 	  		 		if($id !=null){
					 				if($s2->id == $id) {
					 					$select = "selected";
					 				}else {
					 					$select ="";
					 				}
					 			}else{
					 				$select = "";
					 			}
				 	  		 		 echo "<option value=\"$s2->id\" $select>";
				 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -- ".$s2->maindepart_name;

				 	  		 		
				 	  		 			// $num3 = Categories::where('parent_id',$s2->id)->count();
				 	  		 		 // 	if($num3 !='0') {
								 	  		//  	$sub3 = Categories::where('parent_id',$s2->id)->get();
								 	  		//  	foreach($sub3 as $subs3 => $s3) {
								 	  		//  		 echo "<option>";
								 	  		//  		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; > ".$s3->mainmenu_name;

								 	  		 		  
								 	  		//  	}
								 	  	 // }  
				 	  		 	}
				 	  	 }
 	  		 	}
 	  	 }


 	 
 		echo "</option>";
 }

}

 
}
public static function get_submenu($data,$id=null)
{
	  echo '<option value="0">';
      echo Lang::get('msg.parent_item',array(),'th');
      echo '</option>';
 
 foreach ($data as $key => $v) {

 
 	if($v->parent_id =="0") {
 			if($id !=null){
 				if($v->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 		 	echo "<option value=\"$v->id\" $select>";
 			echo "".$v->submenu_name;
 
 	   $num = Submenu::where('parent_id',$v->id)->count();
 	  	 if($num !='0') {
 	  		 	$sub1 = Submenu::where('parent_id',$v->id)->get();
 	  		 	foreach($sub1 as $sub => $s1) {
 	  		 		if($id !=null){
 				if($s1->id == $id) {
 					$select = "selected";
 				}else {
 					$select ="";
 				}
 			}else{
 				$select = "";
 			}
 	  		 		 echo "<option value=\"$s1->id\" $select>";
 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp; - ".$s1->submenu_name;

 	  		 		 $num2 = Submenu::where('parent_id',$s1->id)->count();
 	  		 		 	if($num2 !='0') {
				 	  		 	$sub2 = Submenu::where('parent_id',$s1->id)->get();
				 	  		 	foreach($sub2 as $subs2 => $s2) {
				 	  		 		if($id !=null){
					 				if($s2->id == $id) {
					 					$select = "selected";
					 				}else {
					 					$select ="";
					 				}
					 			}else{
					 				$select = "";
					 			}
				 	  		 		 echo "<option value=\"$s2->id\" $select>";
				 	  		 		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -- ".$s2->submenu_name;

				 	  		 		
				 	  		 			// $num3 = Categories::where('parent_id',$s2->id)->count();
				 	  		 		 // 	if($num3 !='0') {
								 	  		//  	$sub3 = Categories::where('parent_id',$s2->id)->get();
								 	  		//  	foreach($sub3 as $subs3 => $s3) {
								 	  		//  		 echo "<option>";
								 	  		//  		 echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; > ".$s3->mainmenu_name;

								 	  		 		  
								 	  		//  	}
								 	  	 // }  
				 	  		 	}
				 	  	 }
 	  		 	}
 	  	 }


 	 
 		echo "</option>";
 }

}

 
}
public static function CountMDP($id)
{
	$sql = DB::table('tb_subdepart')->where('subdepart_categories', $id)->count();
	return $sql;
}

}