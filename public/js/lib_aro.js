$(document).ready(function() {
    $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });
    $(".number").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          $("#errmsg").html("Number Only").stop().show().fadeOut("slow");
          return false;
        }
      });
   

  $("#btn-exit").click(function(){
    Swal.fire({
              title: 'ต้องการออกจากระบบงานหรือไม่?',       
              icon: 'info',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'ยืนยัน ออกจากระบบงาน !',
              cancelButtonText: 'ยกเลิก'
            }).then((result) => {
              if (result.isConfirmed) {
                //สร้างรายงาน
               
                Swal.fire({
              title :"ออกจากระบบงานเรียบร้อยแล้ว!",
              icon: 'success',
              showConfirmButton: false,
              timer: 1500
            });
                window.setTimeout(function() {
             window.location.href = "logout";
              }, 1500);
              }
            })
  });
});