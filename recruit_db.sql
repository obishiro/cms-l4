-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 06, 2022 at 12:13 PM
-- Server version: 5.7.37
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recruit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_05_11_205235_setup_anvard', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_banner`
--

CREATE TABLE `tb_banner` (
  `id` int(11) NOT NULL,
  `banner_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `banner_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `banner_all_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `banner_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_view` int(15) NOT NULL,
  `banner_show` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_banner`
--

INSERT INTO `tb_banner` (`id`, `banner_name`, `banner_file`, `banner_detail`, `banner_all_detail`, `banner_url`, `banner_view`, `banner_show`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 'ทรงพระเจริญ', '2iUY3qefQPLOCrFJ', '', '', 'ทรงพระเจริญ', 0, 1, '2022-08-05 14:47:39', '2022-08-05 14:47:39', 1),
(2, 'ลงนามถวายพระพร', 'w5TagvVkYdfIXxlN', '', '', 'ลงนามถวายพระพร', 1, 1, '2022-08-05 14:52:21', '2022-08-05 14:53:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_categories`
--

CREATE TABLE `tb_categories` (
  `id` int(25) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `categories_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_show` int(2) NOT NULL,
  `categories_showhome` int(2) NOT NULL,
  `categories_icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `display_type` int(11) NOT NULL,
  `display_embed` longtext COLLATE utf8_unicode_ci NOT NULL,
  `show_title` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_categories`
--

INSERT INTO `tb_categories` (`id`, `parent_id`, `categories_name`, `categories_url`, `categories_show`, `categories_showhome`, `categories_icon`, `sort`, `display_type`, `display_embed`, `show_title`, `created_at`, `updated_at`, `create_by`) VALUES
(4, 0, 'ssssssssssssssss', 'ssssssssssssssss', 1, 1, '', 1, 1, '', 1, '2022-08-05 15:28:01', '2022-08-05 15:28:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_content`
--

CREATE TABLE `tb_content` (
  `id` int(11) NOT NULL,
  `content_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_categories` int(6) NOT NULL,
  `content_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_alldetail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_picture` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_view` int(15) NOT NULL,
  `content_showhome` int(1) NOT NULL,
  `content_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_show` int(1) NOT NULL,
  `content_author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_year` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `content_fast` int(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL,
  `parent_id` int(6) NOT NULL,
  `content_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_content`
--

INSERT INTO `tb_content` (`id`, `content_name`, `content_categories`, `content_detail`, `content_alldetail`, `content_file`, `content_picture`, `content_view`, `content_showhome`, `content_url`, `content_show`, `content_author`, `content_year`, `content_fast`, `created_at`, `updated_at`, `created_by`, `parent_id`, `content_keyword`) VALUES
(1, 'ssssssssssssssss', 1, '<p>errrrrrrrrrrrrrrrrr</p>', '', 'i1QZxc74zpZ4LqFV', 'e7XJS9RWyMQ6YEyc.jpg', 0, 1, 'ssssssssssssssss', 1, '1', '', 1, '2022-08-05 15:12:25', '2022-08-05 15:23:10', 1, 1, ''),
(2, 'ssssssssssssssss', 2, '', '', 'QBKn6SMfx0uiyrJp', 'bvWAYdcjnlfcpnPq.jpg', 0, 1, 'ssssssssssssssss', 1, '1', '', 0, '2022-08-05 15:12:54', '2022-08-05 15:23:19', 1, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_depart`
--

CREATE TABLE `tb_depart` (
  `id` int(11) NOT NULL,
  `depart_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_depart`
--

INSERT INTO `tb_depart` (`id`, `depart_name`, `created_at`, `updated_at`) VALUES
(1, 'แผนกสัสดี', NULL, NULL),
(2, 'แผนกกำลังพล', NULL, NULL),
(3, 'แผนกควบคุมและสถิติ', NULL, NULL),
(4, 'แผนกตรวจและประเมินผล', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_enviroment`
--

CREATE TABLE `tb_enviroment` (
  `id` int(11) NOT NULL,
  `web_name_lo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web_name_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web_address` mediumtext COLLATE utf8_unicode_ci,
  `web_tel` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web_keyword` mediumtext COLLATE utf8_unicode_ci,
  `web_detail` mediumtext COLLATE utf8_unicode_ci,
  `web_logo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web_fb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web_map` text COLLATE utf8_unicode_ci,
  `web_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_enviroment`
--

INSERT INTO `tb_enviroment` (`id`, `web_name_lo`, `web_name_en`, `web_address`, `web_tel`, `web_email`, `web_keyword`, `web_detail`, `web_logo`, `web_fb`, `web_map`, `web_fax`, `created_by`) VALUES
(3, 'กองการสัสดี หน่วยบัญชาการรักษาดินแดน', 'Army Recruiting Officer', 'เลขที่ 2 ถนนเจริญกรุง แขวงพระบรมหาราชวัง เขตพระนคร กรุงเทพมหานคร 10200', 'โทร. 0-2223-3259, โทรทหาร 91980', 'info@gmail.com', '', '', 'BFJ5IdDKGhcIQzsQ.png', '', NULL, '0-2223-3259', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_files`
--

CREATE TABLE `tb_files` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_files`
--

INSERT INTO `tb_files` (`id`, `files_newname`, `files_oldname`, `files_type`, `files_size`, `token`) VALUES
(1, '', '', '0', 0, 'i1QZxc74zpZ4LqFV'),
(2, '', '', '0', 0, 'QBKn6SMfx0uiyrJp');

-- --------------------------------------------------------

--
-- Table structure for table `tb_files_banner`
--

CREATE TABLE `tb_files_banner` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_files_banner`
--

INSERT INTO `tb_files_banner` (`id`, `files_newname`, `files_oldname`, `files_thumb`, `files_type`, `files_size`, `token`) VALUES
(1, 'qNWjZaq6PisvPABi.jpg', '28072565.jpg', 'qNWjZaq6PisvPABi.jpg', 'jpg', 89670, '2iUY3qefQPLOCrFJ'),
(2, 'fGDylpKJahzwwiq6.jpg', 'Banner_Queen_Sirikit.jpg', 'fGDylpKJahzwwiq6.jpg', 'jpg', 108212, 'w5TagvVkYdfIXxlN');

-- --------------------------------------------------------

--
-- Table structure for table `tb_files_gallery`
--

CREATE TABLE `tb_files_gallery` (
  `id` int(15) NOT NULL,
  `files_newname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `files_oldname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `files_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `files_size` int(100) NOT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_gallery`
--

CREATE TABLE `tb_gallery` (
  `id` int(11) NOT NULL,
  `gallery_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `gallery_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_view` int(15) NOT NULL,
  `gallery_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_sorting` int(11) NOT NULL,
  `gallery_show` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(15) NOT NULL,
  `parent_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_maindepart`
--

CREATE TABLE `tb_maindepart` (
  `id` int(25) NOT NULL,
  `maindepart_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maindepart_type` int(6) NOT NULL,
  `maindepart_detail` longtext COLLATE utf8_unicode_ci,
  `maindepart_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maindepart_embed` longtext COLLATE utf8_unicode_ci,
  `m_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maindepart_sorting` int(6) NOT NULL,
  `maindepart_position` int(2) DEFAULT NULL,
  `maindepart_show` int(1) DEFAULT NULL,
  `maindepart_showhome` int(1) DEFAULT NULL,
  `maindepart_logo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `show_title` int(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_maindepart`
--

INSERT INTO `tb_maindepart` (`id`, `maindepart_name`, `maindepart_type`, `maindepart_detail`, `maindepart_url`, `maindepart_embed`, `m_url`, `maindepart_sorting`, `maindepart_position`, `maindepart_show`, `maindepart_showhome`, `maindepart_logo`, `show_title`, `parent_id`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'กองการสัสดี', 3, '', 'http://', '', 'กองการสัสดี', 2, NULL, 1, NULL, '', 0, 0, '2019-03-24 12:28:17', '2020-09-13 18:12:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mainmenu`
--

CREATE TABLE `tb_mainmenu` (
  `id` int(25) NOT NULL,
  `mainmenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_type` int(6) NOT NULL,
  `mainmenu_detail` longtext COLLATE utf8_unicode_ci,
  `mainmenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mainmenu_embed` longtext COLLATE utf8_unicode_ci,
  `m_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mainmenu_sorting` int(6) NOT NULL,
  `mainmenu_position` int(2) DEFAULT NULL,
  `mainmenu_show` int(1) DEFAULT NULL,
  `mainmenu_showhome` int(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_mainmenu`
--

INSERT INTO `tb_mainmenu` (`id`, `mainmenu_name`, `mainmenu_type`, `mainmenu_detail`, `mainmenu_url`, `mainmenu_embed`, `m_url`, `mainmenu_sorting`, `mainmenu_position`, `mainmenu_show`, `mainmenu_showhome`, `parent_id`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'ผู้บังคับบัญชา', 3, '<div align=\"center\"><b>กรมการกำลังสำรองทหารบก\r\n</b></div><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td width=\"100\" valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\"><strong>ลำดับ</strong></div></td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><b><span class=\"style6\">ตั้งแต่</span></b></div></td>\r\n      <td width=\"37\" valign=\"top\"><div class=\"style1\" align=\"center\"></div><br></td>\r\n      <td width=\"230\" valign=\"top\"><div class=\"style4\" align=\"center\"><b><span class=\"style6\">ถึง</span></b></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"> </td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"> </td>\r\n      <td valign=\"top\"> </td>\r\n      <td valign=\"top\"> </td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑</div></td>\r\n      \r\n      <td width=\"43\" valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td width=\"138\" valign=\"top\"><div align=\"left\"><span class=\"style1\">มานะ</span></div></td>\r\n      <td width=\"172\" valign=\"top\"><span class=\"style1\">รัตนโกเศศ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑ ต.ค. ๒๕๑๓</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๑ ต.ค. ๒๕๑๖</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ประศาสน์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ทองใบใหญ่</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑ ต.ค. ๒๕๑๖</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๓ ส.ค. ๒๕๑๙</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">อรรคพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">สมรูป</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๓ ส.ค. ๒๕๑๙</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๑๒ ธ.ค. ๒๕๒๐</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๔</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">พงษ์เกียรติ</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">สุดชูเกียรติ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑๒ ธ.ค. ๒๕๒๐</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๙ พ.ย. ๒๕๒๔</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๕</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">วิจักร</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">นิลจุลกะ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4\"><span class=\"style1\">๙ พ.ย. ๒๕๒๔</span></span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑ ก.พ. ๒๕๒๕</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๖</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ปราโมทย์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ระงับภัย</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑ ก.พ. ๒๕๒๕</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๒ ต.ค. ๒๕๒๖</div></td>\r\n    </tr>\r\n      <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๗</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">สุพจน์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เกิดชูชื่น</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๒ ต.ค. ๒๕๒๖</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๗ ต.ค. ๒๕๒๘</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๘</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ศัลย์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">สุขสำเร็จ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๗ ต.ค. ๒๕๒๘</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๗ ก.พ. ๒๕๓๒</div></td>\r\n    </tr>\r\n      <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๙</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">กิตติพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ผิวผ่อง</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๗ ก.พ. ๒๕๓๒</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๗ ต.ค. ๒๕๓๔</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๐</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">กฤษดา</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ลิมพะสุต</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๗ ต.ค. ๒๕๓๔</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๒๒ ก.ค. ๒๕๓๕</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๑</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">เฉลิมพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เรืองสวัสดิ์์</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๒๒ ก.ค. ๒๕๓๕</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๒๓ พ.ค. ๒๕๓๙</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ธนดล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เผ่าจินดา</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๒๓ พ.ค. ๒๕๓๙</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๒๗ ก.ย. ๒๕๔๒</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชูเกียรติ</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เธียรสุนทร</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๒๗ ก.ย. ๒๕๔๒</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๐ พ.ค. ๒๕๔๔</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n        <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"><span class=\"style1\">แปรสภาพเป็น\r\n กองการสัสดี หน่วยบัญชาการกำลังสำรอง ตามราชกิจจานุเบกษา ฉบับ กฤษฎีกา \r\nเล่ม ๑๑๘ ตอนที่ ๒๘ ก. ลง วันที่ ๑๐ พฤษภาคม พ.ศ. ๒๕๔๔</span></div></td>\r\n    </tr>\r\n            <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\"><strong>ลำดับ</strong></div></td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style6\">ตั้งแต่</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\"></div><br></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style6\">ถึง</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"> </td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"> </td>\r\n      <td valign=\"top\"> </td>\r\n      <td valign=\"top\"> </td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชูเกียรติ</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เธียรสุนทร</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๐ พ.ค. ๒๕๔๔</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๐ พ.ย. ๒๕๔๖</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">อิทธิพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ทองดี</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๐ พ.ย. ๒๕๔๖</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๒๓ เม.ย. ๒๕๔๘</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชาติชาย</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">แจ้งสี</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๒๓ เม.ย. ๒๕๔๘</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๒๑ เม.ย. ๒๕๕๒</span></div></td>\r\n    </tr>\r\n     <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n        <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">ตามคำสั่ง\r\n กองทัพบก (เฉพาะ) ที่ ๒๙/๕๒ ลง ๒๑ เมษายน พ.ศ. ๒๕๕๒ ให้เปลี่ยนนามหน่วยจาก\r\n หน่ยบัญชาการกำลังสำรอง เป็น หน่วยบัญชาการรักษาดินแดน</div></td>\r\n    </tr>\r\n            <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\"><strong>ลำดับ</strong></div></td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style6\">ตั้งแต่</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\"></div><br></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style6\">ถึง</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"> </td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"> </td>\r\n      <td valign=\"top\"> </td>\r\n      <td valign=\"top\"> </td>\r\n    </tr>\r\n   <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชาติชาย</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">แจ้งสี</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๒๓ เม.ย. ๒๕๕๒</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๓๐ ก.ย. ๒๕๕๕</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ไตรจักร์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">นาคะไพบูลย์</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๓๐ ก.ย. ๒๕๕๕</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style8\" align=\"center\">๗ ต.ค. ๒๕๕๙</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">สมพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ปะละไทย</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๗ ต.ค. ๒๕๕๙</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style8\" align=\"center\">๑๕ พ.ค. ๒๕๖๑</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๔</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">เสกสรรค์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เสืออิ่ม</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑๕ พ.ค. ๒๕๖๑</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style8\" align=\"center\">ปัจจุบัน</div></td></tr></tbody></table><p><br></p>', 'http://', '', 'ผู้บังคับบัญชา', 1, 1, 1, NULL, 0, '2021-12-28 19:36:36', '2022-08-05 19:32:44', 1),
(2, 'แผนกภายใน', 3, '', 'http://', '', 'แผนกภายใน', 2, 1, 1, NULL, 0, '2021-12-28 19:37:27', '2022-08-05 19:32:12', 1),
(3, 'ติดต่อเรา', 1, '<p>กองการสัสดี หน่วยบัญชาการรักษาดินแดน</p><p>เลขที่ 2 ถนนเจริญกรุง แขวงพระบรมหาราชวัง เขตพระนคร กรุงเทพมหานคร 10200</p><p>โทร. 0-2223-3259, โทรทหาร 91980</p><p><iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.525699323985!2d100.49234371431163!3d13.747141101106454!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2990556eb4987%3A0x124b89b1c0e4dbda!2sTerritorial%20Defense%20Command!5e0!3m2!1sen!2sth!4v1659017634669!5m2!1sen!2sth\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe><br></p>', 'http://', '', 'ติดต่อเรา', 3, 1, 1, NULL, 0, '2021-12-28 19:37:44', '2022-07-29 22:19:16', 1),
(4, 'ผู้อำนวยการกองการสัสดี', 4, '', 'http://', '<p><br></p>', 'ผู้อำนวยการกองการสัสดี', 1, 2, 0, NULL, 0, '2021-12-28 19:40:41', '2022-08-05 14:39:04', 1),
(5, 'ระเบียบ/คำสั่งต่างๆ', 3, '', 'http://', '', 'ระเบียบ-คำสั่งต่างๆ', 3, 2, 0, NULL, 0, '2021-12-28 19:41:42', '2022-08-05 15:10:59', 1),
(6, 'โปรแกรมกิจการสัสดี', 3, '', 'http://', '', 'โปรแกรมกิจการสัสดี', 2, 2, 1, NULL, 0, '2022-07-28 16:02:37', '2022-08-05 15:10:59', 1),
(7, 'เว็บไซต์แนะนำ', 3, '', 'http://', '', 'เว็บไซต์แนะนำ', 4, 2, 1, NULL, 0, '2022-07-28 16:06:43', '2022-07-29 22:19:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_permission`
--

CREATE TABLE `tb_permission` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `p_permission` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_permission`
--

INSERT INTO `tb_permission` (`id`, `user_id`, `token`, `p_permission`) VALUES
(2, 1, '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_prefix`
--

CREATE TABLE `tb_prefix` (
  `id` int(25) NOT NULL,
  `prefix_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prefix_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_prefix`
--

INSERT INTO `tb_prefix` (`id`, `prefix_name`, `prefix_url`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'นาย', 'นาย', '2016-08-13 21:16:43', '2016-08-13 21:16:43', 1),
(2, 'นาง', 'นาง', '2016-08-13 21:16:52', '2016-08-13 21:16:52', 1),
(3, 'นางสาว', 'นางสาว', '2016-08-13 21:16:59', '2016-08-13 21:18:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_profiles`
--

CREATE TABLE `tb_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `webSiteURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profileURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photoURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `displayName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `firstName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthMonth` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthYear` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailVerified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coverInfoURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `token` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_profiles`
--

INSERT INTO `tb_profiles` (`id`, `user_id`, `provider`, `identifier`, `webSiteURL`, `profileURL`, `photoURL`, `displayName`, `description`, `firstName`, `lastName`, `gender`, `language`, `age`, `birthDay`, `birthMonth`, `birthYear`, `email`, `emailVerified`, `phone`, `address`, `country`, `region`, `city`, `zip`, `username`, `coverInfoURL`, `created_at`, `updated_at`, `token`) VALUES
(2, 1, '', '', NULL, NULL, 'PH6iNkGOpMAgh9oM.png', NULL, NULL, 'ผู้ดูแลระบบ', '', NULL, NULL, NULL, NULL, NULL, NULL, 'kscomsci@gmail.com', NULL, '088-5959-562', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-30 17:00:00', '2016-08-30 17:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_subdepart`
--

CREATE TABLE `tb_subdepart` (
  `id` int(25) NOT NULL,
  `subdepart_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subdepart_categories` int(6) NOT NULL,
  `subdepart_type` int(6) NOT NULL,
  `subdepart_detail` longtext COLLATE utf8_unicode_ci,
  `subdepart_all_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subdepart_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subdepart_show` int(1) NOT NULL,
  `subdepart_sorting` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL,
  `subdepart_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_submenu`
--

CREATE TABLE `tb_submenu` (
  `id` int(25) NOT NULL,
  `submenu_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submenu_categories` int(6) NOT NULL,
  `submenu_type` int(6) NOT NULL,
  `submenu_detail` longtext COLLATE utf8_unicode_ci,
  `submenu_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `submenu_show` int(1) NOT NULL,
  `submenu_sorting` int(11) NOT NULL,
  `submenu_pic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `submenu_picshow` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL,
  `parent_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_submenu`
--

INSERT INTO `tb_submenu` (`id`, `submenu_name`, `submenu_categories`, `submenu_type`, `submenu_detail`, `submenu_url`, `s_url`, `submenu_show`, `submenu_sorting`, `submenu_pic`, `submenu_picshow`, `created_at`, `updated_at`, `create_by`, `parent_id`) VALUES
(1, 'นายทหารการสัสดี', 2, 1, '', 'http://', 'นายทหารการสัสดี', 1, 1, '', 0, '2021-12-28 19:42:39', '2022-07-28 14:31:39', 1, 0),
(2, 'แผนกสัสดี', 2, 1, '', 'http://', 'แผนกสัสดี', 1, 2, '', 0, '2021-12-28 19:43:06', '2022-07-28 14:32:18', 1, 0),
(3, 'แผนกกำลังพล', 2, 1, '', 'http://', 'แผนกกำลังพล', 1, 3, '', 0, '2021-12-28 19:43:21', '2022-07-28 14:32:28', 1, 0),
(4, 'แผนกควบคุมและสถิติ', 2, 1, '', 'http://', 'แผนกควบคุมและสถิติ', 1, 4, '', 0, '2022-07-28 14:59:45', '2022-07-28 14:59:45', 1, 0),
(5, 'แผนกตรวจและประเมินผล', 2, 1, '', 'http://', 'แผนกตรวจและประเมินผล', 1, 5, '', 0, '2022-07-28 15:00:21', '2022-07-28 15:00:21', 1, 0),
(6, 'คำสั่งบรรจุสัสดีสายพันธุ์ใหม่', 5, 1, '', 'http://', 'คำสั่งบรรจุสัสดีสายพันธุ์ใหม่', 1, 1, 'hBh1H6PM2nRnf9U0.jpg', 1, '2022-07-28 15:06:27', '2022-07-28 15:06:27', 1, 0),
(7, 'หลักเกณฑ์การปรับย้าย', 5, 1, '', 'http://', 'หลักเกณฑ์การปรับย้าย', 1, 2, '', 0, '2022-07-28 15:07:29', '2022-07-28 16:01:46', 1, 0),
(8, ' คู่มือการใช้งานโปรแกรม Recruit 4.0', 6, 2, '', 'https://www.tdc.mi.th/pdf/recruit4.0/Manual_Recruit_4.0.pdf', '-คู่มือการใช้งานโปรแกรม-Recruit-4.0', 1, 3, '', 0, '2022-07-28 16:04:10', '2022-07-28 16:39:49', 1, 0),
(9, 'แบบฟอร์ม Linkage Center / ตัวอย่าง', 6, 2, '', 'https://www.tdc.mi.th/pdf/Linkage/Ex_Linkage_Center_1.pdf', 'แบบฟอร์ม-Linkage-Center---ตัวอย่าง', 1, 4, '', 0, '2022-07-28 16:04:57', '2022-07-28 16:39:44', 1, 0),
(10, 'กระทรวงกลาโหม', 7, 2, '', 'https://www.mod.go.th/', 'กระทรวงกลาโหม', 1, 1, '', 0, '2022-07-28 16:07:31', '2022-07-28 16:07:31', 1, 0),
(11, 'กองบัญชาการกองทัพไทย', 7, 2, '', 'https://www.rtarf.mi.th', 'กองบัญชาการกองทัพไทย', 1, 2, '', 0, '2022-07-28 16:08:01', '2022-07-28 16:08:01', 1, 0),
(12, 'กองทัพบก', 7, 2, '', 'https://www.rta.mi.th', 'กองทัพบก', 1, 3, '', 0, '2022-07-28 16:08:36', '2022-07-28 16:08:36', 1, 0),
(13, 'กองทัพเรือ', 7, 2, '', 'https://www.navy.mi.th', 'กองทัพเรือ', 1, 4, '', 0, '2022-07-28 16:09:04', '2022-07-28 16:09:04', 1, 0),
(14, 'กองทัพอากาศ', 7, 2, '', 'https://www.rtaf.mi.th', 'กองทัพอากาศ', 1, 5, '', 0, '2022-07-28 16:09:33', '2022-07-28 16:09:33', 1, 0),
(15, 'ระบบกิจการสัสดี', 6, 2, '', 'https://recruit.tdc.mi.th', 'ระบบกิจการสัสดี', 1, 1, 'U7AhjHuGssl3en3l.jpg', 1, '2022-07-28 16:38:20', '2022-07-28 16:39:40', 1, 0),
(16, 'บันทึกผลการตรวจเลือกฯ', 6, 2, '', 'https://recruit.tdc.mi.th/User/UserPassLogin', 'บันทึกผลการตรวจเลือกฯ', 1, 2, 'QwkFRxd1gLbQOv7z.jpg', 1, '2022-07-28 16:39:20', '2022-07-28 16:40:19', 1, 0),
(17, 'ทำเนียบผู้อำนวยการกองการสัสดี', 1, 1, '<div align=\"center\"><b>กรมการกำลังสำรองทหารบก\r\n</b></div><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td width=\"100\" valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\"><strong>ลำดับ</strong></div></td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><b><span class=\"style6\">ตั้งแต่</span></b></div></td>\r\n      <td width=\"37\" valign=\"top\"><div class=\"style1\" align=\"center\"></div><br></td>\r\n      <td width=\"230\" valign=\"top\"><div class=\"style4\" align=\"center\"><b><span class=\"style6\">ถึง</span></b></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\">&nbsp;</td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\">&nbsp;</td>\r\n      <td valign=\"top\">&nbsp;</td>\r\n      <td valign=\"top\">&nbsp;</td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑</div></td>\r\n      \r\n      <td width=\"43\" valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td width=\"138\" valign=\"top\"><div align=\"left\"><span class=\"style1\">มานะ</span></div></td>\r\n      <td width=\"172\" valign=\"top\"><span class=\"style1\">รัตนโกเศศ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑ ต.ค. ๒๕๑๓</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๑ ต.ค. ๒๕๑๖</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ประศาสน์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ทองใบใหญ่</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑ ต.ค. ๒๕๑๖</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๓ ส.ค. ๒๕๑๙</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">อรรคพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">สมรูป</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๓ ส.ค. ๒๕๑๙</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๑๒ ธ.ค. ๒๕๒๐</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๔</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">พงษ์เกียรติ</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">สุดชูเกียรติ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑๒ ธ.ค. ๒๕๒๐</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๙ พ.ย. ๒๕๒๔</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๕</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">วิจักร</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">นิลจุลกะ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4\"><span class=\"style1\">๙ พ.ย. ๒๕๒๔</span></span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑ ก.พ. ๒๕๒๕</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๖</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ปราโมทย์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ระงับภัย</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑ ก.พ. ๒๕๒๕</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๒ ต.ค. ๒๕๒๖</div></td>\r\n    </tr>\r\n      <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๗</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">สุพจน์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เกิดชูชื่น</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๒ ต.ค. ๒๕๒๖</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๗ ต.ค. ๒๕๒๘</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๘</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ศัลย์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">สุขสำเร็จ</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๗ ต.ค. ๒๕๒๘</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๗ ก.พ. ๒๕๓๒</div></td>\r\n    </tr>\r\n      <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๙</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">กิตติพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ผิวผ่อง</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๗ ก.พ. ๒๕๓๒</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๗ ต.ค. ๒๕๓๔</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๐</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">กฤษดา</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ลิมพะสุต</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๗ ต.ค. ๒๕๓๔</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๒๒ ก.ค. ๒๕๓๕</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๑</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">เฉลิมพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เรืองสวัสดิ์์</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๒๒ ก.ค. ๒๕๓๕</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๒๓ พ.ค. ๒๕๓๙</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ธนดล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เผ่าจินดา</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๒๓ พ.ค. ๒๕๓๙</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๒๗ ก.ย. ๒๕๔๒</div></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชูเกียรติ</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เธียรสุนทร</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๒๗ ก.ย. ๒๕๔๒</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๐ พ.ค. ๒๕๔๔</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n        <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"><span class=\"style1\">แปรสภาพเป็น\r\n กองการสัสดี หน่วยบัญชาการกำลังสำรอง ตามราชกิจจานุเบกษา ฉบับ กฤษฎีกา \r\nเล่ม ๑๑๘ ตอนที่ ๒๘ ก. ลง วันที่ ๑๐ พฤษภาคม พ.ศ. ๒๕๔๔</span></div></td>\r\n    </tr>\r\n            <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\"><strong>ลำดับ</strong></div></td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style6\">ตั้งแต่</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\"></div><br></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style6\">ถึง</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\">&nbsp;</td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\">&nbsp;</td>\r\n      <td valign=\"top\">&nbsp;</td>\r\n      <td valign=\"top\">&nbsp;</td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชูเกียรติ</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เธียรสุนทร</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๐ พ.ค. ๒๕๔๔</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4 style1\" align=\"center\">๑๐ พ.ย. ๒๕๔๖</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">อิทธิพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ทองดี</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style4 style1\">๑๐ พ.ย. ๒๕๔๖</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๒๓ เม.ย. ๒๕๔๘</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชาติชาย</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">แจ้งสี</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๒๓ เม.ย. ๒๕๔๘</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๒๑ เม.ย. ๒๕๕๒</span></div></td>\r\n    </tr>\r\n     <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n        <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">ตามคำสั่ง\r\n กองทัพบก (เฉพาะ) ที่ ๒๙/๕๒ ลง ๒๑ เมษายน พ.ศ. ๒๕๕๒ ให้เปลี่ยนนามหน่วยจาก\r\n หน่ยบัญชาการกำลังสำรอง เป็น หน่วยบัญชาการรักษาดินแดน</div></td>\r\n    </tr>\r\n            <tr>\r\n      <td colspan=\"8\" valign=\"top\" height=\"19\"><div align=\"center\"></div><br></td>\r\n    </tr>\r\n     <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\"><strong>ลำดับ</strong></div></td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style6\">ตั้งแต่</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\"></div><br></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style6\">ถึง</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\">&nbsp;</td>\r\n      <td colspan=\"3\" valign=\"top\"></td>\r\n      <td colspan=\"2\" valign=\"top\">&nbsp;</td>\r\n      <td valign=\"top\">&nbsp;</td>\r\n      <td valign=\"top\">&nbsp;</td>\r\n    </tr>\r\n   <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๑</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ชาติชาย</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">แจ้งสี</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๒๓ เม.ย. ๒๕๕๒</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style4\" align=\"center\"><span class=\"style1\">๓๐ ก.ย. ๒๕๕๕</span></div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๒</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">ไตรจักร์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">นาคะไพบูลย์</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๓๐ ก.ย. ๒๕๕๕</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style8\" align=\"center\">๗ ต.ค. ๒๕๕๙</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๓</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">สมพล</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">ปะละไทย</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๗ ต.ค. ๒๕๕๙</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style8\" align=\"center\">๑๕ พ.ค. ๒๕๖๑</div></td>\r\n    </tr>\r\n    <tr>\r\n      <td valign=\"top\" height=\"19\"><div class=\"style1\" align=\"center\">๔</div></td>\r\n      \r\n      <td valign=\"top\"><div align=\"right\"><span class=\"style1\">พ.อ.</span></div></td>\r\n      <td valign=\"top\"><div align=\"left\"><span class=\"style1\">เสกสรรค์</span></div></td>\r\n      <td valign=\"top\"><span class=\"style1\">เสืออิ่ม</span></td>\r\n      <td colspan=\"2\" valign=\"top\"><div align=\"center\"><span class=\"style1\">๑๕ พ.ค. ๒๕๖๑</span></div></td>\r\n      <td valign=\"top\"><div class=\"style1\" align=\"center\">-</div></td>\r\n      <td valign=\"top\"><div class=\"style8\" align=\"center\">ปัจจุบัน</div></td></tr></tbody></table><p><br></p>', 'http://', 'ทำเนียบผู้อำนวยการกองการสัสดี', 1, 2, '', 0, '2022-08-05 19:33:55', '2022-08-05 19:37:57', 1, 0),
(18, 'ผู้บังคับบัญชา', 1, 1, '<p><br></p>', 'http://', 'ผู้บังคับบัญชา', 1, 1, '', 0, '2022-08-05 19:34:13', '2022-08-05 20:11:48', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tag`
--

CREATE TABLE `tb_tag` (
  `id` int(25) NOT NULL,
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_count` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `create_by` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tag`
--

INSERT INTO `tb_tag` (`id`, `tag_name`, `tag_url`, `tag_count`, `created_at`, `updated_at`, `create_by`) VALUES
(1, 'ข่าวประชาสัมพันธ์', 'ข่าวประชาสัมพันธ์', 0, '2016-08-10 20:33:48', '2020-09-13 12:55:19', 1),
(2, 'ข่าวสารสายงานสัสดี', 'ข่าวสารสายงานสัสดี', 0, '2016-08-11 17:46:35', '2020-09-13 12:55:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tagcontent`
--

CREATE TABLE `tb_tagcontent` (
  `id` int(11) NOT NULL,
  `tag_id` int(6) NOT NULL,
  `content_id` int(15) NOT NULL,
  `tag_count` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_tagcontent`
--

INSERT INTO `tb_tagcontent` (`id`, `tag_id`, `content_id`, `tag_count`) VALUES
(1, 2, 1, 0),
(3, 1, 3, 0),
(4, 2, 1, 0),
(5, 2, 2, 0),
(6, 1, 3, 0),
(8, 2, 4, 0),
(9, 1, 5, 0),
(10, 1, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(25) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` int(3) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_code` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_maindepart` int(6) NOT NULL,
  `user_subdepart` int(6) NOT NULL,
  `user_state` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_status`, `user_type`, `user_code`, `created_at`, `updated_at`, `remember_token`, `user_maindepart`, `user_subdepart`, `user_state`, `created_by`) VALUES
(1, 'admin', '$2y$10$a6ffc1R2fdqwCdFuCENXRuDmHgpVrmO.CfmOxScvZTuZafkJffSgC', 1, 1, '', '2015-10-20 00:00:00', '2022-08-05 15:42:42', 'KOTXJtHu6uCejDFNntVETVaCTiH4kja0kUdRnVtmFH9AxjWMdGZrtoN26pad', 1, 0, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banner`
--
ALTER TABLE `tb_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_categories`
--
ALTER TABLE `tb_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_content`
--
ALTER TABLE `tb_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_depart`
--
ALTER TABLE `tb_depart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files`
--
ALTER TABLE `tb_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files_banner`
--
ALTER TABLE `tb_files_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_files_gallery`
--
ALTER TABLE `tb_files_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_gallery`
--
ALTER TABLE `tb_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_maindepart`
--
ALTER TABLE `tb_maindepart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_permission`
--
ALTER TABLE `tb_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_prefix`
--
ALTER TABLE `tb_prefix`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_profiles`
--
ALTER TABLE `tb_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_subdepart`
--
ALTER TABLE `tb_subdepart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tag`
--
ALTER TABLE `tb_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banner`
--
ALTER TABLE `tb_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_categories`
--
ALTER TABLE `tb_categories`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_content`
--
ALTER TABLE `tb_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_depart`
--
ALTER TABLE `tb_depart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_enviroment`
--
ALTER TABLE `tb_enviroment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_files`
--
ALTER TABLE `tb_files`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_files_banner`
--
ALTER TABLE `tb_files_banner`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_files_gallery`
--
ALTER TABLE `tb_files_gallery`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_gallery`
--
ALTER TABLE `tb_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_maindepart`
--
ALTER TABLE `tb_maindepart`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_mainmenu`
--
ALTER TABLE `tb_mainmenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_permission`
--
ALTER TABLE `tb_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_prefix`
--
ALTER TABLE `tb_prefix`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_profiles`
--
ALTER TABLE `tb_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_subdepart`
--
ALTER TABLE `tb_subdepart`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_submenu`
--
ALTER TABLE `tb_submenu`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tb_tag`
--
ALTER TABLE `tb_tag`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_tagcontent`
--
ALTER TABLE `tb_tagcontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
